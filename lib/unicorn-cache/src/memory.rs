use std::collections::HashMap;
use std::sync::{Arc, Mutex, MutexGuard, PoisonError};

use log::debug;

use unicorn_config::cache::CacheConfig;

#[derive(Debug, Default, Clone)]
pub struct MemoryCacher {
    ttl: Option<u64>,
    data: Arc<Mutex<HashMap<String, String>>>,
}

impl MemoryCacher {
    fn fail(err: PoisonError<MutexGuard<HashMap<String, String>>>) {
        debug!("Mutex locking failed: {}", err);
    }

    fn expire(&self, key: String) {
        if self.ttl.is_some() {
            let ttl_key = format!("expiration:{}", key);
            self.set(ttl_key, chrono::Utc::now().timestamp().to_string(), true);
        }
    }

    fn expired(&self, key: String) -> bool {
        if let Some(ttl) = self.ttl {
            let ttl_key = format!("expiration:{}", key);
            let now = chrono::Utc::now().timestamp();
            let cache_value = self.get(ttl_key, true);
            debug!("{:?}", cache_value);
            let unwrap = cache_value.unwrap_or(chrono::Utc::now().timestamp().to_string());
            debug!("{}", unwrap);
            let parsed = unwrap.parse::<i64>();
            debug!("{:?}", parsed);            
            let then = parsed.unwrap_or(0i64);
            let expired = now > (then + (ttl as i64));
            debug!("{}",now);
            debug!("{}",then);
            debug!("{}",ttl);
            debug!("{}",expired);
            expired
        } else {
            false
        }
    }

    pub fn get(&self, key: String, skip: bool) -> Option<String> {
        let expired = if skip {
            false
        } else {
            self.expired(key.clone())
        };
        if !expired {
            match self.data.lock() {
                Ok(data) => match data.get(&key.to_lowercase()) {
                    Some(val) => Some(val.to_owned()),
                    None => None,
                },
                Err(why) => {
                    Self::fail(why);
                    None
                }
            }
        } else {
            None
        }
    }

    pub fn set(&self, key: String, value: String, skip: bool) {
        let expires = match self.data.lock() {
            Ok(mut data) => {
                if data.contains_key(&key) {
                    data.remove(&key.to_lowercase())
                        .unwrap_or_else(|| "".to_owned());
                }
                data.insert(key.to_lowercase(), value);
                true
            }
            Err(why) => {
                Self::fail(why);
                false
            }
        };
        if expires && !skip {
            self.expire(key.clone());
        }
    }

    pub fn del(&self, key: String) {
        match self.data.lock() {
            Ok(mut data) => {
                data.remove(&key.to_lowercase())
                    .unwrap_or_else(|| "".to_owned());
            }
            Err(why) => {
                Self::fail(why);
            }
        }
    }

    pub fn wipe(&self) {
        match self.data.lock() {
            Ok(mut data) => data.clear(),
            Err(why) => Self::fail(why),
        }
    }
}

impl MemoryCacher {
    pub fn new(cfg: &CacheConfig) -> Self {
        let mut mem = Self::default();
        mem.ttl = cfg.ttl;
        mem
    }
}

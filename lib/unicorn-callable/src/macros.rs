#[macro_export]
macro_rules! define_module {
    ($type:ty, $ctor:path) => {
        #[no_mangle]
        pub extern "C" fn _command_create() -> *mut dyn $crate::UnicornModule {
            let ctor: fn() -> $type = $ctor;
            let boxed: Box<$crate::UnicornModule> = Box::new(ctor());
            Box::into_raw(boxed)
        }
    };
}

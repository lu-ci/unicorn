use std::any::Any;

use serenity::http::CacheHttp;

use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;
use unicorn_information::CommandInformation;

use crate::argument::CommandArgument;
use crate::error::CallableError;
use crate::payload::CommandPayload;
use crate::perms::common::{PermissionParams, UnicornPermissions};

const COMMAND_COOLDOWN: i32 = 1;
const PERMISSION_EMOTE: char = '🔒';
const NO_WRITE_EMOTE: char = '😶';
const COOLDOWN_EMOTE: char = '❄';
const BURST_EMOTE: char = '🕥';
const DISCORD_PERMISSION_EMOTE: &str = "🚧";
const DISCORD_PERMISSION_COLOR: u64 = 0xff_cc_4d;
const NO_EMBED_ERROR: &str = "**Error**: My functions require the \"Embed Links\" permission which seems to be missing.";

#[serenity::async_trait]
pub trait UnicornCommand: Any + Send + Sync {
    /*
    Command Detail Functions
    */
    fn command_name(&self) -> &str;
    fn category(&self) -> &str;
    fn nsfw(&self) -> bool {
        false
    }
    fn owner(&self) -> bool {
        false
    }
    fn premium(&self) -> bool {
        false
    }
    fn allow_dm(&self) -> bool {
        false
    }
    fn aliases(&self) -> Vec<&str> {
        Vec::<&str>::new()
    }
    fn example(&self) -> &str {
        ""
    }
    fn description(&self) -> &str;
    fn information(&self) -> CommandInformation {
        let mut aliases = Vec::<String>::new();
        for alstr in self.aliases() {
            aliases.push(alstr.to_owned());
        }
        CommandInformation {
            name: self.command_name().to_owned(),
            category: self.category().to_owned(),
            nsfw: self.nsfw(),
            owner: self.owner(),
            dm: self.allow_dm(),
            aliases,
            example: self.example().to_owned(),
            description: self.description().to_owned(),
        }
    }

    /*
    Various Pre-Execution Checks
    */
    async fn permissions(&self, pld: &CommandPayload) -> UnicornPermissions {
        let params = PermissionParams::new(
            self.command_name().to_owned(),
            self.category().to_owned(),
            self.allow_dm(),
            self.nsfw(),
            self.owner(),
            pld.clone(),
        );
        let mut perms = UnicornPermissions::new(params);
        perms.check().await;
        perms
    }

    async fn basic_permissions(&self, pld: &CommandPayload) -> bool {
        let me = pld.ctx.cache.current_user().await;
        match pld.msg.guild(&pld.ctx.cache).await {
            Some(gld) => {
                let perms = gld.user_permissions_in(pld.msg.channel_id, me.id).await;
                if perms.send_messages() {
                    if perms.embed_links() {
                        true
                    } else {
                        match pld.msg.channel_id.send_message(pld.ctx.http(), |m| {
                            m.content(NO_EMBED_ERROR);
                            m
                        }) {
                            _ => false,
                        }
                    }
                } else {
                    if perms.add_reactions() {
                        match pld.msg.react(pld.ctx.http(), NO_WRITE_EMOTE) {
                            _ => {}
                        }
                    }
                    false
                }
            }
            None => true,
        }
    }

    async fn discord_permissions(&self, _pld: &mut CommandPayload) -> bool {
        true
    }

    fn cooling(&self, pld: &mut CommandPayload) -> bool {
        let is_owner = pld.cfg.discord.owners.contains(&pld.msg.author.id.0);
        if !is_owner {
            let key = format!("cmd:{}:{}", self.command_name(), pld.msg.author.id);
            let cdi = pld.cd.get_cooldown(key.clone());
            if !cdi.active {
                pld.cd.set_cooldown(key, u64::from(pld.msg.author.id), COMMAND_COOLDOWN);
                false
            } else {
                let _ = pld.msg.react(pld.ctx.http.clone(), BURST_EMOTE);
                true
            }
        } else {
            false
        }
    }

    /*
    Command Execution Calls
    */
    fn on_load(&self, _db: &DatabaseHandler, _cfg: &mut ConfigurationBuilder) {}
    fn pre_run(&self) {}
    fn parameters(&self) -> Vec<CommandArgument> {
        Vec::new()
    }
    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError>;
    async fn run(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        if self.basic_permissions(pld).await {
            if self.discord_permissions(pld).await {
                let perms = self.permissions(pld).await;
                if perms.ok() {
                    if !self.cooling(pld) {
                        self.execute(pld).await?
                    } else {
                        match pld.msg.react(pld.ctx.http(), COOLDOWN_EMOTE) {
                            _ => {}
                        }
                    }
                } else {
                    match perms.global.response() {
                        Some(resp) => {
                            let res = pld
                                .msg
                                .channel_id
                                .send_message(pld.ctx.http(), |m| {
                                    m.embed(|e| {
                                        let resp = resp.clone();
                                        e.color(resp.color);
                                        e.title(format!("{} {}", resp.icon, resp.title));
                                        e.description(resp.description);
                                        e
                                    });
                                    m
                                })
                                .await;
                            if res.is_err() {
                                match pld.msg.react(pld.ctx.http(), resp.icon).await {
                                    _ => {}
                                }
                            }
                        }
                        None => match pld.msg.react(pld.ctx.http(), PERMISSION_EMOTE).await {
                            _ => {}
                        },
                    }
                }
            } else {
                match pld.msg.channel_id.send_message(pld.ctx.http(), |m| {
                    m.embed(|e| {
                        e.color(DISCORD_PERMISSION_COLOR);
                        e.title(format!("{} Missing Discord permissions.", DISCORD_PERMISSION_EMOTE));
                        e.description(format!(
                            "{} **{}**. {} `{}help {}` {}",
                            "You seem to be missing the required permissions to execute",
                            self.command_name(),
                            "Please run",
                            pld.get_prefix(),
                            self.command_name(),
                            "for more information on what permissions are required."
                        ));
                        e
                    });
                    m
                }) {
                    _ => {}
                }
            }
        }
        Ok(())
    }
    fn post_run(&self) {}
}

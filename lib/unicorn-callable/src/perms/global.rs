use bson::*;

use crate::perms::common::{BlacklistUserDocument, PermissionParams, PermissionResponse};

pub struct GlobalCommandPermissions {
    // Data
    pub params: PermissionParams,
    // Blacklists
    pub black_usr: bool,
    pub black_gld: bool,
    pub black_cmd: bool,
    pub black_mdl: bool,
    // Denials
    pub dm_denied: bool,
    pub nsfw_denied: bool,
    pub owner_denied: bool,
}

impl GlobalCommandPermissions {
    pub fn new(params: PermissionParams) -> Self {
        Self {
            params,
            black_usr: false,
            black_gld: false,
            black_cmd: false,
            black_mdl: false,
            dm_denied: false,
            nsfw_denied: false,
            owner_denied: false,
        }
    }

    fn check_dm(&mut self) {
        self.dm_denied = if self.params.dm || self.params.nsfw {
            false
        } else {
            self.params.pld.msg.guild_id.is_none()
        }
    }

    async fn check_nsfw(&mut self) {
        self.nsfw_denied = if self.params.nsfw {
            match self.params.pld.msg.channel(&self.params.pld.ctx.cache).await {
                Some(chn) => !chn.is_nsfw(),
                None => true,
            }
        } else {
            false
        }
    }

    fn check_owner(&mut self) {
        self.owner_denied = if self.params.owner {
            !self.params.pld.db.cfg.discord.owners.contains(&self.params.pld.msg.author.id.0)
        } else {
            false
        }
    }

    async fn check_denials(&mut self) {
        self.check_dm();
        self.check_nsfw().await;
        self.check_owner();
    }

    fn black_guild(&mut self) {
        self.black_gld = match self.params.pld.db.get_client() {
            Some(cli) => match self.params.pld.msg.guild_id {
                Some(gid) => {
                    let lookup = doc! {"server_id": gid.0};
                    match cli
                        .database(&self.params.pld.db.cfg.database.name)
                        .collection("blacklisted_servers")
                        .count_documents(lookup, None)
                    {
                        Ok(resp) => resp > 0,
                        Err(_) => false,
                    }
                }
                None => false,
            },
            None => false,
        }
    }

    fn black_user(&mut self, bdoc: BlacklistUserDocument) {
        self.black_usr = if bdoc.total {
            true
        } else {
            self.black_module(bdoc.clone());
            self.black_command(bdoc);
            self.black_mdl || self.black_cmd
        }
    }

    fn black_module(&mut self, bdoc: BlacklistUserDocument) {
        self.black_mdl = bdoc.modules.contains(&self.params.category);
    }

    fn black_command(&mut self, bdoc: BlacklistUserDocument) {
        bdoc.commands.contains(&self.params.name);
    }

    fn check_blacklists(&mut self) {
        self.black_guild();
        if let Some(black_doc) = BlacklistUserDocument::new(&self.params.pld.db, self.params.pld.msg.author.id.0) {
            self.black_user(black_doc);
        }
    }

    fn denied(&self) -> bool {
        self.dm_denied || self.nsfw_denied || self.owner_denied
    }

    fn blacklisted(&self) -> bool {
        self.black_usr || self.black_gld
    }

    pub async fn check(&mut self) {
        self.check_denials().await;
        self.check_blacklists();
    }

    pub fn ok(&self) -> bool {
        !(self.denied() || self.blacklisted())
    }

    pub fn response(&self) -> Option<PermissionResponse> {
        if self.owner_denied {
            Some(PermissionResponse::new(
                '⛔',
                0xbe_19_31,
                "Developer only.",
                format!("I'm sorry {}, I'm afraid I can't let you do that.", self.params.pld.msg.author.name),
            ))
        } else if self.dm_denied {
            Some(PermissionResponse::new(
                '⛔',
                0xbe_19_31,
                "Can't be used in direct messages.",
                format!(
                    "Please use {}{} on a server where I am present.",
                    self.params.pld.get_prefix(),
                    self.params.name
                ),
            ))
        } else if self.nsfw_denied {
            Some(PermissionResponse::new(
                '🍆',
                0x74_4e_aa,
                format!("The {} command is not safe for work.", &self.params.name),
                "Make sure the \"NSFW Channel\" marker is enabled in the channel settings.",
            ))
        } else {
            None
        }
    }
}

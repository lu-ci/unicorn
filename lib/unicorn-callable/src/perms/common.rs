use bson::*;
use serde::{Deserialize, Serialize};

use unicorn_database::DatabaseHandler;

use crate::payload::CommandPayload;
use crate::perms::global::GlobalCommandPermissions;
use crate::perms::local::LocalCommandPermissions;

#[derive(Clone)]
pub struct PermissionParams {
    pub pld: CommandPayload,
    pub name: String,
    pub category: String,
    pub dm: bool,
    pub nsfw: bool,
    pub owner: bool,
}

impl PermissionParams {
    pub fn new(name: String, category: String, dm: bool, nsfw: bool, owner: bool, pld: CommandPayload) -> Self {
        Self {
            pld,
            name,
            category,
            dm,
            nsfw,
            owner,
        }
    }
}

pub struct UnicornPermissions {
    pub params: PermissionParams,
    pub global: GlobalCommandPermissions,
    pub local: LocalCommandPermissions,
}

impl UnicornPermissions {
    pub fn new(params: PermissionParams) -> Self {
        let global = GlobalCommandPermissions::new(params.clone());
        let local = LocalCommandPermissions::new(params.clone());
        Self { params, global, local }
    }

    pub async fn check(&mut self) {
        self.global.check().await;
        self.local.check().await;
    }

    pub fn ok(&self) -> bool {
        self.global.ok() && self.local.ok()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BlacklistUserDocument {
    pub user_id: u64,
    pub total: bool,
    pub modules: Vec<String>,
    pub commands: Vec<String>,
}

impl BlacklistUserDocument {
    pub fn new(db: &DatabaseHandler, gid: u64) -> Option<Self> {
        let cli = db.get_client()?;
        let lookup = doc! {"user_id": gid};
        match cli.database(&db.cfg.database.name).collection("blacklisted_users").find_one(lookup, None) {
            Ok(opt) => {
                let bdoc = bson::Bson::Document(opt?);
                let dres: DecoderResult<Self> = bson::from_bson(bdoc);
                match dres {
                    Ok(bset) => Some(bset),
                    Err(_) => None,
                }
            }
            Err(_) => None,
        }
    }
}

#[derive(Clone)]
pub struct PermissionResponse {
    pub icon: char,
    pub color: u64,
    pub title: String,
    pub description: String,
}

impl PermissionResponse {
    pub fn new(icon: char, color: u64, title: impl ToString, description: impl ToString) -> Self {
        Self {
            icon,
            color,
            title: title.to_string(),
            description: description.to_string(),
        }
    }
}

use std::collections::HashMap;

use bson::*;
use serde::{Deserialize, Serialize};
use serenity::model::channel::Channel;
use serenity::model::guild::Role;
use serenity::model::user::User;

use unicorn_database::DatabaseHandler;
use unicorn_utility::shorts::UnicornShorts;

use crate::payload::CommandPayload;
use crate::perms::common::PermissionParams;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PermissionExceptions {
    pub roles: Vec<u64>,
    pub users: Vec<u64>,
    pub channels: Vec<u64>,
}

impl PermissionExceptions {
    fn role_excepted(&self, roles: Vec<Role>) -> bool {
        let mut result = false;
        for role in roles {
            if self.roles.contains(&role.id.0) {
                result = true;
                break;
            }
        }
        result
    }

    fn user_excepted(&self, user: &User) -> bool {
        self.users.contains(&user.id.0)
    }

    fn channel_excepted(&self, channel: &Option<Channel>) -> bool {
        match channel {
            Some(chn) => self.channels.contains(&chn.id().0),
            None => false,
        }
    }

    pub async fn excepted(&self, pld: &CommandPayload) -> bool {
        self.role_excepted(UnicornShorts::get_roles(&pld.ctx, &pld.msg).await)
            || self.user_excepted(&pld.msg.author)
            || self.channel_excepted(&pld.msg.channel(&pld.ctx.cache).await)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PermissionsDocument {
    pub server_id: u64,
    pub disabled_commands: Vec<String>,
    pub disabled_modules: Vec<String>,
    pub command_exceptions: HashMap<String, PermissionExceptions>,
    pub module_exceptions: HashMap<String, PermissionExceptions>,
}

impl PermissionsDocument {
    pub fn new(db: &DatabaseHandler, gid: u64) -> Option<Self> {
        let cli = db.get_client()?;
        let lookup = doc! {"server_id": gid};
        match cli.database(&db.cfg.database.name).collection("permissions").find_one(lookup, None) {
            Ok(opt) => {
                let bdoc = bson::Bson::Document(opt?);
                let dres: DecoderResult<Self> = bson::from_bson(bdoc);
                match dres {
                    Ok(bset) => Some(bset),
                    Err(_) => None,
                }
            }
            Err(_) => None,
        }
    }
}

pub struct LocalCommandPermissions {
    // Data
    pub params: PermissionParams,
    // Disabled entries
    pub permitted: bool,
}

impl LocalCommandPermissions {
    pub fn new(params: PermissionParams) -> Self {
        Self { params, permitted: true }
    }

    async fn command_excepted(&self, pdoc: &PermissionsDocument) -> bool {
        match pdoc.command_exceptions.get(&self.params.name) {
            Some(pex) => pex.excepted(&self.params.pld).await,
            None => false,
        }
    }

    async fn module_excepted(&self, pdoc: &PermissionsDocument) -> bool {
        match pdoc.module_exceptions.get(&self.params.name) {
            Some(pex) => pex.excepted(&self.params.pld).await,
            None => false,
        }
    }

    fn command_disabled(&self, pdoc: &PermissionsDocument) -> bool {
        pdoc.disabled_commands.contains(&self.params.name)
    }

    fn module_disabled(&self, pdoc: &PermissionsDocument) -> bool {
        pdoc.disabled_modules.contains(&self.params.category)
    }

    pub async fn check(&mut self) {
        if let Some(gid) = self.params.pld.msg.guild_id {
            if let Some(pdoc) = PermissionsDocument::new(&self.params.pld.db, gid.0) {
                let mdl_d = self.module_disabled(&pdoc);
                let cmd_d = self.command_disabled(&pdoc);
                self.permitted = if mdl_d || cmd_d {
                    let mdl_x = self.module_excepted(&pdoc).await;
                    let cmd_x = self.command_excepted(&pdoc).await;
                    if mdl_x {
                        if cmd_d {
                            cmd_x
                        } else {
                            true
                        }
                    } else {
                        cmd_x
                    }
                } else {
                    true
                }
            }
        }
    }

    pub fn ok(&self) -> bool {
        self.permitted
    }
}

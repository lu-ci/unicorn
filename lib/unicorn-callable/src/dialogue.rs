use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::channel::ReactionType;

use unicorn_embed::UnicornEmbed;
use unicorn_utility::user::Avatar;

use crate::error::CallableError;
use crate::payload::CommandPayload;
use std::time::Duration;
use std::ops::Deref;

pub struct Dialogue {
    command_name: String,
    options: Vec<String>,
    pub title: Option<String>,
    pub footer: Option<String>,
    pub description: Option<Box<dyn Fn(&mut CommandPayload) -> String + Sync + Send>>,
    pub timeout: Box<dyn Fn(&mut CommandPayload) -> String + Sync + Send>,
}

impl Dialogue {
    pub fn options(&self) -> Vec<&str> {
        self.options.iter().map(|s| s.as_str()).collect()
    }

    pub async fn show_dialog(&self, pld: &mut CommandPayload) -> Result<DialogResult, CallableError> {
        let mut prompt = CreateMessage::default();
        prompt.embed(|e| {
            e.color(0xF9F9F9);
            if let Some(t) = &self.title {
                e.title(t);
            }
            if let Some(d) = &self.description {
                e.description((d)(pld));
            }
            if let Some(ftr) = &self.footer {
                e.footer(|f| f.text(ftr));
            }
            e.author(|a| {
                a.name(pld.msg.author.name.clone());
                a.icon_url(Avatar::url(pld.msg.author.clone()));
                a
            });
            e
        });

        if let Ok(message) = pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut prompt).await {
            let partial_key = self.key(pld.msg.author.id);
            pld.cache
                .set(partial_key.clone(), format!("u:{}-{}", message.id.to_string(), self.options().join(",")));

            let http = pld.ctx.http.clone();
            let channel = pld.msg.channel_id.clone();

            for icon in self.options() {
                message.react(pld.ctx.http(), ReactionType::Unicode(icon.to_string())).await?;
            }
            let options_filter = self.options.clone();
            let reaction_collector = message.await_reaction(&pld.ctx);
            let reaction = reaction_collector
                .timeout(Duration::from_secs(30))
                .collect_limit(1)
                .added(true)
                .filter(move |r| {
                    if let ReactionType::Unicode(emoji) = r.emoji.clone() {
                        options_filter.contains(&emoji)
                    } else {
                        false
                    }
                })
                .await;

            message.delete(pld.ctx.http()).await?;
            pld.cache.del(partial_key.clone());

            match reaction {
                Some(reaction_action) => {
                    if let serenity::collector::ReactionAction::Added(added_reaction) = reaction_action.deref() {
                        if let ReactionType::Unicode(emoji) = added_reaction.emoji.clone() {
                            match emoji.as_ref() {
                                "✅" => Ok(DialogResult::Ok),
                                "❌" => Ok(DialogResult::Cancel),
                                _ => {
                                    let number = Dialogue::choice(&emoji);
                                    Ok(DialogResult::Number(number))
                                }
                            }
                        } else {
                            Err(CallableError::String("Unrecognised Reaction".to_string()))
                        }
                    } else {
                        Err(CallableError::String("Only Added reactions are recognised".to_string()))
                    }
                }
                None => {
                    let mut response = UnicornEmbed::small_embed("🕙", 0x696969, (self.timeout)(pld));
                    let _ = channel.delete_message(http.clone(), message.id);
                    let _ = channel.send_message(http.clone(), |_| &mut response);
                    Ok(DialogResult::TimeOut)
                }
            }
        } else {
            Err(CallableError::String("Message failed to send".to_string()))
        }
    }

    fn key<Id: Into<u64>>(&self, user: Id) -> String {
        format!("{}:user{}", self.command_name, user.into())
    }

    pub fn is_open<Id: Into<u64>>(&self, pld: &CommandPayload, user: Id) -> bool {
        let partial_key = self.key(user.into());
        pld.cache.get(partial_key) != None
    }

    fn new(command_name: &str) -> Self {
        Self {
            command_name: command_name.to_owned(),
            title: None,
            description: None,
            footer: None,
            timeout: Box::new(|_| "The command timed out".to_owned()),
            options: Vec::new(),
        }
    }

    pub fn ok_cancel(command_name: &str) -> Self {
        let mut me = Self::new(command_name);
        me.options = vec!["✅".to_owned(), "❌".to_owned()];
        me
    }

    pub fn numbered_options(command_name: &str, count: usize) -> Self {
        let mut options = Vec::new();
        let source = vec!["1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣"];
        for i in 0..count {
            match source.get(i) {
                Some(s) => options.push(s.to_string()),
                None => break,
            }
        }
        let mut me = Self::new(command_name);
        me.options = options;
        me
    }

    fn choice(emoji: &str) -> usize {
        match emoji {
            "1⃣" => 1,
            "2⃣" => 2,
            "3⃣" => 3,
            "4⃣" => 4,
            "5⃣" => 5,
            "6⃣" => 6,
            "7⃣" => 7,
            "8⃣" => 8,
            "9⃣" => 9,
            _ => 0,
        }
    }
}

pub enum DialogResult {
    Ok,
    Number(usize),
    Cancel,
    TimeOut,
}

use std::collections::HashMap;
use std::sync::Arc;

use serenity::client::Context;
use serenity::model::channel::{Message, Reaction};

use unicorn_cache::handler::CacheHandler;
use unicorn_config::config::Configuration;
use unicorn_cooldown::UnicornCooldown;
use unicorn_database::DatabaseHandler;
use unicorn_database::settings::GuildSettings;
use unicorn_information::ModuleInformation;
use unicorn_timer::TimerController;

use crate::argument::CommandArguments;

#[derive(Clone)]
pub struct CommandPayload {
    pub db: DatabaseHandler,
    pub meta: HashMap<String, ModuleInformation>,
    pub cfg: Configuration,
    pub cd: UnicornCooldown,
    pub args: CommandArguments,
    pub cache: CacheHandler,
    pub ctx: Context,
    pub msg: Message,
    pub timeouts: Arc<TimerController>,
    pub settings: Option<GuildSettings>,
}

impl CommandPayload {
    pub fn new(
        ctx: &Context,
        meta: &HashMap<String, ModuleInformation>,
        cfg: &Configuration,
        db: &DatabaseHandler,
        cd: &UnicornCooldown,
        cache: &CacheHandler,
        timeout: Arc<TimerController>,
        msg: &Message,
    ) -> Self {
        let settings = if let Some(gid) = msg.guild_id {
            Some(db.clone().get_settings(gid.0))
        } else {
            None
        };
        Self {
            db: db.clone(),
            meta: meta.clone(),
            cfg: cfg.clone(),
            cd: cd.clone(),
            cache: cache.clone(),
            ctx: ctx.clone(),
            msg: msg.clone(),
            args: CommandArguments::new("", Vec::new()),
            timeouts: timeout.clone(),
            settings,
        }
    }

    pub fn with_arguments(mut self, args: CommandArguments) -> CommandPayload {
        self.args = args;
        self
    }

    pub fn get_prefix(&self) -> String {
        match &self.settings {
            Some(settings) => match &settings.prefix {
                Some(pfx) => pfx.to_owned(),
                None => self.db.cfg.preferences.prefix.clone(),
            },
            None => self.db.cfg.preferences.prefix.clone(),
        }
    }
}


#[derive(Clone)]
pub struct ReactionPayload {
    pub db: DatabaseHandler,
    pub meta: HashMap<String, ModuleInformation>,
    pub cfg: Configuration,
    pub cd: UnicornCooldown,
    pub cache: CacheHandler,
    pub ctx: Context,
    pub timeouts: Arc<TimerController>,
    pub reaction: Reaction,
}

impl ReactionPayload {
    pub fn new(
        ctx: &Context,
        meta: &HashMap<String, ModuleInformation>,
        cfg: &Configuration,
        db: &DatabaseHandler,
        cd: &UnicornCooldown,
        cache: &CacheHandler,
        timeouts: Arc<TimerController>,
        reaction: &Reaction,
    ) -> Self {
        Self {
            db: db.clone(),
            meta: meta.clone(),
            cfg: cfg.clone(),
            cd: cd.clone(),
            cache: cache.clone(),
            ctx: ctx.clone(),
            timeouts: timeouts.clone(),
            reaction: reaction.clone(),
        }
    }
}

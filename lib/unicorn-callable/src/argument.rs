use std::collections::HashMap;
use std::fmt::Error;
use std::fmt::Formatter;
use std::sync::Arc;

use log::{error, info};
use serde::{Deserialize, Serialize};

struct MagicBox<Args, Output> {
    core: Box<dyn Fn(Args) -> Output + Send + Sync>,
}

impl<Args, Output> MagicBox<Args, Output> {
    fn new<F: 'static>(function: F) -> Self
    where
        F: Fn(Args) -> Output + Send + Sync,
    {
        Self { core: Box::new(function) }
    }
    fn run(&self, a: Args) -> Output {
        (self.core)(a)
    }
}

impl<Args, Output> std::fmt::Debug for MagicBox<Args, Output> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        f.debug_struct("MagicBox").field("core", &"Boxed function".to_owned()).finish()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CommandArgument {
    optional: bool,
    name: String,
    group: Option<String>,
    allows_spaces: bool,
    has_value: bool,
    is_flag: bool,
    anywhere: bool,
    #[serde(skip_serializing, skip_deserializing)]
    available_when: Option<Arc<MagicBox<String, bool>>>,
}

impl CommandArgument {
    pub fn new(name: &str, optional: bool) -> Self {
        Self {
            name: name.to_string(),
            optional,
            allows_spaces: false,
            has_value: false,
            group: None,
            available_when: None,
            is_flag: false,
            anywhere: false,
        }
    }

    pub fn allows_spaces(mut self) -> CommandArgument {
        self.allows_spaces = true;
        self
    }

    pub fn with_value(mut self) -> CommandArgument {
        if self.is_flag {
            error!("Can't add a value to a flag")
        }
        self.has_value = true;
        self.anywhere = true;
        self
    }

    pub fn group(mut self, group_name: &str) -> CommandArgument {
        self.group = Some(group_name.to_owned());
        self
    }

    pub fn is_flag(mut self) -> CommandArgument {
        if self.has_value {
            error!("Flags can't have values")
        }
        self.is_flag = true;
        self.anywhere = true;
        self
    }

    pub fn available_when<F: 'static>(mut self, check: F) -> CommandArgument
    where
        F: Fn(String) -> bool + Send + Sync,
    {
        if !self.optional {
            error!("available_when parameters must be optional!")
        }
        self.available_when = Some(Arc::new(MagicBox::new(check)));
        self
    }
    pub fn anywhere(mut self) -> CommandArgument {
        if self.available_when.is_none() {
            error!("Anywhere is only valid for filtered arguments")
        } else {
            self.anywhere = true;
        }
        self
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CommandArguments {
    raw: Vec<String>,
    args: Vec<CommandArgument>,
    values: HashMap<String, String>,
    groups: HashMap<String, (String, String)>,
}

impl CommandArguments {
    pub fn new(string: &str, args: Vec<CommandArgument>) -> Self {
        let mut values = HashMap::new();
        let mut groups = HashMap::new();
        let mut raw = Vec::<String>::new();
        let pieces: Vec<&str> = string.split(' ').collect();
        for (pix, piece) in pieces.into_iter().enumerate() {
            if pix != 0 && !piece.is_empty() {
                raw.push(piece.to_owned());
            }
        }
        let mut temp_args = raw.clone();
        let mut args_to_fill = args.clone();
        if args_to_fill.len() > 0 {
            //This loop deals with "Named" arguments, so either flags or with_value. The name of the argument
            //must appear in the argument set
            let mut index_adjustment = 0;
            for (index, arg) in raw.clone().into_iter().enumerate() {
                for (request_index, request) in args_to_fill.clone().into_iter().enumerate() {
                    if index_adjustment > index {
                        continue;
                    }
                    let arg_index = index - index_adjustment;
                    if arg.to_lowercase() == request.name.to_lowercase() && request.has_value && (temp_args.len() > arg_index + 1) {
                        match request.group {
                            Some(group_name) => {
                                if groups.get(&group_name) == None {
                                    temp_args.remove(arg_index);
                                    let value = temp_args.remove(arg_index);
                                    index_adjustment += 2;
                                    groups.insert(group_name.clone(), (request.name.clone(), value.clone()));
                                    values.insert(request.name.clone(), value);
                                    let mut i = args_to_fill.len() - 1;
                                    loop {
                                        if let Some(a) = args_to_fill.get(i) {
                                            if let Some(g) = &a.group {
                                                if g == &group_name {
                                                    args_to_fill.remove(i);
                                                }
                                            }
                                        }
                                        if i == 0 {
                                            break;
                                        }
                                        i -= 1;
                                    }
                                    break;
                                } else {
                                    error!("Groups only accept one of the grouped options");
                                }
                            }
                            None => {
                                temp_args.remove(arg_index);
                                values.insert(request.name.clone(), temp_args.remove(arg_index));
                                index_adjustment += 2;
                                args_to_fill.remove(request_index);
                                break;
                            }
                        }
                    } else if arg.to_lowercase() == request.name.to_lowercase() && request.is_flag {
                        match request.group {
                            Some(group_name) => {
                                if groups.get(&group_name) == None {
                                    temp_args.remove(arg_index);
                                    index_adjustment += 1;
                                    groups.insert(group_name, (request.name.clone(), "true".to_owned()));
                                    values.insert(request.name.clone(), "true".to_owned());
                                    args_to_fill.remove(request_index);
                                    break;
                                } else {
                                    error!("Groups only accept one of the grouped options");
                                }
                            }
                            None => {
                                temp_args.remove(arg_index);
                                index_adjustment += 1;
                                values.insert(request.name.clone(), "true".to_owned());
                                args_to_fill.remove(request_index);
                                break;
                            }
                        }
                    } else if request.anywhere
                        && match request.available_when {
                            Some(f) => f.run(arg.to_owned()),
                            None => false,
                        }
                    {
                        temp_args.remove(index);
                        values.insert(request.name.clone(), arg.to_owned());
                        args_to_fill.remove(request_index);
                        break;
                    }
                }
            }

            //Clean up any unfilled flags or value arguments
            if args_to_fill.len() > 0 {
                let mut i = args_to_fill.len() - 1;
                loop {
                    if let Some(a) = args_to_fill.get(i) {
                        if a.has_value || a.is_flag {
                            args_to_fill.remove(i);
                        }
                    }
                    if i == 0 {
                        break;
                    }
                    i -= 1;
                }
            }

            //This loop processes "Indexed" arguments, arguments managed by the order in which they appear
            let mut i = 0;
            while i < temp_args.len() {
                if let Some(arg) = temp_args.get(i) {
                    let mut arg_processed = false;
                    for (arg_index, request) in args_to_fill.clone().into_iter().enumerate() {
                        if match request.available_when {
                            Some(f) => f.run(arg.to_owned()),
                            None => true,
                        } {
                            match request.group {
                                Some(group_name) => {
                                    if groups.get(&group_name) == None {
                                        if request.allows_spaces {
                                            groups.insert(group_name, (request.name.clone(), temp_args.join(" ")));
                                            values.insert(request.name.clone(), temp_args.join(" "));
                                            temp_args.clear();
                                        } else {
                                            groups.insert(group_name, (request.name.clone(), arg.to_owned()));
                                            values.insert(request.name.clone(), arg.to_owned());
                                            temp_args.remove(0);
                                        }
                                        arg_processed = true;
                                        args_to_fill.remove(arg_index);
                                    } else {
                                        error!("Groups only accept one of the grouped options");
                                    }
                                }
                                None => {
                                    if request.allows_spaces {
                                        values.insert(request.name.clone(), temp_args.join(" "));
                                        temp_args.clear();
                                    } else {
                                        values.insert(request.name.clone(), arg.to_owned());
                                        temp_args.remove(0);
                                    }
                                    arg_processed = true;
                                    args_to_fill.remove(arg_index);
                                }
                            }
                            break;
                        }
                    }
                    if !arg_processed {
                        i += 1
                    }
                }
            }
        }
        Self {
            raw: raw.clone(),
            args,
            values: values.clone(),
            groups: groups.clone(),
        }
    }

    pub fn has_argument(&self, name: &str) -> bool {
        self.values.contains_key(name)
    }

    pub fn get_or_default<T>(&self, name: &str, default: T) -> T
    where
        T: std::str::FromStr,
    {
        if self.has_argument(name) {
            if let Ok(val) = self.get(name).parse::<T>() {
                val
            } else {
                default
            }
        } else {
            default
        }
    }

    pub fn get(&self, name: &str) -> &str {
        match self.values.get(name) {
            Some(value) => value,
            None => "",
        }
    }

    pub fn get_group(&self, name: &str) -> Option<&(String, String)> {
        self.groups.get(&name.to_string())
    }

    pub fn is_empty(&self) -> bool {
        self.values.is_empty()
    }

    pub fn raw(&self) -> Vec<String> {
        self.raw.clone()
    }

    pub fn to_string(&self) -> String {
        self.raw.join(" ")
    }

    pub fn satisfied(&self) -> bool {
        let mut result = true;
        let mut groups = Vec::new();
        for arg in &self.args {
            if !arg.optional {
                if !self.has_argument(&arg.name) {
                    result = false;
                    info!("Missing argument: {}", &arg.name);
                    break;
                }
            }
            if let Some(group_name) = &arg.group {
                groups.push(group_name)
            }
        }
        for group in groups {
            if self.get_group(group.as_str()) == None {
                info!("Missing argument for group: {}", &group);
                result = false;
                break;
            }
        }
        result
    }
}

use std::any::Any;

use unicorn_information::EventInformation;

pub trait UnicornEvent: Any + Send + Sync {
    fn event_name(&self) -> &str;
    fn category(&self) -> &str;
    fn trigger(&self) -> &str;
    fn information(&self) -> EventInformation {
        EventInformation {
            name: self.event_name().to_owned(),
            category: self.category().to_owned(),
            trigger: self.trigger().to_owned(),
        }
    }
}

use bson::*;
use chrono::Utc;
use log::error;
use mongodb;
use mongodb::options::FindOneAndUpdateOptions;
use serde::{Deserialize, Serialize};

use unicorn_database::DatabaseHandler;

#[derive(Debug, Clone)]
pub struct CooldownEntry {
    pub key: String,
    pub stamp: i32,
    pub active: bool,
    pub duration: i32,
}

impl CooldownEntry {
    pub fn new(key: String, stamp: i32) -> Self {
        let now = Utc::now().timestamp() as i32;
        let (active, duration) = if now < stamp { (true, stamp - now) } else { (false, 0) };
        Self {
            key,
            stamp,
            active,
            duration,
        }
    }
}

impl From<CooldownDatabaseEntry> for CooldownEntry {
    fn from(cdbe: CooldownDatabaseEntry) -> Self {
        CooldownEntry::new(cdbe.name, cdbe.end_stamp)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CooldownDatabaseEntry {
    pub name: String,
    pub end_stamp: i32,
}

impl CooldownDatabaseEntry {
    pub fn new(name: String, end_stamp: i32) -> Self {
        Self { name, end_stamp }
    }

    pub fn get(db: &DatabaseHandler, key: String) -> Option<Self> {
        match db.get_client() {
            Some(cli) => {
                let lookup = doc! {"name": &key};
                match cli.database(&db.cfg.database.name).collection("cooldowns").find_one(lookup.clone(), None) {
                    Ok(result) => match result {
                        Some(doc) => {
                            let cdbe: DecoderResult<Self> = bson::from_bson(bson::Bson::Document(doc));
                            match cdbe {
                                Ok(cdbe) => {
                                    let cd = CooldownEntry::from(cdbe.clone());
                                    if !cd.active {
                                        if let Err(_) = cli
                                            .database(&db.cfg.database.name)
                                            .collection("cooldowns")
                                            .delete_one(lookup.clone(), None)
                                        {
                                            error!("Failed clearing inactive cooldown for {}.", &key);
                                        }
                                    }
                                    Some(cdbe)
                                }
                                Err(_) => {
                                    error!("Failed deserializing document for {}.", &key);
                                    None
                                }
                            }
                        }
                        None => None,
                    },
                    Err(_) => {
                        error!("Failed looking up cooldown document for {}.", &key);
                        None
                    }
                }
            }
            None => None,
        }
    }

    pub fn remove(db: &DatabaseHandler, key: String) {
        match db.get_client() {
            Some(cli) => {
                let lookup = doc! {"name": &key};
                if let Err(_) = cli
                    .database(&db.cfg.database.name)
                    .collection("cooldowns")
                    .delete_one(lookup.clone(), None)
                {
                    error!("Failed clearing cooldown for {}.", &key);
                }
            }
            None => error!("Unable to get db client"),
        }
    }

    pub fn to_doc(&self) -> Document {
        doc! {"name": &self.name, "end_stamp": self.end_stamp}
    }

    pub fn save(&self, db: &DatabaseHandler) {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"name": &self.name};
            let update = doc! {"$set": self.to_doc()};
            let options = FindOneAndUpdateOptions::builder().upsert(true).build();
            if let Err(why) = cli
                .database(&db.cfg.database.name)
                .collection("cooldowns")
                .find_one_and_update(lookup, update, options)
            {
                error!("Failed adding the cooldown document for {}: {}", &self.name, why);
            }
        }
    }
}

impl From<CooldownEntry> for CooldownDatabaseEntry {
    fn from(cde: CooldownEntry) -> Self {
        CooldownDatabaseEntry {
            name: cde.key,
            end_stamp: cde.stamp,
        }
    }
}

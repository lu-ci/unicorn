use std::collections::HashMap;

use bson::*;
use log::error;
use mongodb::options::UpdateOptions;
use serde::{Deserialize, Serialize};

use unicorn_cache::handler::CacheHandler;

use crate::DatabaseHandler;

static CACHE_KEY: &str = "userprofile";

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct UserProfile {
    pub user_id: i64,
    pub sabotaged: bool,
    upgrades: HashMap<String, i32>,
    //pub machines: ??,
    pub spouses: Vec<u64>,
    pub virgin: bool,
    pub prefix: Option<String>,
}

impl UserProfile {
    pub fn new(uid: u64) -> Self {
        let mut profile = Self::default();
        profile.user_id = uid as i64;
        profile
    }

    fn to_bson(&self) -> Option<Bson> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs),
            Err(why) => {
                error!("Failed serializing profile BSON: {}", why);
                None
            }
        }
    }

    pub fn get_upgrade(&self, upgrade: &str) -> i32 {
        if let Some(val) = self.upgrades.get(upgrade) {
            *val as i32
        } else {
            0
        }
    }

    pub fn set_upgrade(&mut self, upgrade: &str, value: i32) {
        if let Some(current_level) = self.upgrades.get_mut(upgrade) {
            *current_level = value;
        } else {
            self.upgrades.insert(upgrade.to_owned(), value);
        }
    }

    fn get_from_cache(cache: &CacheHandler, uid: u64) -> Option<Self> {
        match cache.get(format!("{}:{}", CACHE_KEY, uid)) {
            Some(profile_body) => {
                let res = serde_json::from_str::<Self>(&profile_body);
                match res {
                    Ok(profile) => Some(profile),
                    Err(why) => {
                        error!("Cached profile deserialization failed: {}", why);
                        None
                    }
                }
            }
            None => None,
        }
    }

    fn save_to_cache(cache: &CacheHandler, profile: Self) {
        match serde_json::to_string(&profile) {
            Ok(profile_body) => {
                cache.set(format!("{}:{}", CACHE_KEY, &profile.user_id), profile_body);
            }
            Err(why) => {
                error!("Cached profile serialization failed: {}", why);
            }
        }
    }

    fn get_from_database(db: &DatabaseHandler, uid: u64) -> Option<Self> {
        if let Some(cli) = db.get_client() {
            {
                let lookup = doc! {"user_id": uid as i64};
                match cli
                    .database(&db.cfg.database.name)
                    .collection("user_profiles")
                    .find_one(lookup, None)
                {
                    Ok(sopt) => match sopt {
                        Some(sdoc) => {
                            let sdoc = bson::Bson::Document(sdoc);
                            let sres: DecoderResult<Self> = bson::from_bson(sdoc);
                            match sres {
                                Ok(profile) => Some(profile),
                                Err(_) => None,
                            }
                        }
                        None => None,
                    },
                    Err(why) => {
                        error!("Failed retrieving profile: {}", why);
                        None
                    }
                }
            }
        } else {
            None
        }
    }

    fn save_to_database(db: &DatabaseHandler, profile: Self) {
        if let Some(bdat) = profile.to_bson() {
            let mut data = doc! {};
            data.insert("$set", bdat);
            let lookup = doc! {"user_id": profile.user_id};
            let opts = UpdateOptions::builder().upsert(true).build();
            if let Some(cli) = db.get_client() {
                if let Err(why) = cli
                    .database(&db.cfg.database.name)
                    .collection("user_profiles")
                    .update_one(lookup, data, opts)
                {
                    error!(
                        "Failed updating profile for user {}: {}",
                        profile.user_id, why
                    );
                }
            }
        }
    }

    pub fn get(db: &DatabaseHandler, uid: u64) -> Self {
        match Self::get_from_cache(&db.cache, uid) {
            Some(profile) => profile,
            None => match Self::get_from_database(db, uid) {
                Some(profile) => {
                    Self::save_to_cache(&db.cache, profile.clone());
                    profile
                }
                None => Self::new(uid),
            },
        }
    }

    pub fn save(&self, db: &DatabaseHandler) {
        Self::save_to_cache(&db.cache, self.clone());
        Self::save_to_database(db, self.clone());
    }
}

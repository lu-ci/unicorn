use mongodb;
use r2d2;

#[derive(Debug)]
pub enum UnicornDatabaseError {
    R2D2Error(r2d2::Error),
    MongoDBError(mongodb::error::Error),
}

impl From<r2d2::Error> for UnicornDatabaseError {
    fn from(err: r2d2::Error) -> Self {
        UnicornDatabaseError::R2D2Error(err)
    }
}

impl From<mongodb::error::Error> for UnicornDatabaseError {
    fn from(err: mongodb::error::Error) -> Self {
        UnicornDatabaseError::MongoDBError(err)
    }
}

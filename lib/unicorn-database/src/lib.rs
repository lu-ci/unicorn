use std::sync::Arc;

use bson::*;
use log::{error, info};
use mongodb::options::{ClientOptions, UpdateOptions};
use mongodb::Client;

use error::UnicornDatabaseError;
use unicorn_cache::handler::CacheHandler;
use unicorn_config::config::Configuration;
use unicorn_resource::origin::ResourceOrigin;
use unicorn_resource::UnicornResource;

use crate::profile::UserProfile;
use crate::settings::GuildSettings;

pub mod error;
pub mod profile;
pub mod settings;

#[derive(Debug, Clone)]
pub struct DatabaseHandler {
    pub cfg: Arc<Configuration>,
    pub cache: Arc<CacheHandler>,
    pub client: Client,
}

impl DatabaseHandler {
    pub fn new(cfg: Arc<Configuration>, cache: Arc<CacheHandler>) -> Result<Self, UnicornDatabaseError> {
        let params = if cfg.database.auth {
            format!(
                "mongodb://{}:{}@{}:{}/admin",
                &cfg.database.username, &cfg.database.password, &cfg.database.host, &cfg.database.port
            )
        } else {
            format!("mongodb://{}:{}", &cfg.database.host, &cfg.database.port,)
        };

        let options = ClientOptions::parse(&params)?;

        let client = Client::with_options(options)?;

        info!("Testing database connection");
        match client.database(&cfg.database.name).list_collections(None, None) {
            Ok(_) => {
                info!("Connection Ok");
                Ok(Self { cfg, cache, client })
            }
            Err(error) => Err(UnicornDatabaseError::from(error)),
        }
    }

    pub fn get_client(&self) -> Option<Client> {
        Some(self.client.clone())
    }

    pub fn get_resource(&self, name: &str, uid: u64) -> UnicornResource {
        let mut resource = if let Some(cli) = self.get_client() {
            let coll = format!("{}_resource", name.to_lowercase());
            if let Ok(docres) = cli
                .database(&self.cfg.database.name)
                .collection(&coll)
                .find_one(doc! {"user_id": uid}, None)
            {
                if let Some(docopt) = docres {
                    let bdoc = bson::Bson::Document(docopt);
                    let dres: DecoderResult<UnicornResource> = bson::from_bson(bdoc);
                    if let Ok(res) = dres {
                        res
                    } else {
                        UnicornResource::default()
                    }
                } else {
                    UnicornResource::default()
                }
            } else {
                UnicornResource::default()
            }
        } else {
            UnicornResource::default()
        };
        if resource.user_id == 0 {
            resource.user_id = uid as i64;
        }
        resource
    }

    pub fn del_resource(&self, name: &str, uid: u64, amount: i64, trigger: &str, origin: ResourceOrigin, ranked: bool) {
        let mut resource = self.get_resource(name, uid);
        resource.expenses.add_function(trigger.to_owned(), amount);
        resource.del(|f| {
            match origin {
                ResourceOrigin::UserId(id) => f.user = Some(u64::from(id)),
                ResourceOrigin::GuildId(id) => f.channel = Some(u64::from(id)),
                ResourceOrigin::ChannelId(id) => f.guild = Some(u64::from(id)),
            }
            f.amount = amount;
            f.ranked = ranked;
            f
        });
        self.set_resource(name, uid, resource);
    }

    pub fn add_resource(&self, name: &str, uid: u64, amount: i64, trigger: &str, origin: ResourceOrigin, ranked: bool) {
        let mut resource = self.get_resource(name, uid);
        resource.origins.add_function(trigger.to_owned(), amount);
        resource.add(|f| {
            match origin {
                ResourceOrigin::UserId(id) => f.user = Some(u64::from(id)),
                ResourceOrigin::GuildId(id) => f.channel = Some(u64::from(id)),
                ResourceOrigin::ChannelId(id) => f.guild = Some(u64::from(id)),
            }
            f.amount = amount;
            f.ranked = ranked;
            f
        });
        self.set_resource(name, uid, resource);
    }

    pub fn set_resource(&self, name: &str, uid: u64, res: UnicornResource) {
        if let Some(cli) = self.get_client() {
            if let Some(bdat) = res.to_bson() {
                let coll = format!("{}_resource", name.to_lowercase());
                let mut data = doc! {};
                data.insert("$set", bdat);
                let lookup = doc! {"user_id": uid};
                let opts = UpdateOptions::builder().upsert(true).build();
                if let Err(why) = cli.database(&self.cfg.database.name).collection(&coll).update_one(lookup, data, opts) {
                    error!("Failed updating {} resource for user {}: {}", &name, &uid, why);
                }
            }
        }
    }

    pub fn get_settings(&self, gid: u64) -> GuildSettings {
        GuildSettings::get(self, gid)
    }

    pub fn get_profile(&self, uid: u64) -> UserProfile {
        UserProfile::get(self, uid)
    }
}

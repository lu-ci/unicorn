use bson::*;
use log::error;
use mongodb::options::UpdateOptions;
use serde::{Deserialize, Serialize};

use unicorn_cache::handler::CacheHandler;

use crate::DatabaseHandler;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct GuildSettings {
    pub server_id: i64,
    #[serde(default = "GuildSettings::default_str_option")]
    pub prefix: Option<String>,
    #[serde(default = "GuildSettings::default_bool_option")]
    pub unflip: Option<bool>,
}

// Default Methods
impl GuildSettings {
    fn default_str_option() -> Option<String> {
        None
    }

    fn default_bool_option() -> Option<bool> {
        Some(false)
    }
}

// Useable Methods
impl GuildSettings {
    pub fn new(gid: u64) -> Self {
        let mut settings = Self::default();
        settings.server_id = gid as i64;
        settings
    }

    fn to_bson(&self) -> Option<Bson> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs),
            Err(why) => {
                error!("Failed serializing settings BSON: {}", why);
                None
            }
        }
    }

    fn get_from_cache(cache: &CacheHandler, gid: u64) -> Option<Self> {
        match cache.get(format!("settings:{}", gid)) {
            Some(settings_body) => {
                let res = serde_json::from_str::<GuildSettings>(&settings_body);
                match res {
                    Ok(settings) => Some(settings),
                    Err(why) => {
                        error!("Cached settings deserialization failed: {}", why);
                        None
                    }
                }
            }
            None => None,
        }
    }

    fn save_to_cache(cache: &CacheHandler, settings: Self) {
        match serde_json::to_string(&settings) {
            Ok(settings_body) => {
                cache.set(format!("settings:{}", &settings.server_id), settings_body);
            }
            Err(why) => {
                error!("Cached settings serialization failed: {}", why);
            }
        }
    }

    fn get_from_database(db: &DatabaseHandler, gid: u64) -> Option<Self> {
        if let Some(cli) = db.get_client() {
            {
                let lookup = doc! {"server_id": gid as i64};
                match cli.database(&db.cfg.database.name).collection("server_settings").find_one(lookup, None) {
                    Ok(sopt) => match sopt {
                        Some(sdoc) => {
                            let sdoc = bson::Bson::Document(sdoc);
                            let sres: DecoderResult<Self> = bson::from_bson(sdoc);
                            match sres {
                                Ok(sset) => Some(sset),
                                Err(_) => None,
                            }
                        }
                        None => None,
                    },
                    Err(why) => {
                        error!("Failed retrieving settings: {}", why);
                        None
                    }
                }
            }
        } else {
            None
        }
    }

    fn save_to_database(db: &DatabaseHandler, settings: Self) {
        if let Some(bdat) = settings.to_bson() {
            let mut data = doc! {};
            data.insert("$set", bdat);
            let lookup = doc! {"server_id": settings.server_id};
            let opts = UpdateOptions::builder().upsert(true).build();
            if let Some(cli) = db.get_client() {
                if let Err(why) = cli
                    .database(&db.cfg.database.name)
                    .collection("server_settings")
                    .update_one(lookup, data, opts)
                {
                    error!("Failed updating settings for guild {}: {}", settings.server_id, why);
                }
            }
        }
    }

    pub fn get(db: &DatabaseHandler, gid: u64) -> Self {
        match Self::get_from_cache(&db.cache, gid) {
            Some(settings) => settings,
            None => match Self::get_from_database(db, gid) {
                Some(settings) => {
                    Self::save_to_cache(&db.cache, settings.clone());
                    settings
                }
                None => Self::new(gid),
            },
        }
    }

    pub fn save(&self, db: &DatabaseHandler) {
        Self::save_to_cache(&db.cache, self.clone());
        Self::save_to_database(db, self.clone());
    }
}

use std::any::Any;
use std::fmt::Debug;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use dyn_clone::DynClone;
use log::info;
use serde::de::DeserializeOwned;

use crate::error::UnicornConfigError;

pub trait ConfigLoader: DynClone + Any + Sync + Send + Debug {
    fn file_name() -> String
        where
            Self: DeserializeOwned;

    fn env_name(name: String) -> String
        where
            Self: DeserializeOwned,
    {
        format!("UNC_{}_{}", Self::file_name(), name).to_uppercase()
    }

    fn exists() -> ExistResult
        where
            Self: DeserializeOwned,
    {
        ExistResult::exists(Self::file_name())
    }

    fn default() -> Self
        where
            Self: DeserializeOwned;

    fn as_any(&self) -> &dyn Any;

    fn new() -> Result<Self, UnicornConfigError>
        where
            Self: DeserializeOwned,
    {
        let exists = Self::exists();
        if exists.exists {
            Ok(match exists.extension.as_ref() {
                "yml" => Self::from_yaml(exists)?,
                "yaml" => Self::from_yaml(exists)?,
                "json" => Self::from_json(exists)?,
                _ => {
                    info!("unrecognised extension: {}", Self::file_name());
                    Self::default()
                }
            })
        } else {
            info!("File not found: {}", Self::file_name());
            Ok(Self::default())
        }
    }

    fn from_yaml(exist: ExistResult) -> Result<Self, UnicornConfigError>
        where
            Self: DeserializeOwned,
    {
        let result: Self = serde_yaml::from_reader(exist.get_reader()?)?;
        Ok(result)
    }

    fn from_json(exist: ExistResult) -> Result<Self, UnicornConfigError>
        where
            Self: DeserializeOwned,
    {
        let result: Self = serde_json::from_reader(exist.get_reader()?)?;
        Ok(result)
    }

    fn describe(&self) {}
}

pub struct ExistResult {
    pub exists: bool,
    pub location: String,
    pub extension: String,
}

impl ExistResult {
    fn root() -> String {
        "conf".to_owned()
    }

    fn extensions() -> Vec<String> {
        vec!["yml".to_owned(), "yaml".to_owned(), "json".to_owned()]
    }

    pub fn exists(name: String) -> ExistResult {
        let mut result = ExistResult {
            exists: false,
            location: "".to_owned(),
            extension: "".to_owned(),
        };
        for extension in Self::extensions() {
            let path_string = format!("{}/{}.{}", Self::root(), &name, &extension);
            let path = Path::new(&path_string);
            if path.exists() {
                result.exists = true;
                result.location = path_string;
                result.extension = extension;
                break;
            }
        }
        result
    }

    pub fn get_file(&self) -> Result<File, UnicornConfigError> {
        Ok(File::open(self.location.clone())?)
    }

    pub fn get_reader(&self) -> Result<BufReader<File>, UnicornConfigError> {
        Ok(BufReader::new((&self).get_file()?))
    }
}

use std::any::Any;

use log::{info, warn};
use serde::Deserialize;

use unicorn_utility::hash::UnicornHasher;

use crate::common::ConfigLoader;

#[derive(Debug, Clone, Deserialize)]
pub struct DiscordConfig {
    #[serde(default = "DiscordConfig::default_token")]
    pub token: String,
    #[serde(default = "DiscordConfig::default_owners")]
    pub owners: Vec<u64>,
}

impl ConfigLoader for DiscordConfig {
    fn file_name() -> String {
        "discord".to_owned()
    }

    fn default() -> Self {
        Self {
            token: Self::default_token(),
            owners: Self::default_owners(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        if &self.token != "" {
            info!(
                "Token Hash: {}",
                UnicornHasher::calculate_u64_hash(&self.token)
            );
        } else {
            warn!("Token: The token is empty!");
        }
        let mut id_strings: Vec<String> = Vec::new();
        for oid in &self.owners {
            id_strings.push(oid.to_string())
        }
        info!("Owner IDs: {}", id_strings.join(", "));
    }
}

impl DiscordConfig {
    fn default_token() -> String {
        std::env::var(Self::env_name("token".to_owned())).unwrap_or_else(|_| "".to_owned())
    }

    fn default_owners() -> Vec<u64> {
        match std::env::var(Self::env_name("owners".to_owned())) {
            Ok(owner_str) => {
                let owner_pieces: Vec<&str> = owner_str.split(',').collect();
                let mut owners_env: Vec<u64> = Vec::new();
                for owner_piece in owner_pieces {
                    if let Ok(oid) = owner_piece.trim().parse::<u64>() {
                        owners_env.push(oid);
                    }
                }
                owners_env
            }
            Err(_) => vec![137_951_917_644_054_529],
        }
    }
}

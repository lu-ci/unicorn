use std::any::Any;

use log::info;
use serde::Deserialize;

use crate::common::ConfigLoader;

#[derive(Debug, Clone, Deserialize)]
pub struct ChannelsConfig {
    #[serde(default = "ChannelsConfig::default_error")]
    pub error: Option<u64>,
    #[serde(default = "ChannelsConfig::default_movement")]
    pub movement: Option<u64>,
    #[serde(default = "ChannelsConfig::default_anticheat")]
    pub anticheat: Option<u64>,
}

impl ConfigLoader for ChannelsConfig {
    fn file_name() -> String {
        "preferences_channels".to_owned()
    }

    fn default() -> Self {
        Self {
            error: Self::default_error(),
            movement: Self::default_movement(),
            anticheat: Self::default_anticheat(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        info!(
            "Error: {}",
            match &self.error {
                Some(val) => val.to_string(),
                None => "Unset".to_owned(),
            }
        );
        info!(
            "Movement: {}",
            match &self.movement {
                Some(val) => val.to_string(),
                None => "Unset".to_owned(),
            }
        );
        info!(
            "Anti-Cheat: {}",
            match &self.anticheat {
                Some(val) => val.to_string(),
                None => "Unset".to_owned(),
            }
        );
    }
}

impl ChannelsConfig {
    fn default_env_u64(env_name: &'static str) -> Option<u64> {
        match std::env::var(Self::env_name(env_name.to_owned())) {
            Ok(var_val) => match var_val.parse::<u64>() {
                Ok(chn_id) => Some(chn_id),
                Err(_) => None,
            },
            Err(_) => None,
        }
    }

    pub fn default_error() -> Option<u64> {
        Self::default_env_u64("error")
    }

    pub fn default_movement() -> Option<u64> {
        Self::default_env_u64("movement")
    }

    pub fn default_anticheat() -> Option<u64> {
        Self::default_env_u64("anticheat")
    }
}

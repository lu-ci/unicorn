use unicorn_database::error::UnicornDatabaseError;

#[derive(Debug)]
pub enum UnicornClientError {
    SerenityError(serenity::Error),
    UnicornDBError(UnicornDatabaseError),
}

impl From<serenity::Error> for UnicornClientError {
    fn from(err: serenity::Error) -> Self {
        UnicornClientError::SerenityError(err)
    }
}

impl From<UnicornDatabaseError> for UnicornClientError {
    fn from(err: UnicornDatabaseError) -> Self {
        UnicornClientError::UnicornDBError(err)
    }
}

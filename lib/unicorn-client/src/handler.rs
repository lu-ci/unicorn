use std::sync::Arc;
use std::time::Duration;

use fern::Dispatch;
use log::info;
use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::model::channel::Reaction;
use serenity::model::channel::ReactionType;
use serenity::model::gateway::Ready;
use serenity::prelude::EventHandler;

use unicorn_cache::handler::CacheHandler;
use unicorn_callable::argument::CommandArguments;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_config::config::Configuration;
use unicorn_cooldown::UnicornCooldown;
use unicorn_database::DatabaseHandler;
use unicorn_error::UnicornError;
use unicorn_modman::manager::ModuleManager;
use unicorn_timer::TimerController;

use crate::command::CommandQuery;
use crate::error::UnicornClientError;
use crate::state::ClientState;

pub struct UnicornHandler {
    db: Arc<DatabaseHandler>,
    cd: Arc<UnicornCooldown>,
    cfg: Arc<Configuration>,
    state: Arc<ClientState>,
    cache: Arc<CacheHandler>,
    modman: Arc<ModuleManager>,
    timer: Arc<TimerController>,
}

impl UnicornHandler {
    pub fn new(cfg: &mut Configuration) -> Result<Self, UnicornClientError> {
        let cache = Arc::new(CacheHandler::new(Arc::new(cfg.cache.clone())));
        let db = Arc::new(DatabaseHandler::new(Arc::new(cfg.clone()), cache.clone())?);
        let mut proto_modman = ModuleManager::new();
        let cd = Arc::new(UnicornCooldown::new(db.clone(), cache.clone()));
        let mut config_builder = ConfigurationBuilder::new(Arc::new(cfg.clone()));
        proto_modman.load_commands(&db, &mut config_builder);
        cfg.add_custom(config_builder);
        let modman = Arc::new(proto_modman);
        let state = Arc::new(ClientState::default());
        let mut timer = TimerController::new(500, 4);
        &timer.start();

        Ok(Self {
            db,
            cd,
            cfg: Arc::new(cfg.clone()),
            state,
            cache,
            modman,
            timer: Arc::new(timer),
        })
    }

    fn init_logging() {
        if let Ok(logfile) = fern::log_file(format!("logs/unc_{}.log", chrono::Local::now().format("%Y-%m-%d"))) {
            let _ = Dispatch::new()
                .format(|out, message, record| {
                    out.finish(format_args!(
                        "{}[{}][{}] {}",
                        chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                        record.target(),
                        record.level(),
                        message
                    ))
                })
                .level(log::LevelFilter::Info)
                .chain(std::io::stdout())
                .chain(logfile)
                .apply();
        }
    }

    fn ci_check(&self) {
        if std::env::var("CI").is_ok() {
            let timeout = 10;
            info!("CI environment detected, shutting down in {}s...", timeout);
            std::thread::sleep(Duration::from_secs(timeout));
            info!("Shutting down...");
            std::process::exit(0);
        }
    }

    fn all_ready(&self, _ctx: Context) {
        info!("All shards confirmed ready.");
        self.ci_check();
    }

    async fn log_command(pld: &CommandPayload, cmd: &str) {
        let user = format!("{}#{:0>4} [{}]", pld.msg.author.name, pld.msg.author.discriminator, pld.msg.author.id);
        let channel = format!(
            "#{} [{}]",
            pld.msg
                .channel_id
                .name(&pld.ctx.cache)
                .await
                .unwrap_or_else(|| "DIRECT MESSAGE".to_owned()),
            pld.msg.channel_id
        );
        let guild = format!(
            "{} [{}]",
            match pld.msg.guild(&pld.ctx.cache).await {
                Some(gld) => {
                    gld.name
                }
                None => "DIRECT MESSAGE".to_owned(),
            },
            match pld.msg.guild_id {
                Some(gid) => format!("{}", gid),
                None => "NOT FOUND".to_owned(),
            }
        );
        if pld.args.is_empty() {
            info!("CMD: {} | USR: {} | CHN: {} | GLD: {}", cmd.to_uppercase(), user, channel, guild)
        } else {
            info!(
                "CMD: {} | USR: {} | CHN: {} | GLD: {} | ARGS: {}",
                cmd.to_uppercase(),
                user,
                channel,
                guild,
                pld.args.to_string()
            )
        }
    }
}

#[serenity::async_trait]
impl EventHandler for UnicornHandler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.author.bot {
            return;
        }

        UnicornHandler::init_logging();

        let mut pld = CommandPayload::new(
            &ctx,
            &self.modman.information,
            &self.cfg,
            &self.db,
            &self.cd,
            &self.cache,
            self.timer.clone(),
            &msg,
        );

        let qry = CommandQuery::new(&pld.get_prefix(), &msg);
        if qry.ok {
            if let Some(cmd) = &self.modman.find_command(&qry.name) {
                let arguments = CommandArguments::new(&msg.content, cmd.parameters());
                pld = pld.with_arguments(arguments);
                Self::log_command(&pld, &cmd.command_name()).await;
                if let Err(why) = cmd.run(&mut pld).await {
                    let err = UnicornError::new(cmd.information(), &mut pld, why);
                    err.handle().await;
                }
            }
        }
    }

    async fn reaction_add(&self, ctx: Context, reaction: Reaction) {
        if ctx.cache.current_user().await.id == reaction.user_id {
            return;
        }

        UnicornHandler::init_logging();

        let mut pld = ReactionPayload::new(
            &ctx,
            &self.modman.information,
            &self.cfg,
            &self.db,
            &self.cd,
            &self.cache,
            self.timer.clone(),
            &reaction,
        );
        if let ReactionType::Unicode(emoji) = &reaction.emoji {
            if let Some(reacts) = &self.modman.find_reaction(emoji) {
                for react in reacts {
                    if let Err(why) = react.run(&mut pld).await {
                        println!("{}", why);
                    }
                }
            }
        }
    }

    async fn ready(&self, ctx: Context, data: Ready) {
        match data.shard {
            Some(sharding) => {
                let shard = sharding[0];
                let total = sharding[1];
                self.state.add_shard(shard);
                info!("Shard #{} ({}/{}) ready.", shard, self.state.count(), total);
                if self.state.ready(total) {
                    self.all_ready(ctx);
                }
            }
            None => {
                info!("Main shard ready.");
                self.all_ready(ctx);
            }
        }
    }
}

use bson::*;
use log::{debug, error};
use serenity::http::CacheHttp;

use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_database::DatabaseHandler;
use unicorn_information::CommandInformation;
use unicorn_utility::hash::UnicornHasher;

use crate::entry::ErrorEntry;

mod entry;

const ERROR_EMOTE: char = '❗';

pub struct UnicornError {
    pub cmd: CommandInformation,
    pub pld: CommandPayload,
    pub err: CallableError,
}

impl UnicornError {
    pub fn new(cmd: CommandInformation, pld: &mut CommandPayload, err: CallableError) -> Self {
        Self { cmd, pld: pld.clone(), err }
    }

    pub async fn handle(&self) {
        self.log();
        self.save().await;
        self.react().await;
        self.notify().await;
        self.report().await;
    }

    pub fn log(&self) {
        error!("ERR: {:?} | TKN: {} | MSG: {}", &self.err, self.token(), self.err)
    }

    pub fn get(db: &DatabaseHandler, token: String) -> Option<ErrorEntry> {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"token": token};
            let entry_result = cli.database(&db.cfg.database.name).collection("errors").find_one(lookup, None);
            match entry_result {
                Ok(entry_bson) => {
                    if let Some(entry_bson) = entry_bson {
                        let decored_result: DecoderResult<ErrorEntry> = bson::from_bson(Bson::Document(entry_bson));
                        match decored_result {
                            Ok(error_entry) => Some(error_entry),
                            Err(why) => {
                                error!("Error entry deserialization failure: {:?}", why);
                                None
                            }
                        }
                    } else {
                        None
                    }
                }
                Err(why) => {
                    error!("Error entry lookup failure: {:?}", why);
                    None
                }
            }
        } else {
            None
        }
    }

    pub async fn save(&self) {
        if let Some(cli) = self.pld.db.get_client() {
            if let Some(entry) = ErrorEntry::new(&self).await.to_doc() {
                if let Err(why) = cli.database(&self.pld.db.cfg.database.name).collection("errors").insert_one(entry, None) {
                    error!("Failed saving the error document: {}", why);
                }
            } else {
                error!("Failed creating the error entry");
            }
        }
    }

    pub fn token(&self) -> String {
        let marker = format!(
            "{}_{}_{}_{}",
            match self.pld.msg.guild_id {
                Some(gid) => gid.0,
                None => 0,
            },
            self.pld.msg.channel_id.0,
            self.pld.msg.author.id.0,
            self.pld.msg.id.0
        );
        UnicornHasher::calculate_md5_hash(marker.into_bytes())
    }

    pub async fn react(&self) {
        if let Err(why) = self.pld.msg.react(self.pld.ctx.http(), ERROR_EMOTE).await {
            debug!("Failed reacting with an error indicator: {}", why);
        }
    }

    pub async fn notify(&self) {
        if let Err(why) = self
            .pld
            .msg
            .channel_id
            .send_message(self.pld.ctx.http(), |m| {
                m.embed(|e| {
                    e.color(0xbe_19_31);
                    e.title(format!("{} An Unexpected Error Occurred", ERROR_EMOTE));
                    e.description(format!(
                        "{} {} {} {} {}",
                        "Something seems to have gone wrong.",
                        "Please be patient while we work on fixing the issue.",
                        "The error has been relayed to the developers.",
                        "If you feel like dropping by and asking about it,",
                        format!("the invite link is in the **{}help** command", self.pld.get_prefix())
                    ));
                    e.footer(|f| {
                        f.text(format!("Token: {}", self.token()));
                        f
                    });
                    e
                });
                m
            })
            .await
        {
            debug!("Failed delivering the error message: {}", why);
        }
    }

    pub async fn report(&self) {
        if let Some(ecid) = self.pld.db.cfg.preferences.channels.error {
            if let Some(echn) = self.pld.ctx.cache.channel(ecid).await {
                let ecmd = format!(
                    "Command: **{}**\nCategory: **{}**\nMessage: **{}**\nArguments: **{}**",
                    self.cmd.name,
                    self.cmd.category,
                    self.pld.msg.id.0,
                    if self.pld.args.is_empty() {
                        "<no arguments>".to_owned()
                    } else {
                        self.pld.args.to_string()
                    }
                );
                let (gid, gnam) = match self.pld.msg.guild(&self.pld.ctx.cache).await {
                    Some(gld) => (gld.id.0, gld.name.clone()),
                    None => (0, "<unknown guild>".to_owned()),
                };
                let eorg = format!(
                    "Author: **{}**#{:0>4}\n- {}\nChannel: **#{}**\n- {}\nGuild: **{}**\n- {}",
                    self.pld.msg.author.name,
                    self.pld.msg.author.discriminator,
                    self.pld.msg.author.id.0,
                    self.pld
                        .msg
                        .channel_id
                        .name(&self.pld.ctx.cache)
                        .await
                        .unwrap_or_else(|| "<unknown channel>".to_owned()),
                    self.pld.msg.channel_id.0,
                    gnam,
                    gid
                );
                let etrc = format!("```hs\n{:#?}\n```", self.err);
                if let Err(why) = echn
                    .id()
                    .send_message(self.pld.ctx.http(), |m| {
                        m.embed(|e| {
                            e.color(0xbe_19_31);
                            e.title(format!("{} Error: {}", ERROR_EMOTE, self.token()));
                            e.field("Call", ecmd, true);
                            e.field("Origin", eorg, true);
                            e.field("Details", etrc, false);
                            e.timestamp(format!("{}", chrono::Utc::now().format("%Y-%m-%dT%H:%M:%S")));
                            e
                        });
                        m
                    })
                    .await
                {
                    debug!("Failed reporting the error: {}", why);
                }
            }
        }
    }
}

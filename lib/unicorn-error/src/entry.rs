use bson::ordered::OrderedDocument;
use log::error;
use serde::{Deserialize, Serialize};

use unicorn_callable::argument::CommandArguments;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::UnicornError;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ErrorEntry {
    pub token: String,
    pub error: ErrorTrace,
    pub message: ErrorMessage,
    pub user: ErrorGeneric,
    pub guild: ErrorGeneric,
    pub channel: ErrorGeneric,
}

impl ErrorEntry {
    pub async fn new(err: &UnicornError) -> Self {
        Self {
            token: err.token(),
            error: ErrorTrace::new(&err.err),
            message: ErrorMessage::new(&err),
            user: ErrorGeneric::from_user(&err.pld),
            guild: ErrorGeneric::from_guild(&err.pld).await,
            channel: ErrorGeneric::from_channel(&err.pld).await,
        }
    }

    pub fn to_doc(&self) -> Option<OrderedDocument> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs.as_document()?.clone()),
            Err(why) => {
                error!("Failed encoding error entry: {}", why);
                None
            }
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ErrorTrace {
    pub error: String,
    pub trace: String,
    pub message: String,
}

impl ErrorTrace {
    pub fn new(err: &CallableError) -> Self {
        Self {
            error: format!("{:?}", &err),
            trace: format!("{:#?}", &err),
            message: format!("{}", &err),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ErrorMessage {
    pub id: i64,
    pub cmd: ErrorCommand,
}

impl ErrorMessage {
    pub fn new(err: &UnicornError) -> Self {
        Self {
            id: err.pld.msg.id.0 as i64,
            cmd: ErrorCommand::new(err),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ErrorCommand {
    pub name: String,
    pub category: String,
    pub args: CommandArguments,
}

impl ErrorCommand {
    pub fn new(err: &UnicornError) -> Self {
        Self {
            name: err.cmd.name.clone(),
            category: err.cmd.category.clone(),
            args: err.pld.args.clone(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ErrorGeneric {
    pub id: i64,
    pub name: String,
}

impl ErrorGeneric {
    pub fn new(id: u64, name: String) -> Self {
        Self { id: id as i64, name }
    }

    pub async fn from_guild(pld: &CommandPayload) -> Self {
        let (gid, gnam) = match pld.msg.guild(pld.ctx.cache.clone()).await {
            Some(gld) => (gld.id.0, gld.name.clone()),
            None => (0, "<unknown guild>".to_owned()),
        };
        Self::new(gid, gnam)
    }

    pub async fn from_channel(pld: &CommandPayload) -> Self {
        let cnam = match pld.msg.channel_id.name(pld.ctx.cache.clone()).await {
            Some(cnam) => cnam,
            None => "<unknown channel>".to_owned(),
        };
        Self::new(pld.msg.channel_id.0, cnam)
    }

    pub fn from_user(pld: &CommandPayload) -> Self {
        let unam = format!("{}#{:0>4}", pld.msg.author.name, pld.msg.author.discriminator,);
        Self::new(pld.msg.author.id.0, unam)
    }
}

pub struct Table;

impl Table {
    pub fn format_table(headers: Option<Vec<&str>>, data: Vec<Vec<String>>, formatting: Option<TableFormatting>) -> String {
        let mut column_widths = Vec::new();
        {
            if let Some(headings) = &headers {
                for header in headings {
                    column_widths.push(header.chars().count());
                }
            }
        }
        for row in &data {
            for (idx, field) in row.iter().enumerate() {
                match column_widths.get_mut(idx) {
                    Some(width) => *width = std::cmp::max(*width, field.chars().count()),
                    None => column_widths.push(field.chars().count())
                }
            }
        }
        let mut table_width = column_widths.iter().sum();
        table_width += (column_widths.len() * 3) + 1;
        let mut output = "-".repeat(table_width);
        {
            if let Some(headings) = headers {
                output.push_str("\n");
                for (idx, header) in headings.iter().enumerate() {
                    if let Some(width) = column_widths.get(idx) {
                        output += Table::format_line(header, width, &formatting, idx).as_str();
                    }
                }
                output += "|\n";
                output += "-".repeat(table_width).as_str();
            }
        }
        output += "\n";
        for row in &data {
            for (idx, field) in row.iter().enumerate() {
                if let Some(width) = column_widths.get(idx) {
                    output += Table::format_line(field, width, &formatting, idx).as_str();
                }
            }
            output += "|\n";
        }
        output += "-".repeat(table_width).as_str();
        output
    }

    fn format_line(text: &str, width: &usize, formatting: &Option<TableFormatting>, column_index: usize) -> String {
        match formatting {
            None => format!("| {:1$} ", text, width),
            Some(formatting) => {
                if let Some(alignment_options) = &formatting.alignment {
                    if let Some(alignment) = alignment_options.get(column_index) {
                        match alignment {
                            ColumnAlignment::Left => format!("| {:<1$} ", text, width),
                            ColumnAlignment::Centre => format!("| {:^1$} ", text, width),
                            ColumnAlignment::Right => format!("| {:>1$} ", text, width),
                        }
                    } else {
                        format!("| {:1$} ", text, width)
                    }
                } else {
                    format!("| {:1$} ", text, width)
                }
            }
        }
    }
}

pub struct TableFormatting {
    pub alignment: Option<Vec<ColumnAlignment>>,
}

impl TableFormatting {
    pub fn new() -> Self {
        Self { alignment: None }
    }
}

pub enum ColumnAlignment {
    Left,
    Centre,
    Right,
}

pub struct UnicornString;

impl UnicornString {
    pub fn connector(text: &str) -> String {
        let mut connector = "a";
        for ch in text.chars() {
            if vec!['a', 'e', 'i', 'o', 'u'].contains(&ch) {
                connector = "an";
            }
            break;
        }
        connector.to_owned()
    }
}

use std::collections::HashMap;
use std::env;
use std::path::{Path, PathBuf};

use lib::{self, Library, Symbol};
use log::{debug, error, info};

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::module::{CreateFn, UnicornModule};
use unicorn_callable::reaction::UnicornReaction;
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;
use unicorn_information::ModuleInformation;

#[derive(Default)]
pub struct ModuleManager {
    libs: Vec<Library>,
    aliases: HashMap<String, String>,
    commands: HashMap<String, Box<dyn UnicornCommand>>,
    reaction_map: HashMap<String, Vec<usize>>,
    reactions: HashMap<usize, Box<dyn UnicornReaction>>,
    pub information: HashMap<String, ModuleInformation>,
}

impl ModuleManager {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_command_libs() -> Vec<PathBuf> {
        env::current_dir()
            .unwrap()
            .join("modules")
            .read_dir()
            .expect("Broken dir.")
            .map(|lib| lib.expect("Broken lib.").path())
            .collect::<Vec<_>>()
    }

    pub fn load_commands(&mut self, db: &DatabaseHandler, cfg: &mut ConfigurationBuilder) {
        for path in Self::get_command_libs() {
            if let Err(e) = self.load_command(&path, db, cfg) {
                error!("{}", e)
            }
        }
    }

    pub fn load_command<P>(&mut self, path: P, db: &DatabaseHandler, cfg: &mut ConfigurationBuilder) -> Result<(), Box<dyn std::error::Error>>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();

        debug!("Loading module: {:?}", path);

        let lib = Library::new(&path).map_err(|e| format!("Failed loading command lib for {}: {}", path.display(), e))?;

        let mdl: Box<dyn UnicornModule> = unsafe {
            let ctor = lib
                .get::<Symbol<'_, CreateFn>>(b"_command_create")
                .map_err(|e| format!("Failed loading command constructor for {}: {}", path.display(), e))?;

            Box::from_raw(ctor())
        };
        self.libs.push(lib);
        mdl.init_logger(if cfg.cfg.preferences.debug {
            log::LevelFilter::Debug
        } else {
            log::LevelFilter::Info
        });
        let module_name = mdl.name();
        mdl.on_load(&db, cfg);
        let mut meta = ModuleInformation {
            name: module_name.to_owned(),
            commands: Vec::new(),
        };

        for cmd in mdl.commands() {
            let cmd_name = cmd.command_name().to_lowercase();
            debug!("Adding {} command to the manager.", cmd_name);
            for alias in cmd.aliases().clone() {
                self.aliases.insert(alias.to_owned(), cmd_name.clone());
            }
            cmd.on_load(&db, cfg);
            let command_info = cmd.information();
            self.commands.insert(cmd_name, cmd);
            meta.commands.push(command_info);
        }
        for (index, react) in mdl.reactions().into_iter().enumerate() {
            react.on_load(&db, cfg);
            let emojis = react.emojis();
            match emojis {
                None => {
                    debug!("Adding unfiltered reaction handler to the manager.");
                    self.add_reaction_mapping("None", index);
                }
                Some(icons) => {
                    debug!("Adding reactions ({}) to the manager.", icons.join(", "));
                    for icon in icons {
                        self.add_reaction_mapping(icon, index);
                    }
                }
            }
            self.reactions.insert(index, react);
        }
        self.information.insert(module_name.to_lowercase().to_owned(), meta);
        info!("Loaded the {} module.", module_name);
        Ok(())
    }

    pub fn find_command<S>(&self, lookup: S) -> Option<&Box<dyn UnicornCommand>>
    where
        S: AsRef<str>,
    {
        debug!("Looking up command: {}...", lookup.as_ref());

        let cmd_name = match self.aliases.get(&lookup.as_ref().to_lowercase()) {
            Some(val) => val.to_owned(),
            None => lookup.as_ref().to_lowercase(),
        };

        self.commands.get(&cmd_name).map(|cmd| {
            debug!("Found command {}.", cmd_name);
            cmd
        })
    }

    fn add_reaction_mapping(&mut self, key: &str, target: usize) {
        if !self.reaction_map.contains_key(&key.to_owned()) {
            self.reaction_map.insert(key.to_owned(), Vec::new());
        }
        self.reaction_map.get_mut(&key.to_owned()).unwrap().push(target);
    }

    pub fn find_reaction<S>(&self, lookup: S) -> Option<Vec<&Box<dyn UnicornReaction>>>
    where
        S: AsRef<str>,
    {
        debug!("Looking up reaction: {}...", lookup.as_ref());
        let mut targets = Vec::new();
        if let Some(filtered) = self.reaction_map.get(&lookup.as_ref().to_owned()) {
            for react in filtered {
                targets.push(react.clone());
            }
        }
        if let Some(unfiltered) = self.reaction_map.get(&"None".to_owned()) {
            for react in unfiltered {
                targets.push(react.clone());
            }
        }
        let mut result = Vec::new();
        for i in targets {
            if let Some(target) = self.reactions.get(&i) {
                result.push(target);
            }
        }
        if result.len() > 0 {
            Some(result)
        } else {
            None
        }
    }
}

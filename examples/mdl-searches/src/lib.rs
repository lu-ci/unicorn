use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};
use unicorn_callable::module::UnicornModule;

use crate::crates::CratesCommand;
use crate::deezer::DeezerCommand;
use crate::imdb::IMDBCommand;
use crate::kitsu::anime::AnimeCommand;
use crate::kitsu::manga::MangaCommand;

mod crates;
mod deezer;
mod imdb;
mod kitsu;

#[derive(Default)]
pub struct SearchModule;

impl UnicornModule for SearchModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            DeezerCommand::boxed(),
            IMDBCommand::boxed(),
            AnimeCommand::boxed(),
            MangaCommand::boxed(),
            CratesCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Searches"
    }
}

define_module!(SearchModule, Default::default);

use serde::Deserialize;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

const IMDB_ICON: &str = "https://ia.media-imdb.com/images/G/01/imdb/images/mobile/\
                         apple-touch-icon-web-152x152-1475823641._CB522736557_.png";
const IMDB_COLOR: u64 = 0xeb_c1_2d;
const IMDB_API_BASE: &str = "https://sg.media-imdb.com/suggests/";

#[derive(Deserialize, Clone, Debug)]
pub struct ImageThing(String, u64, u64);

impl ImageThing {
    pub fn get_image(&self) -> String {
        self.0.clone()
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct IMDBMovie {
    l: String,
    pub id: String,
    s: Option<String>,
    y: Option<u16>,
    i: Option<ImageThing>,
}

impl IMDBMovie {
    pub fn title(&self) -> String {
        self.l.clone()
    }

    pub fn staring(&self) -> String {
        match self.s.clone() {
            Some(s) => s,
            None => "<No Data>".to_owned(),
        }
    }

    pub fn year(&self) -> Option<u16> {
        self.y
    }

    pub fn image(&self) -> Option<String> {
        match self.i.clone() {
            Some(ic) => Some(ic.get_image()),
            None => None,
        }
    }

    pub fn url(&self) -> String {
        format!("http://www.imdb.com/title/{}/", self.id)
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct IMDBResponse {
    pub d: Vec<IMDBMovie>,
}

impl IMDBResponse {
    pub fn is_empty(&self) -> bool {
        self.d.is_empty()
    }

    pub fn best_match(&self, lookup: impl ToString) -> IMDBMovie {
        let mut exact = false;
        let mut best = self.d[0].clone();
        let low_lookup = lookup.to_string().to_lowercase();
        for movie in self.d.clone() {
            if movie.title().to_lowercase() == low_lookup {
                exact = true;
                best = movie;
                break;
            }
        }
        if !exact {
            for movie in self.d.clone() {
                if movie.title().to_lowercase().contains(&low_lookup) {
                    best = movie;
                    break;
                }
            }
        }
        best
    }
}

#[derive(Default)]
pub struct IMDBCommand;

impl IMDBCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn get_api(lookup: impl ToString) -> String {
        let look_str = lookup.to_string();
        let look_chars = look_str.chars().collect::<Vec<char>>();
        let look_char = look_chars[0].to_string().to_lowercase();
        format!("{}/{}/{}.json", IMDB_API_BASE, look_char, look_str)
    }

    fn trim_body(body: String) -> String {
        let mut start_pieces: Vec<&str> = body.split('(').collect();
        start_pieces.remove(0);
        let after = start_pieces.join("(");
        let mut after_pieces: Vec<&str> = after.split(')').collect();
        after_pieces.remove(after_pieces.len() - 1);
        after_pieces.join(")")
    }
}

#[serenity::async_trait]
impl UnicornCommand for IMDBCommand {
    fn command_name(&self) -> &str {
        "imdb"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["movie"]
    }

    fn example(&self) -> &str {
        "blade runner"
    }

    fn description(&self) -> &str {
        "Searches the Internet Movie Database for your input. \
         Gives you the poster, release year, \
         and who stars in the movie, \
         as well as a link to the page of the movie."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            let api_url = Self::get_api(&lookup);
            match reqwest::blocking::get(&api_url) {
                Ok(reqres) => match reqres.text() {
                    Ok(body) => {
                        let trimmed = Self::trim_body(body);
                        match serde_json::from_str::<IMDBResponse>(&trimmed) {
                            Ok(imdb_resp) => {
                                if !imdb_resp.is_empty() {
                                    let movie = imdb_resp.best_match(lookup);
                                    let mut msg = CreateMessage::default();
                                    msg.embed(|e| {
                                        e.color(IMDB_COLOR);
                                        e.author(|a| {
                                            a.url(movie.url());
                                            a.icon_url(IMDB_ICON);
                                            a.name(&movie.title());
                                            a
                                        });
                                        e.description(format!(
                                            "Released: {}\nStaring: {}",
                                            match movie.year() {
                                                Some(y) => y.to_string(),
                                                None => "<No Data>".to_owned(),
                                            },
                                            movie.staring()
                                        ));
                                        if let Some(img) = movie.image() {
                                            e.thumbnail(img);
                                        }
                                        e
                                    });
                                    msg
                                } else {
                                    UnicornEmbed::not_found("Nothing found for that lookup.")
                                }
                            }
                            Err(_why) => UnicornEmbed::error("Failed to parse the response."),
                        }
                    }
                    Err(_why) => UnicornEmbed::error("Failed to read the response."),
                },
                Err(_why) => UnicornEmbed::error("Failed to communicate with IMDB."),
            }
        } else {
            UnicornEmbed::error("No movie name given to look up.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

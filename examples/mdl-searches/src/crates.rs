use std::collections::HashMap;

use chrono::FixedOffset;
use log::debug;
use serde::Deserialize;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

const CRATES_API: &str = "https://crates.io/api/v1/crates";
const CRATES_ICON: &str = "https://i.imgur.com/Nyw7kSc.png";
const CRATES_COLOR: u64 = 0x3a_64_36;
const CRATES_DATE_FMT: &str = "%+";

#[derive(Debug)]
pub enum ResponseError {
    NotFound,
    ReqwestError(reqwest::Error),
    SerdeJSONError(serde_json::Error),
}

impl ResponseError {
    pub fn not_found(&self) -> bool {
        match self {
            ResponseError::NotFound => true,
            _ => false,
        }
    }

    pub fn describe(&self) -> &str {
        match self {
            ResponseError::NotFound => "Crate not found.",
            ResponseError::ReqwestError(_) => "Failed to communicated with the API.",
            ResponseError::SerdeJSONError(_) => "Failed to parse the API response.",
        }
    }
}

impl From<reqwest::Error> for ResponseError {
    fn from(err: reqwest::Error) -> Self {
        ResponseError::ReqwestError(err)
    }
}

impl From<serde_json::Error> for ResponseError {
    fn from(err: serde_json::Error) -> Self {
        ResponseError::SerdeJSONError(err)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateLinks {
    pub version_downloads: String,
    pub versions: Option<String>,
    pub owners: String,
    pub owner_team: String,
    pub owner_user: String,
    pub reverse_dependencies: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct OwnersResponse {
    pub users: Vec<CrateUser>,
}

impl CrateLinks {
    pub fn get_owners(&self) -> Result<OwnersResponse, ResponseError> {
        let url = format!("https://crates.io{}", &self.owners);
        let resp = reqwest::blocking::get(&url)?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<OwnersResponse>(&body)?)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Crate {
    pub id: String,
    pub name: String,
    updated_at: String,
    pub versions: Vec<u64>,
    pub keywords: Vec<String>,
    pub categories: Vec<String>,
    created_at: String,
    pub downloads: u64,
    pub recent_downloads: u64,
    pub max_version: String,
    pub newest_version: String,
    pub description: String,
    pub homepage: Option<String>,
    pub documentation: Option<String>,
    pub repository: Option<String>,
    pub links: CrateLinks,
    pub exact_match: bool,
}

impl Crate {
    pub fn _updated_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.updated_at)
    }

    pub fn created_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.created_at)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateVersionLinks {
    pub dependencies: String,
    pub version_downloads: String,
    pub authors: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateUser {
    pub id: u64,
    pub login: String,
    pub name: String,
    pub avatar: String,
    pub url: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct AuditAction {
    pub action: String,
    pub user: CrateUser,
    time: String,
}

impl AuditAction {
    pub fn _time(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.time)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateVersion {
    pub id: u64,
    #[serde(rename = "crate")]
    pub pack: String,
    pub num: String,
    pub dl_path: String,
    pub readme_path: String,
    updated_at: String,
    created_at: String,
    pub downloads: u64,
    pub features: HashMap<String, Vec<String>>,
    pub yanked: bool,
    pub license: Option<String>,
    pub crate_size: Option<u64>,
    pub published_by: Option<CrateUser>,
    pub audit_actions: Vec<AuditAction>,
}

impl CrateVersion {
    pub fn updated_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.updated_at)
    }

    pub fn _created_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.created_at)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateKeyword {
    pub id: String,
    pub keyword: String,
    created_at: String,
    pub crates_cnt: u64,
}

impl CrateKeyword {
    pub fn _created_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.created_at)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateCategory {
    pub id: String,
    pub category: String,
    pub slug: String,
    pub description: String,
    created_at: String,
    pub crates_cnt: u64,
}

impl CrateCategory {
    pub fn _created_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.created_at)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CratesResponse {
    #[serde(rename = "crate")]
    pub pack: Crate,
    pub versions: Vec<CrateVersion>,
    pub keywords: Vec<CrateKeyword>,
    pub categories: Vec<CrateCategory>,
}

impl CratesResponse {
    pub fn new(lookup: impl ToString) -> Result<CratesResponse, ResponseError> {
        let url = format!("{}/{}", CRATES_API, lookup.to_string());
        let resp = reqwest::blocking::get(&url)?;
        let body = resp.text()?;
        match serde_json::from_str::<Self>(&body) {
            Ok(cr) => Ok(cr),
            Err(why) => {
                if body.to_lowercase().contains("not found") {
                    Err(ResponseError::NotFound)
                } else {
                    Err(ResponseError::SerdeJSONError(why))
                }
            }
        }
    }

    pub fn url(&self) -> String {
        format!("https://crates.io/crates/{}", &self.pack.id)
    }

    pub fn latest(&self) -> CrateVersion {
        let mut best = self.versions[0].clone();
        for ver in &self.versions {
            if ver.num == self.pack.newest_version {
                best = ver.clone();
                break;
            } else if ver.id > best.id {
                best = ver.clone();
            }
        }
        best
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CratesError {
    pub detail: String,
}

#[derive(Default)]
pub struct CratesCommand;

impl CratesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    pub fn datetime(text: impl ToString) -> Option<chrono::DateTime<FixedOffset>> {
        if let Ok(dt) = chrono::DateTime::parse_from_str(&text.to_string(), CRATES_DATE_FMT) {
            Some(dt)
        } else {
            None
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for CratesCommand {
    fn command_name(&self) -> &str {
        "crates"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["crate", "cargo"]
    }

    fn example(&self) -> &str {
        "javelin"
    }

    fn description(&self) -> &str {
        "Search Rust's/Cargo's package repository on crates.io \
         for the specified package and displays its details."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("crate", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("crate") {
            let lookup = pld.args.get("crate");
            match CratesResponse::new(lookup) {
                Ok(pack) => {
                    let owner_result = pack.pack.links.get_owners();
                    let (owners, owner_ender) = match &owner_result {
                        Ok(ors) => {
                            let mut stack = Vec::<String>::new();
                            for usr in &ors.users {
                                stack.push(format!("[{}]({})", &usr.login, &usr.url));
                            }
                            (stack.join(", "), if stack.len() == 1 { "" } else { "s" })
                        }
                        Err(_) => ("Couldn\'t retrieve owners.".to_owned(), ""),
                    };
                    let latest = pack.latest();
                    let mut msg = CreateMessage::default();
                    msg.embed(|e| {
                        let info = format!(
                            "Owner{}: {}\nCreated: {}\nDownloads: {}\n**{}**",
                            owner_ender,
                            owners,
                            if let Some(ts) = pack.pack.created_at() {
                                ts.format("%D. %m. %Y").to_string()
                            } else {
                                "Unknown".to_owned()
                            },
                            pack.pack.downloads,
                            pack.pack.description
                        );
                        e.color(CRATES_COLOR);
                        if let Some(uat) = latest.updated_at() {
                            e.timestamp(uat.to_rfc3339());
                        }
                        e.description(info);
                        e.author(|a| {
                            a.url(pack.url());
                            a.icon_url(CRATES_ICON);
                            a.name(format!("{} {}", &pack.pack.name, &latest.num));
                            a
                        });
                        if let Ok(ow) = owner_result {
                            if let Some(usr) = ow.users.first() {
                                e.thumbnail(&usr.avatar);
                            }
                        }
                        e.footer(|f| {
                            f.text(if !pack.keywords.is_empty() {
                                format!("Keywords: {}", pack.pack.keywords.join(", "))
                            } else {
                                "Last updated".to_owned()
                            });
                            f
                        });
                        e
                    });
                    msg
                }
                Err(why) => {
                    if why.not_found() {
                        UnicornEmbed::not_found(why.describe())
                    } else {
                        debug!("Crate lookup failed: {:?}", &why);
                        UnicornEmbed::error(why.describe())
                    }
                }
            }
        } else {
            UnicornEmbed::error("No crate name given.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use serde::Deserialize;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;

const DEEZER_API: &str = "https://api.deezer.com/search/track";
const DEEZER_ICON: &str = "https://e-cdns-files.dzcdn.net/cache/images/common/favicon/favicon-32x32.42917d14a8cb0cc4045c827251b57c0e.png";

#[derive(Deserialize, Debug, Clone)]
pub struct DeezerResponse {
    pub data: Vec<DeezerTrack>,
}

impl DeezerResponse {
    pub fn best_match(&self, lookup: impl ToString) -> DeezerTrack {
        let mut exact = false;
        let mut best = self.data[0].clone();
        let low_lookup = lookup.to_string().to_lowercase();
        for track in self.data.clone() {
            let full_match = track.title.to_lowercase() == low_lookup;
            let short_match = track.title_short.to_lowercase() == low_lookup;
            if full_match || short_match {
                exact = true;
                best = track;
                break;
            }
        }
        if !exact {
            for track in self.data.clone() {
                let full_cont = track.title.to_lowercase().contains(&low_lookup);
                let short_cont = track.title_short.to_lowercase().contains(&low_lookup);
                if full_cont || short_cont {
                    best = track;
                    break;
                }
            }
        }
        best
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct DeezerArtist {
    pub id: u64,
    pub name: String,
    pub link: String,
    pub picture: String,
    pub picture_small: String,
    pub picture_medium: String,
    pub picture_big: String,
    pub picture_xl: String,
    pub tracklist: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct DeezerAlbum {
    pub id: u64,
    pub title: String,
    pub cover: String,
    pub cover_small: String,
    pub cover_medium: String,
    pub cover_big: String,
    pub cover_xl: String,
    pub tracklist: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct DeezerTrack {
    pub id: u64,
    pub readable: bool,
    pub title: String,
    pub title_short: String,
    pub link: String,
    pub duration: u64,
    pub rank: u64,
    pub explicit_lyrics: bool,
    pub explicit_content_lyrics: u8,
    pub explicit_content_cover: u8,
    pub preview: String,
    pub artist: DeezerArtist,
    pub album: DeezerAlbum,
}

#[derive(Default)]
pub struct DeezerCommand;

impl DeezerCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn duration(secs: u64) -> String {
        let mins = secs / 60;
        let leftover = secs - (60 * mins);
        format!("{}:{}", mins, leftover)
    }
}

#[serenity::async_trait]
impl UnicornCommand for DeezerCommand {
    fn command_name(&self) -> &str {
        "deezer"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["findsong"]
    }

    fn example(&self) -> &str {
        "Highway to Hell"
    }

    fn description(&self) -> &str {
        "Searches Deezer for infomation on the specified song. \
         The output will include a song preview link."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            let api_url = format!("{}?q={}", DEEZER_API, &lookup);
            if let Ok(resp) = reqwest::blocking::get(&api_url) {
                if let Ok(body) = resp.text() {
                    match serde_json::from_str::<DeezerResponse>(&body) {
                        Ok(qres) => {
                            if !qres.data.is_empty() {
                                let track = qres.best_match(lookup);
                                let mut msg = CreateMessage::default();
                                let duration = Self::duration(track.duration);
                                msg.embed(|e| {
                                    e.color(ImageProcessing::from_url(track.album.cover_medium.clone()));
                                    e.author(|a| {
                                        a.name(&track.artist.name);
                                        a.icon_url(&track.artist.picture_medium);
                                        a.url(&track.link);
                                        a
                                    });
                                    e.field(
                                        &track.title,
                                        format!("Preview: [Here]({})\nDuration: {}", &track.preview, duration),
                                        false,
                                    );
                                    e.thumbnail(&track.album.cover_medium);
                                    e.footer(|f| {
                                        f.icon_url(DEEZER_ICON);
                                        f.text(format!("Album: {}", &track.album.title));
                                        f
                                    });
                                    e
                                });
                                msg
                            } else {
                                UnicornEmbed::not_found("Nothing found for that lookup.")
                            }
                        }
                        Err(_why) => UnicornEmbed::error("Failed to parse the response."),
                    }
                } else {
                    UnicornEmbed::error("Failed to read the response body.")
                }
            } else {
                UnicornEmbed::error("Failed to communicate with the Deezer API.")
            }
        } else {
            UnicornEmbed::error("No lookup given.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use log::debug;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::kitsu::common::{KitsuMode, KitsuResponse, KITSU_COLOR, KITSU_ICON};

#[derive(Default)]
pub struct AnimeCommand;

impl AnimeCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for AnimeCommand {
    fn command_name(&self) -> &str {
        "anime"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["animu", "kitsuanime"]
    }

    fn example(&self) -> &str {
        "Plastic Memories"
    }

    fn description(&self) -> &str {
        "Searches Kitsu.io for the specified anime. \
         The outputed results will be information like the number of episodes, \
         user rating, air time, plot summary, and poster image."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            match KitsuResponse::new(lookup, KitsuMode::Anime) {
                Ok(qry) => {
                    if let Some(anime) = qry.best(lookup) {
                        let mut msg = CreateMessage::default();
                        msg.embed(|e| {
                            e.color(KITSU_COLOR);
                            e.author(|a| {
                                a.icon_url(KITSU_ICON);
                                a.name(anime.en_title().unwrap_or(anime.ja_title().unwrap_or("<Unprocessable Name>".to_owned())));
                                a.url(anime.url());
                                a
                            });
                            let info = format!(
                                "Title: {}\nRating: {}%\nAir Time: {} - {}\nEpisodes: {}\nDuration: {}",
                                anime.ja_title().unwrap_or("<Unprocessable Name>".to_owned()),
                                anime.attrs.rating(),
                                anime.attrs.start_date.unwrap_or("???".to_owned()),
                                anime.attrs.end_date.unwrap_or("???".to_owned()),
                                match anime.attrs.episode_count {
                                    Some(cnt) => cnt.to_string(),
                                    None => "Unknown".to_owned(),
                                },
                                match anime.attrs.episode_length {
                                    Some(len) => format!("{} Minutes", len),
                                    None => "Unknown".to_owned(),
                                }
                            );
                            if let Some(imgs) = anime.attrs.poster_image {
                                e.thumbnail(imgs.original);
                            }
                            e.field("Information", info, false);
                            e.field("Synopsis", anime.attrs.synopsis.split_at(384).0, false);
                            e.footer(|f| {
                                f.text("Click the title at the top to see the page of the anime.");
                                f
                            });
                            e
                        });
                        msg
                    } else {
                        UnicornEmbed::not_found("Nothing found for that lookup.")
                    }
                }
                Err(why) => {
                    debug!("Kitsu anime lookup failed: {:?}", why);
                    UnicornEmbed::error(why.describe())
                }
            }
        } else {
            UnicornEmbed::error("No lookup given.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

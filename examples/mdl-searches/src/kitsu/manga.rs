use log::debug;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::kitsu::common::{KitsuMode, KitsuResponse, KITSU_COLOR, KITSU_ICON};

#[derive(Default)]
pub struct MangaCommand;

impl MangaCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for MangaCommand {
    fn command_name(&self) -> &str {
        "manga"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["mango", "kitsumanga"]
    }

    fn example(&self) -> &str {
        "A Silent Voice"
    }

    fn description(&self) -> &str {
        "Searches Kitsu.io for the specified manga. \
         The outputed results will be information like the number of chapters, \
         user rating, plot summary, and poster image."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            match KitsuResponse::new(lookup, KitsuMode::Manga) {
                Ok(qry) => {
                    if let Some(manga) = qry.best(lookup) {
                        let mut msg = CreateMessage::default();
                        msg.embed(|e| {
                            e.color(KITSU_COLOR);
                            e.author(|a| {
                                a.icon_url(KITSU_ICON);
                                a.name(manga.en_title().unwrap_or(manga.ja_title().unwrap_or("<Unprocessable Name>".to_owned())));
                                a.url(manga.url());
                                a
                            });
                            let info = format!(
                                "Title: {}\nRating: {}%\nAir Time: {} - {}\nVolumes: {}\nChapters: {}",
                                manga.ja_title().unwrap_or("<Unprocessable Name>".to_owned()),
                                manga.attrs.rating(),
                                manga.attrs.start_date.unwrap_or("???".to_owned()),
                                manga.attrs.end_date.unwrap_or("???".to_owned()),
                                match manga.attrs.volume_count {
                                    Some(cnt) => cnt.to_string(),
                                    None => "Unknown".to_owned(),
                                },
                                match manga.attrs.chapter_count {
                                    Some(len) => format!("{} Minutes", len),
                                    None => "Unknown".to_owned(),
                                }
                            );
                            if let Some(imgs) = manga.attrs.poster_image {
                                e.thumbnail(imgs.original);
                            }
                            e.field("Information", info, false);
                            e.field("Synopsis", manga.attrs.synopsis.split_at(384).0, false);
                            e.footer(|f| {
                                f.text("Click the title at the top to see the page of the manga.");
                                f
                            });
                            e
                        });
                        msg
                    } else {
                        UnicornEmbed::not_found("Nothing found for that lookup.")
                    }
                }
                Err(why) => {
                    debug!("Kitsu manga lookup failed: {:?}", why);
                    UnicornEmbed::error(why.describe())
                }
            }
        } else {
            UnicornEmbed::error("No lookup given.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use std::collections::HashMap;

use serde::Deserialize;

pub const KITSU_ICON: &str = "https://avatars3.githubusercontent.com/u/7648832?v=3&s=200";
pub const KITSU_COLOR: u64 = 0xff_33_00;

#[derive(Debug)]
pub enum KitsuError {
    ReqwestError(reqwest::Error),
    SerdeJSONError(serde_json::Error),
}

impl KitsuError {
    pub fn describe(&self) -> &str {
        match self {
            KitsuError::ReqwestError(_) => "Failed to communicated with the API.",
            KitsuError::SerdeJSONError(_) => "Failed to parse the API response.",
        }
    }
}

pub enum KitsuMode {
    Anime,
    Manga,
}

impl KitsuMode {
    pub fn api_end(&self) -> &str {
        match self {
            KitsuMode::Anime => "anime",
            KitsuMode::Manga => "manga",
        }
    }
}

impl From<reqwest::Error> for KitsuError {
    fn from(err: reqwest::Error) -> Self {
        KitsuError::ReqwestError(err)
    }
}

impl From<serde_json::Error> for KitsuError {
    fn from(err: serde_json::Error) -> Self {
        KitsuError::SerdeJSONError(err)
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct KitsuMeta {
    pub count: u64,
}

#[derive(Deserialize, Clone, Debug)]
pub struct KitsuLinks {
    pub first: String,
    #[serde(default = "KitsuLinks::default_none")]
    pub prev: Option<String>,
    #[serde(default = "KitsuLinks::default_none")]
    pub next: Option<String>,
    pub last: String,
}

impl KitsuLinks {
    fn default_none() -> Option<String> {
        None
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct KitsuImages {
    pub original: String,
}

#[derive(Deserialize, Clone, Debug)]
pub struct KitsuAttributes {
    pub slug: String,
    pub synopsis: String,
    pub titles: HashMap<String, Option<String>>,
    #[serde(rename = "averageRating")]
    rating: Option<String>,
    #[serde(rename = "startDate")]
    pub start_date: Option<String>,
    #[serde(rename = "endDate")]
    pub end_date: Option<String>,
    #[serde(rename = "episodeCount", default = "KitsuAttributes::default_count")]
    pub episode_count: Option<u64>,
    #[serde(rename = "episodeLength", default = "KitsuAttributes::default_count")]
    pub episode_length: Option<u64>,
    #[serde(rename = "chapterCount", default = "KitsuAttributes::default_count")]
    pub chapter_count: Option<u64>,
    #[serde(rename = "volumeCount", default = "KitsuAttributes::default_count")]
    pub volume_count: Option<u64>,
    #[serde(rename = "posterImage")]
    pub poster_image: Option<KitsuImages>,
}

impl KitsuAttributes {
    fn default_count() -> Option<u64> {
        None
    }

    pub fn rating(&self) -> f64 {
        self.rating.clone().unwrap_or("0.0".to_owned()).parse::<f64>().unwrap_or(0f64)
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct KitsuEntry {
    #[serde(rename = "type")]
    pub kind: String,
    #[serde(rename = "attributes")]
    pub attrs: KitsuAttributes,
}

impl KitsuEntry {
    pub fn url(&self) -> String {
        format!("https://kitsu.io/{}/{}", self.kind, self.attrs.slug)
    }

    fn title_contains(&self, lookup: &str) -> Option<String> {
        let mut best = None;
        for (key, title) in self.attrs.titles.clone() {
            if key.to_lowercase().contains(lookup) {
                if let Some(t) = title {
                    best = Some(t);
                    break;
                }
            }
        }
        best
    }

    pub fn en_title(&self) -> Option<String> {
        self.title_contains("en")
    }

    pub fn ja_title(&self) -> Option<String> {
        if let Some(res) = self.title_contains("ja") {
            Some(res)
        } else {
            self.title_contains("jp")
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct KitsuResponse {
    pub data: Vec<KitsuEntry>,
    pub meta: KitsuMeta,
    pub links: KitsuLinks,
}

impl KitsuResponse {
    pub fn new(lookup: impl ToString, mode: KitsuMode) -> Result<Self, KitsuError> {
        let url = format!("https://kitsu.io/api/edge/{}?filter[text]={}", mode.api_end(), lookup.to_string());
        let resp = reqwest::blocking::get(&url)?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<KitsuResponse>(&body)?)
    }

    pub fn best(&self, lookup: impl ToString) -> Option<KitsuEntry> {
        let mut best = None;
        for entry in self.data.clone() {
            for (_, title) in entry.attrs.titles.clone() {
                if let Some(t) = title {
                    if t.to_lowercase() == lookup.to_string().to_lowercase() {
                        best = Some(entry);
                        break;
                    }
                }
            }
            if best.is_some() {
                break;
            }
        }
        if best.is_none() {
            for entry in self.data.clone() {
                for (_, title) in entry.attrs.titles.clone() {
                    if let Some(t) = title {
                        if t.to_lowercase().contains(&lookup.to_string().to_lowercase()) {
                            best = Some(entry);
                            break;
                        }
                    }
                }
                if best.is_some() {
                    break;
                }
            }
        }
        if best.is_none() && !self.data.is_empty() {
            best = Some(self.data[0].clone());
        }
        best
    }
}

#[derive(Debug)]
pub enum MightyPartyError {
    ReqwestError(reqwest::Error),
    SerdeJSONError(serde_json::Error),
    IoError(std::io::Error),
    DbError(mongodb::error::Error),
}

impl From<mongodb::error::Error> for MightyPartyError {
    fn from(err: mongodb::error::Error) -> Self {
        MightyPartyError::DbError(err)
    }
}

impl From<reqwest::Error> for MightyPartyError {
    fn from(err: reqwest::Error) -> Self {
        MightyPartyError::ReqwestError(err)
    }
}

impl From<serde_json::Error> for MightyPartyError {
    fn from(err: serde_json::Error) -> Self {
        MightyPartyError::SerdeJSONError(err)
    }
}

impl From<std::io::Error> for MightyPartyError {
    fn from(err: std::io::Error) -> Self {
        MightyPartyError::IoError(err)
    }
}
use super::error::MightyPartyError;
use super::model::*;
use bson::*;
use flate2::read::GzDecoder;
use log::{error, info};
use reqwest::blocking::Client;
use reqwest::header::{HeaderMap, HeaderValue};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::{collections::HashMap, io::Read};
use unicorn_database::DatabaseHandler;
use uuid::Uuid;

const URL_BASE: &str = "https://blitzmightyparty.ru/gs_api/";
const VERSION: &str = "0.67";

#[derive(Deserialize)]
struct HashPair {
    pub name: String,
    pub hash: String,
}

pub struct MightyPartyClient {
    pub device_id: String,
}

impl MightyPartyClient {
    pub fn new() -> Self {
        Self {
            device_id: "Bot2b8498230c2d4ede858725832395c2dd".to_owned(),
        }
    }

    // pub fn impersonate(player_id: &str) -> Self {
    //     Self {
    //         device_id: player_id.to_owned(),
    //     }
    // }

    fn add_header(&self, map: &mut HeaderMap, key: &'static str, value: &str) {
        if let Ok(val) = HeaderValue::from_str(value) {
            map.append(key, val);
        }
    }

    fn obscure_number(number: i32) -> String {
        (number ^ i32::max_value()).to_string().chars().rev().collect()
    }

    fn secure_string(&self, data: &str, packet_number: i32) -> String {
        format!(
            "{:x}",
            md5::compute(format!("{:x}", md5::compute(data)) + &self.device_id + VERSION + &packet_number.to_string())
        )
    }

    fn send_message(&self, path: &str, data: &str, compressed: bool) -> Result<String, MightyPartyError> {
        let client = Client::new();
        let request = client.post(&(URL_BASE.to_owned() + path));
        let mut header_map = HeaderMap::new();

        let now = chrono::Local::now().to_rfc3339();
        let utc_now = chrono::Utc::now().to_rfc3339();

        let packet_number = 1;
        let obscure_ping = MightyPartyClient::obscure_number(packet_number);
        let secure_string = self.secure_string(&data, packet_number);

        self.add_header(&mut header_map, "PNK-Retry", "1");
        self.add_header(&mut header_map, "PNK-Device-ID", &self.device_id);
        self.add_header(&mut header_map, "PNK-Login-ID", &self.device_id);
        self.add_header(&mut header_map, "PNK-Player-ID", &self.device_id);
        self.add_header(&mut header_map, "PNK-Platform", "MTPSITE");
        self.add_header(&mut header_map, "PNK-Env", "WindowsPlayer");
        self.add_header(&mut header_map, "PNK-Version", VERSION);
        self.add_header(&mut header_map, "PNK-Secure-String", &secure_string);
        self.add_header(&mut header_map, "PNK-request-client-start-time", &now);
        self.add_header(&mut header_map, "PNK-request-client-system-time", &now);
        self.add_header(&mut header_map, "PNK-request-client-system-time-utc", &utc_now);
        self.add_header(&mut header_map, "PNK-number", &obscure_ping);
        self.add_header(
            &mut header_map,
            "PNK-Request-Id",
            Uuid::new_v4().to_hyphenated().encode_upper(&mut Uuid::encode_buffer()),
        );
        let response = request.headers(header_map).body(data.to_owned()).send()?;
        if compressed {
            let bytes = response.bytes()?;
            let mut gz = GzDecoder::new(bytes.as_ref());
            let mut s = String::new();
            gz.read_to_string(&mut s)?;
            //response.bytes().
            Ok(s)
        } else {
            Ok(response.text()?)
        }
    }

    fn get_hash_from_db(&self, db: &DatabaseHandler, config_name: &str) -> String {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"name": config_name};
            if let Ok(Some(doc)) = cli.database(&db.cfg.database.name).collection("mp-config-hashes").find_one(lookup, None) {
                if let Ok(hash) = bson::from_bson::<HashPair>(bson::Bson::Document(doc)) {
                    return hash.hash;
                }
            }
        }
        "0".to_string()
    }

    fn should_update(&self, db: &DatabaseHandler, config_name: &str, hash: &str, matches: Vec<&str>) -> bool {
        let mut result = false;
        for config in matches {
            if config_name.to_lowercase().contains(&config.to_lowercase()) || config_name.to_lowercase() == config.to_lowercase() {
                let db_hash = self.get_hash_from_db(db, config_name);
                result = hash != db_hash
            }
        }
        result
    }

    fn update_hash(&self, db: &DatabaseHandler, config_name: &str, hash: &str) -> Result<(), MightyPartyError> {
        if let Some(cli) = db.get_client() {
            let collection = cli.database(&db.cfg.database.name).collection("mp-config-hashes");
            let lookup = doc! {"name": config_name};
            let value = doc! {"name": config_name, "hash":hash};
            if let Ok(Some(_)) = collection.find_one(lookup.clone(), None) {
                collection.update_one(lookup, value, None)?;
            } else {
                collection.insert_one(value, None)?;
            }
        }
        Ok(())
    }

    fn update_config<T, F>(&self, db: &DatabaseHandler, collection_name: &str, config_name: &str, hash: &str, key: F) -> Result<(), MightyPartyError>
    where
        T: DeserializeOwned + Serialize,
        F: Fn(T) -> Document,
    {
        info!("Updating {}", config_name);
        if let Ok(config) = self.send_message(
            "configs_zip/get_data",
            &format!("{{\"config_names\":[\"{}\"], \"encode_base64\":true}}", config_name),
            true,
        ) {
            let collection = &format!("mp-config-{}", collection_name);
            if let Some(cli) = db.get_client() {
                let mongo_db = cli.database(&db.cfg.database.name);
                let collection = mongo_db.collection(collection);
                match serde_json::from_str::<Vec<T>>(&config) {
                    Ok(new_config) => {
                        for item in new_config {
                            if let Ok(entry) = bson::to_bson::<T>(&item) {
                                if let Some(doc) = entry.as_document() {
                                    let lookup = key(item);
                                    if let Ok(Some(_)) = collection.find_one(lookup.clone(), None) {
                                        if let Err(error) = collection.update_one(lookup, doc.to_owned(), None) {
                                            error!("{}", error);
                                        }
                                    } else {
                                        if let Err(error) = collection.insert_one(doc.to_owned(), None) {
                                            error!("{}", error);
                                        }
                                    }
                                }
                            }
                        }
                        self.update_hash(&db, config_name, hash)?;
                    }
                    Err(err) => error!("Error deserializing {}: {}", config_name, err),
                }
            }
        }
        info!("Updated {}", config_name);
        Ok(())
    }

    fn update_localisation(&self, db: &DatabaseHandler, config_name: &str, hash: &str) -> Result<(), MightyPartyError> {
        info!("Updating {}", config_name);
        if let Ok(config) = self.send_message(
            "configs_zip/get_data",
            &format!("{{\"config_names\":[\"{}\"], \"encode_base64\":true}}", config_name),
            true,
        ) {
            let lines: Vec<&str> = config.split("[NL]").collect();
            if let Some(cli) = db.get_client() {
                let collection = cli.database(&db.cfg.database.name).collection("mp-config-localisation-en");
                let total_lines = lines.len();
                let mut line_number = 0;
                for line in lines {
                    let kvp: Vec<&str> = line.splitn(2, ":").collect();
                    if kvp.len() == 2 {
                        if let Some(key) = kvp.get(0) {
                            if let Some(local_string) = kvp.get(1) {
                                let lookup = doc! {"key": key};
                                let value = doc! {"key": key, "value":local_string};
                                if let Ok(Some(_)) = collection.find_one(lookup.clone(), None) {
                                    collection.update_one(lookup, value, None)?;
                                } else {
                                    collection.insert_one(value, None)?;
                                }
                                if key.starts_with("#monster_name_") {
                                    if let Ok(monster_id) = key.chars().skip(14).collect::<String>().parse::<i32>() {
                                        let collection = cli.database(&db.cfg.database.name).collection("mp-monster-name");
                                        let lookup = doc! {"name": local_string, "id":monster_id};
                                        let value = doc! {"name": local_string, "id":monster_id};
                                        if let Ok(Some(_)) = collection.find_one(lookup.clone(), None) {
                                            collection.update_one(lookup, value, None)?;
                                        } else {
                                            collection.insert_one(value, None)?;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    line_number += 1;
                    if total_lines > 2000 && (line_number % (total_lines / 10) == 0) {
                        info!("updating... {}% comlete ", line_number * 100 / total_lines);
                    }
                }
            }
            self.update_hash(&db, config_name, hash)?;
        }
        info!("Updated {}", config_name);
        Ok(())
    }

    pub fn update_configs(&self, db: &DatabaseHandler) -> Result<(), MightyPartyError> {
        let hash_result = self.send_message("configs/get_config_hashes", "{\"config_names\":null}", false)?;
        let hashes = serde_json::from_str::<ConfigHashes>(&hash_result)?;

        for hash in hashes.result.hashes {
            if self.should_update(&db, &hash.0, &hash.1, vec!["Monsters_", "Custom_Monsters"]) {
                self.update_config(db, "monsters", &hash.0, &hash.1, |m: Monster| doc! { "id": m.id, "inner_id":m.inner_id})?;
            } else if self.should_update(&db, &hash.0, &hash.1, vec!["MonsterSetPatterns"]) {
                self.update_config(db, "soulbinds", &hash.0, &hash.1, |s: SetPattern| doc! { "id": s.id})?;
            } else if self.should_update(&db, &hash.0, &hash.1, vec!["MonsterPromotePatterns"]) {
                self.update_config(
                    db,
                    "promotes",
                    &hash.0,
                    &hash.1,
                    |p: PromotePattern| doc! { "id": p.id, "monster_inner_id":p.monster_inner_id},
                )?;
            } else if self.should_update(&db, &hash.0, &hash.1, vec!["MonsterResets", "Custom_MonsterResets"]) {
                self.update_config(
                    db,
                    "reborns",
                    &hash.0,
                    &hash.1,
                    |r: Reset| doc! { "monster_id": r.monster_id, "reset_num":r.reset_num},
                )?
            } else if self.should_update(
                &db,
                &hash.0,
                &hash.1,
                vec![
                    "Localization_en",
                    "Localization_Custom_en",
                    "Localization_Event_en",
                    "Localization_Allmighty_en",
                    "Localization_News_en",
                ],
            ) {
                self.update_localisation(db, &hash.0, &hash.1)?;
            }
        }
        Ok(())
    }

    pub fn world_chat(&self) -> Result<Vec<Message>, MightyPartyError> {
        let messages = self.send_message(
            "chat/get_messages",
            "{\"channel_id\":\"global_channel\",\"limit\":50,\"since_date_time\":null}",
            false,
        )?;
        let channel= serde_json::from_str::<ChatChannel>(&messages)?;
        Ok(channel.result.messages)
    }

    pub fn send_message_to_channel(&self, channel_id:&str, message: &str) -> Result<(), MightyPartyError> {
        self.send_message(    
            "chat/send_message",        
            &format!("{{\"channel_id\":\"{}\",\"text\":\"{}\"}}",channel_id, message),
            false
        )?;
        Ok(())
    }

    pub fn find_channel(&self, player_id:&str) -> Result<String, MightyPartyError> {
        let messages = self.send_message(    
            "chat/get_channel",        
            &format!("{{\"channel_key\":\"{}\"}}",player_id),
            false
        )?;
        let channel= serde_json::from_str::<GetChannelResponse>(&messages)?;
        Ok(channel.result.channel_id)
    }

    pub fn monster_from_name(&self, db: &DatabaseHandler, name: &str) -> Result<Vec<Monster>, MightyPartyError> {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"name": {"$regex": format!(".*{}.*",name)}};
            if let Ok(Some(doc)) = cli.database(&db.cfg.database.name).collection("mp-monster-name").find_one(lookup, None) {
                for stat in doc {
                    let (name, id) = stat;
                    if name == "id" {
                        if let Some(monster_id) = id.as_i32() {
                            return self.monster_from_id(&db, monster_id);
                        }
                    }
                }
            }
        }
        Ok(Vec::new())
    }

    pub fn monster_name(&self, db: &DatabaseHandler, id: i32) -> String {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"id": id};
            if let Ok(Some(doc)) = cli.database(&db.cfg.database.name).collection("mp-monster-name").find_one(lookup, None) {
                for stat in doc {
                    let (name, value) = stat;
                    if name == "name" {
                        if let Some(monster_name) = value.as_str() {
                            return monster_name.to_owned();
                        }
                    }
                }
            }
        }
        "unknown".to_string()
    }

    pub fn monster_from_id(&self, db: &DatabaseHandler, id: i32) -> Result<Vec<Monster>, MightyPartyError> {
        let mut monsters = Vec::new();
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"id":  id};
            if let Ok(cursor) = cli.database(&db.cfg.database.name).collection("mp-config-monsters").find(lookup, None) {
                for entry in cursor {
                    if let Ok(doc) = entry {
                        if let Ok(monster) = bson::from_bson::<Monster>(bson::Bson::Document(doc)) {
                            monsters.push(monster);
                        }
                    }
                }
            }
        }
        monsters.sort_by_key(|m| m.inner_id);
        Ok(monsters)
    }

    pub fn reborns_for_monster(&self, db: &DatabaseHandler, monster_id: i32) -> Result<Vec<Reset>, MightyPartyError> {
        let mut reborns = Vec::new();
        if let Some(cli) = db.get_client() {
            let lookup = doc! { "monster_id": monster_id };
            match cli.database(&db.cfg.database.name).collection("mp-config-reborns").find(lookup, None) {
                Ok(cursor) => {
                    for entry in cursor {
                        if let Ok(doc) = entry {
                            if let Ok(monster) = bson::from_bson::<Reset>(bson::Bson::Document(doc)) {
                                reborns.push(monster);
                            }
                        }
                    }
                }
                Err(err) => error!("{}", err),
            }
        }
        reborns.sort_by_key(|r| r.reset_num);
        Ok(reborns)
    }

    pub fn upgrade_skill(&self, skills: &mut HashMap<String, String>, skill: &str, value: &str) {
        if let Some(current_value) = &skills.get(skill) {
            if let Ok(current) = current_value.parse::<i32>() {
                if let Ok(new) = value.parse::<i32>() {
                    if let Some(updated_skill) = skills.get_mut(skill) {
                        *updated_skill = (current + new).to_string();
                    }
                } else {
                    error!("new value is not an integer");
                }
            } else {
                error!("current value is not an integer");
            }
        } else {
            error!("monster does not have skill {}", skill);
        }
    }

    pub fn localized_string(&self, db: &DatabaseHandler, prefix: &str, id: &str) -> String {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"key":  prefix.to_owned() + id};
            if let Ok(Some(doc)) = cli
                .database(&db.cfg.database.name)
                .collection("mp-config-localisation-en")
                .find_one(lookup, None)
            {
                for stat in doc {
                    let (name, value) = stat;
                    if name == "value" {
                        if let Some(localised_text) = value.as_str() {
                            return localised_text.to_owned();
                        }
                    }
                }
            }
        }
        id.to_owned()
    }

    fn process_token(&self, token: &str) -> String {
        if let Ok(_) = i32::from_str_radix(token, 16) {
            //colours are not supported
            String::new()
        } else {
            match token {
                "rage" => "⚡".to_string(),
                "runa_hp" => "❤️".to_string(),
                "swords" => "🗡️".to_string(),
                "hp" => "❤️".to_string(),
                "block_skill" => "🛡️".to_string(),
                "-" => {
                    //end colour tag
                    String::new()
                }
                _ => token.to_string(),
            }
        }
    }

    fn process_value_token(&self, token: &str, value: &str) -> String {
        match token {
            "val" => value.to_string(),
            "count" => {
                let v: Vec<&str> = value.split(|c| c == 'c' || c == 'v').collect();
                if let Some(&count) = v.get(1) {
                    count.to_string()
                } else {
                    String::new()
                }
            }
            "rVal" => {
                let v: Vec<&str> = value.split(|c| c == 'c' || c == 'v').collect();
                if let Some(&val) = v.get(2) {
                    val.to_string()
                } else {
                    String::new()
                }
            }
            _ => format!("{}:{}", token, value),
        }
    }

    fn process_or_token(&self, token: &str, count: i32) -> String {
        let tokens: Vec<&str> = token.split(',').collect();
        match count {
            0..=1 => {
                if let Some(&part) = tokens.get(0).to_owned() {
                    part.to_owned()
                } else {
                    String::new()
                }
            }
            _ => {
                if let Some(&part) = tokens.get(1).to_owned() {
                    part.to_owned()
                } else {
                    String::new()
                }
            }
        }
    }

    pub fn format_localised_string(&self, text: &str, value: &str) -> String {
        let mut result = String::new();
        let mut in_token = false;
        let mut in_value = false;
        let mut in_or = false;
        let mut or_token = String::new();
        let mut value_token = String::new();
        let mut token = String::new();

        let mut count = 1;
        if value.starts_with("c") {
            let v: Vec<&str> = value.split(|c| c == 'c' || c == 'v').collect();
            if let Some(&count_text) = v.get(1) {
                if let Ok(c) = count_text.parse::<i32>() {
                    count = c;
                }
            }
        }

        for c in text.chars() {
            if c == '|' {
                in_or = true;
            } else if in_or && (c == ' ' || c == ':') {
                in_or = false;
                result += &self.process_or_token(&or_token, count);
                result += &c.to_string();
            } else if in_or {
                or_token += &c.to_string();
            } else {
                result += &c.to_string();
            }
        }
        let mut updated_text = result;
        result = String::new();

        for c in updated_text.chars() {
            if c == '%' && !in_value {
                in_value = true;
                value_token = String::new();
            } else if c == '%' && in_value {
                in_value = false;
                result += &self.process_value_token(&value_token, value)
            } else if in_value {
                value_token += &c.to_string();
            } else {
                result += &c.to_string();
            }
        }

        if in_value {
            result += &("%".to_owned() + &value_token);
        }

        updated_text = result;
        result = String::new();

        for c in updated_text.chars() {
            if c == '[' {
                in_token = true;
                token = String::new();
            } else if c == ']' {
                in_token = false;
                result += &self.process_token(&token);
            } else if in_token {
                token += &c.to_string();
            } else {
                result += &c.to_string();
            }
        }
        result.to_owned()
    }
}

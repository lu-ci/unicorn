use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::{argument::CommandArgument, payload::CommandPayload};

use rand::Rng;

use crate::mightyparty::client::MightyPartyClient;
use bson::doc;
use std::time::Duration;
use unicorn_embed::UnicornEmbed;
use log::error;

#[derive(Default)]
pub struct RegisterCommand;

impl RegisterCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    pub fn generate_registration_code(&self) -> String {
        let mut rng = rand::thread_rng();
        let mut code = rng.gen_range(0, core::u32::MAX);
        let mut base26 = String::new();
        while code > 0 {
            let next_letter = code % 26;
            code /= 26;
            if let Some(ch) = std::char::from_u32(next_letter + 65) {
                base26 += &ch.to_string();
            }
        }
        base26
    }
}

#[serenity::async_trait]
impl UnicornCommand for RegisterCommand {
    fn command_name(&self) -> &str {
        "mp-register"
    }

    fn category(&self) -> &str {
        "mighty party"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["mpr"]
    }

    fn description(&self) -> &str {
        "Links your discord account to your Mighty Party Account"
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("playerId", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.satisfied() {
            let client = MightyPartyClient::new();
            if let Ok(channel_id) = client.find_channel(pld.args.get("playerId")) {
                let ids = channel_id.split(':').collect::<Vec<&str>>();
                let (_, their_id) = (ids[0], ids[1]);
                let registration_code = self.generate_registration_code();
                if let Ok(()) = client.send_message_to_channel(&channel_id, &format!("Registration Code {}", registration_code)) {
                    pld.msg
                        .channel_id
                        .send_message(pld.ctx.http(), |m| m.content("Enter Registration Code:"))
                        .await?;

                    let rcode = registration_code.clone();
                    if let Some(reply) = pld
                        .msg
                        .channel_id
                        .await_reply(&pld.ctx)
                        .timeout(Duration::from_secs(300))
                        .filter(move |f| f.content.to_lowercase() == rcode.to_lowercase())
                        .await
                    {
                        if reply.content.to_lowercase() == registration_code.to_lowercase() {
                            if let Some(db) = pld.db.get_client() {
                                if let Err(e) = db
                                    .database(&pld.db.cfg.database.name)
                                    .collection("mp-profile-link")
                                    .insert_one(doc! {"id" : u64::from(pld.msg.author.id), "mp_id" : their_id}, None)
                                {
                                    error!("{}", e);
                                    UnicornEmbed::ok("Registration Failed")
                                } else {
                                    UnicornEmbed::ok("Registration Complete")
                                }
                            } else {
                                UnicornEmbed::error("Unable to access DB")
                            }
                        } else {
                            UnicornEmbed::error("This can't happen?")
                        }
                    } else {
                        UnicornEmbed::not_found("Registration timed out")
                    }
                } else {
                    UnicornEmbed::error("Unable to send registration message to MP")
                }
            } else {
                UnicornEmbed::error("Unable to form channel")
            }
        } else {
            UnicornEmbed::error("Missing Player Id")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

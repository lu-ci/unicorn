use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use log::{error, info};

use crate::mightyparty::client::MightyPartyClient;
use crate::mightyparty::model::{Race, Rarity};
use std::collections::HashMap;

#[derive(Default)]
pub struct ShowCardCommand;

impl ShowCardCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ShowCardCommand {
    fn command_name(&self) -> &str {
        "mp-showcard"
    }

    fn category(&self) -> &str {
        "mighty party"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["mpsc"]
    }

    fn description(&self) -> &str {
        "Shows the requested mighty party card at the specified level and reborn. \
        if no reborn or level specified"
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("name", false).allows_spaces(),
            CommandArgument::new("reborn", true).with_value(),
            CommandArgument::new("level", true).with_value(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        //Check for local data files
        //if !exist mp-engine.GetDataFiles()
        let client = MightyPartyClient::new();
        if let Err(err) = client.update_configs(&pld.db) {
            error!("{:?}", err);
        }
        let mut response = if let Ok(monster_levels) = client.monster_from_name(&pld.db, pld.args.get("name")) {
            if monster_levels.len() > 0 {
                let mut race = Race::Order;
                let mut rarity = Rarity::Common;
                let mut id = 0;
                let mut hp = 0;
                let mut atk = 0;
                let mut inner_level = 0;
                let mut inner_id = 0;
                let mut skills = HashMap::<String, String>::new();
                let level = pld.args.get_or_default("level", 1usize);
                for monster in monster_levels {
                    if id == 0 {
                        let inner_levels = monster.promote_type.split(',').collect::<Vec<&str>>().len();
                        inner_id = ((level - 1) / inner_levels) + 1;
                        inner_level = (level - 1) % inner_levels;
                        race = monster.race;
                        rarity = monster.rarity;
                        id = monster.id;
                        info!(
                            "Reading data for monster {}, inner id {}, inner level {}",
                            monster.id, inner_id, inner_level
                        );
                    }
                    if monster.inner_id == inner_id as i32 {
                        //Read basic stats
                        hp = monster.hp;
                        atk = monster.attack;
                        if !monster.skill_value.is_empty() {
                            let skill_values: Vec<&str> = monster.skill_value.split(',').collect();
                            for (i, skill) in monster.skills_id.split(',').into_iter().enumerate() {
                                if let Some(&skill_value) = skill_values.get(i) {
                                    skills.insert(skill.to_owned(), skill_value.to_owned());
                                } else {
                                    error!("Monster {}: Unable to find skill value for {}", monster.id, skill);
                                }
                            }
                        }
                        //Apply level ups
                        let promote_types: Vec<&str> = monster.promote_type.split(',').collect();
                        let promote_values: Vec<&str> = monster.promote_value.split(',').collect();
                        for i in 0..inner_level {
                            if let Some(&promote_type) = promote_types.get(i) {
                                if let Some(&promote_value) = promote_values.get(i) {
                                    match promote_type {
                                        "hp" => hp += promote_value.parse::<i32>()?,
                                        "attack" => atk += promote_value.parse::<i32>()?,
                                        skill => client.upgrade_skill(&mut skills, skill, promote_value),
                                    }
                                } else {
                                    error!("Monster {}: Unable to find promote value for level {}", monster.id, i);
                                }
                            } else {
                                error!("Monster {}: Unable to find promote type for level {}", monster.id, i);
                            }
                        }
                        //Apply Soulbinds
                        let soulbind_types: Vec<&str> = monster.set_type.split(',').collect();
                        let soulbind_values: Vec<&str> = monster.set_value.split(',').collect();
                        for i in 0..4 {
                            if let Some(&soulbind_type) = soulbind_types.get(i) {
                                if let Some(&soulbind_value) = soulbind_values.get(i) {
                                    match soulbind_type {
                                        "hp" => hp += soulbind_value.parse::<i32>()?,
                                        "attack" => atk += soulbind_value.parse::<i32>()?,
                                        _ => error!("Unknown soulbind type {}", soulbind_type),
                                    }
                                } else {
                                    error!("Monster {}: Unable to find promote value for level {}", monster.id, i);
                                }
                            } else {
                                error!("Monster {}: Unable to find promote type for level {}", monster.id, i);
                            }
                        }
                        //Apply Reborns
                        let reborn_count = pld.args.get_or_default("reborn", 0);
                        if reborn_count > 0 {
                            if let Ok(reborns) = client.reborns_for_monster(&pld.db, monster.id) {
                                for i in 0..reborn_count {
                                    if let Some(reborn) = reborns.get(i) {
                                        let bonus_types: Vec<&str> = reborn.bonus_type.split(',').collect();
                                        let bonus_values: Vec<&str> = reborn.bonus_value.split(',').collect();
                                        for j in 0..bonus_types.len() {
                                            if let Some(&bonus_type) = bonus_types.get(j) {
                                                if let Some(&bonus_value) = bonus_values.get(j) {
                                                    match bonus_type {
                                                        "hp" => hp += bonus_value.parse::<i32>()?,
                                                        "attack" => atk += bonus_value.parse::<i32>()?,
                                                        skill => client.upgrade_skill(&mut skills, skill, bonus_value),
                                                    }
                                                } else {
                                                    error!("Monster {}: Unable to find reborn value for reborn {}", monster.id, j);
                                                }
                                            } else {
                                                error!("Monster {}: Unable to find reborn type for reborn {}", monster.id, j);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                let mut skill_output = String::new();
                for (name, value) in skills {
                    let skill_text = client.localized_string(&pld.db, "#skill_", &name);
                    skill_output += &format!("{}\n", client.format_localised_string(&skill_text, &value));
                }
                if skill_output.len() == 0 {
                    skill_output += "None";
                }
                let colour = match rarity {
                    Rarity::Legendary => 0xFBB500,
                    Rarity::Epic => 0xC633FB,
                    Rarity::Rare => 0x3AD3FB,
                    Rarity::Common => 0xBCA250,
                };
                let icon = match race {
                    Race::Order => "https://vignette.wikia.nocookie.net/mightyparty/images/5/58/Order.png/revision/latest",
                    Race::Nature => "https://vignette.wikia.nocookie.net/mightyparty/images/a/a7/Nature.png/revision/latest",
                    Race::Chaos => "https://vignette.wikia.nocookie.net/mightyparty/images/8/8f/Chaos.png/revision/latest",
                };
                let mut msg = CreateMessage::default();
                msg.embed(|e| {
                    e.color(colour);
                    e.author(|a| {
                        a.name(client.monster_name(&pld.db, id));
                        a.icon_url(icon);
                        a
                    });
                    e.field("Attack", atk, true);
                    e.field("Health", hp, true);
                    e.field("Skills", skill_output, false);
                    e
                });
                msg
            //Read monster
            //Read skills
            //Check level/reborn
            //Apply upgrades for level/reborn
            //Translate skills using localisation file
            //Display
            } else {
                unicorn_embed::UnicornEmbed::not_found(format!("Couldn't find {}", pld.args.get("name")))
            }
        } else {
            unicorn_embed::UnicornEmbed::error(format!("Error reading database"))
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

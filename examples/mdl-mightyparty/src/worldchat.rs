use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use log::info;

use crate::mightyparty::client::MightyPartyClient;

#[derive(Default)]
pub struct WorldChatCommand;

impl WorldChatCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for WorldChatCommand {
    fn command_name(&self) -> &str {
        "mp-worldchat"
    }

    fn category(&self) -> &str {
        "mighty party"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["mpwc"]
    }

    fn description(&self) -> &str {
        "Shows the most recent world chat messages"
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let client = MightyPartyClient::new();
        let mut response = if let Ok(messages) = client.world_chat() {
            let mut output = String::new();
            for message in messages {
                let formatted_message = format!("{} {}: {}\n", message.posted_at.format("%H:%M"), message.name.name, message.text);
                if output.len() + formatted_message.len() < 2000 {
                    output += &formatted_message;
                } else {
                    break;
                }
            }
            info!("Total message length {}", output.len());
            let mut msg = CreateMessage::default();
            msg.embed(|e| {
                e.description(output);
                e
            });
            msg
        } else {
            unicorn_embed::UnicornEmbed::error(format!("Error reading world chat"))
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

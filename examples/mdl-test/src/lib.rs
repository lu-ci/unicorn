use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};
use unicorn_callable::module::UnicornModule;
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;

use crate::avatar::AvatarCommand;
use crate::covid::CovidCommand;
use crate::stats::StatsCommand;
use crate::test::TestCommand;
use crate::time::TimeCommand;
use crate::timezone::TimezoneConfig;

mod avatar;
mod covid;
mod stats;
mod test;
mod time;
pub mod timezone;

#[derive(Default)]
pub struct TestModule;

impl UnicornModule for TestModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            CovidCommand::boxed(),
            TestCommand::boxed(),
            TimeCommand::boxed(),
            AvatarCommand::boxed(),
            StatsCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Test"
    }

    fn on_load(&self, _db: &DatabaseHandler, cfg: &mut ConfigurationBuilder) {
        cfg.add::<TimezoneConfig>();
    }
}

define_module!(TestModule, Default::default);

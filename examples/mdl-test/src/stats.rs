use serenity::http::CacheHttp;

use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

const SIGMA_IMAGE: &str = "https://i.imgur.com/mGyqMe1.png";
const SIGMA_TITLE: &str = "Apex Sigma: Statistics";
const SUPPORT_URL: &str = "https://discordapp.com/invite/aEUCHwX";

pub struct StatsCommand;

impl StatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for StatsCommand {
    fn command_name(&self) -> &str {
        "stats"
    }

    fn category(&self) -> &str {
        "utility"
    }

    fn description(&self) -> &str {
        "Displays the current time in UTC."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let cache = pld.ctx.cache.clone();
        let guild_count = pld.ctx.cache.guild_count().await;
        let mut role_count = 0u64;
        let mut user_count = 0u64;
        let channel_count = cache.guild_channel_count().await;
        for gid in &cache.guilds().await {
            if let Some(guild) = cache.guild(gid).await {
                role_count += guild.roles.len() as u64;
                user_count += guild.member_count;
            }
        }
        let _ = pld.msg.channel_id.send_message(pld.ctx.http(), |m| {
            m.embed(|e| {
                e.color(1_797_983);
                e.author(|a| {
                    a.name(SIGMA_TITLE);
                    a.icon_url(SIGMA_IMAGE);
                    a.url(SUPPORT_URL);
                    a
                });
                e.field(
                    "Population",
                    format!(
                        "Guilds: **{}**\nChannels: **{}**\nRoles: **{}**\nUsers: **{}**",
                        guild_count, channel_count, role_count, user_count
                    ),
                    true,
                );
                e
            });
            m
        });
        Ok(())
    }
}

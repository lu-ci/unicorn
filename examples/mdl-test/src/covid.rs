use std::fmt::Formatter;

use chrono::{DateTime, NaiveDateTime, Utc};
use serde::{Deserialize, Serialize};
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

const COVID_URL: &str = "https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/ncov_cases/FeatureServer/2/query?f=json&where=Confirmed%20%3E%200&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=Confirmed%20desc&resultOffset=0&resultRecordCount=100&cacheHint=true";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CovidFeatureOuter {
    pub attributes: CovidFeatureInner,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CovidFeatureInner {
    #[serde(rename = "Country_Region")]
    pub country: String,
    #[serde(rename = "Last_Update")]
    pub timestamp: u64,
    #[serde(rename = "Confirmed")]
    pub confirmed: u64,
    #[serde(rename = "Deaths")]
    pub deaths: u64,
    #[serde(rename = "Recovered")]
    pub recovered: u64,
}

impl CovidFeatureInner {
    fn percentage(a: u64, b: u64) -> f64 {
        (a as f64 / b as f64) * 100f64
    }

    pub fn death_percent(&self) -> f64 {
        Self::percentage(self.deaths, self.confirmed)
    }

    pub fn death_relative(&self) -> f64 {
        let ended = self.deaths + self.recovered;
        if ended != 0 {
            Self::percentage(self.deaths, ended)
        } else {
            0f64
        }
    }

    pub fn recovered_percent(&self) -> f64 {
        Self::percentage(self.recovered, self.confirmed)
    }

    pub fn recovered_relative(&self) -> f64 {
        let ended = self.deaths + self.recovered;
        if ended != 0 {
            Self::percentage(self.recovered, ended)
        } else {
            0f64
        }
    }

    pub fn infected(&self) -> u64 {
        self.confirmed - self.recovered - self.deaths
    }

    pub fn infected_percent(&self) -> f64 {
        Self::percentage(self.infected(), self.confirmed)
    }

    pub fn embed<'a>(&self) -> CreateMessage<'a> {
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x77_b2_55);
            e.title(format!("🦠 COVID-19: {}", self.country));
            e.description(format!(
                "🏥 Confirmed: **{}**\n🌡️ Infected: **{}** ({:.2}%)\n❤️ Recoveries: **{}** ({:.2}%; {:.2}%)\n💀 Deaths: **{}** ({:.2}%; {:.2}%)",
                self.confirmed,
                self.infected(),
                self.infected_percent(),
                self.recovered,
                self.recovered_percent(),
                self.recovered_relative(),
                self.deaths,
                self.death_percent(),
                self.death_relative()
            ));
            e.footer(|f| {
                f.text("Last updated ->");
                f
            });
            e.timestamp(&DateTime::<Utc>::from_utc(
                NaiveDateTime::from_timestamp(self.timestamp as i64 / 1000, 0),
                Utc,
            ));
            e
        });
        msg
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CovidResponse {
    #[serde(rename = "features")]
    pub regions: Vec<CovidFeatureOuter>,
}

#[derive(Debug)]
pub enum CovidError {
    ReqwestError(reqwest::Error),
    SerdeJSONError(serde_json::Error),
}

impl std::fmt::Display for CovidError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CovidError::ReqwestError(err) => err.fmt(f),
            CovidError::SerdeJSONError(err) => err.fmt(f),
        }
    }
}

impl From<serde_json::Error> for CovidError {
    fn from(err: serde_json::Error) -> Self {
        CovidError::SerdeJSONError(err)
    }
}

impl From<reqwest::Error> for CovidError {
    fn from(err: reqwest::Error) -> Self {
        CovidError::ReqwestError(err)
    }
}

impl CovidResponse {
    pub fn new(cmd: &impl UnicornCommand, pld: &CommandPayload) -> Result<Self, CovidError> {
        let key = format!("{}:{}:api", cmd.category(), cmd.command_name());
        let body = if let Some(body) = pld.cache.get(key.clone()) {
            body
        } else {
            let resp = reqwest::blocking::get(COVID_URL)?;
            let body = resp.text()?;
            pld.cache.set(key, body.clone());
            body
        };
        Ok(serde_json::from_str::<Self>(&body)?)
    }

    pub fn region(&self, name: impl ToString) -> Option<CovidFeatureInner> {
        let mut reg = None;
        for region in self.regions.clone() {
            if region.attributes.country.to_lowercase().contains(&name.to_string().to_lowercase()) {
                reg = Some(region.attributes);
                break;
            }
        }
        reg
    }

    pub fn total(&self) -> CovidFeatureInner {
        let mut conf = 0;
        let mut deat = 0;
        let mut reco = 0;
        let mut latest = 0;
        for region in self.regions.clone() {
            conf += region.attributes.confirmed;
            deat += region.attributes.deaths;
            reco += region.attributes.recovered;
            if region.attributes.timestamp > latest {
                latest = region.attributes.timestamp;
            }
        }
        CovidFeatureInner {
            country: "Total".to_owned(),
            timestamp: latest,
            confirmed: conf,
            deaths: deat,
            recovered: reco,
        }
    }
}

#[derive(Default)]
pub struct CovidCommand;

impl CovidCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for CovidCommand {
    fn command_name(&self) -> &str {
        "covid19"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["corona", "covid"]
    }

    fn description(&self) -> &str {
        "Displays the current statistics of the COVID-19 virus. \
         You can also specify a country/region name to get stats only about that place. \
         You can see available regions by adding \"--regions\" to the command."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("region", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = match CovidResponse::new(self, pld) {
            Ok(cvres) => {
                let lookup = pld.args.get("region").to_lowercase();
                if pld.args.has_argument("region") {
                    match lookup.as_ref() {
                        "--regions" => {
                            let mut region_list = Vec::<String>::new();
                            for rgn in cvres.regions {
                                region_list.push(rgn.attributes.country);
                            }
                            region_list.sort();
                            let mut msg = CreateMessage::default();
                            msg.embed(|e| {
                                e.color(0x3b_88_c3);
                                e.title("ℹ COVID-19 Region List");
                                e.description(format!("{}.", region_list.join(", ")));
                                e
                            });
                            msg
                        }
                        _ => {
                            if let Some(region) = cvres.region(lookup) {
                                region.embed()
                            } else {
                                UnicornEmbed::not_found("Couldn't find that region, try the --regions argument")
                            }
                        }
                    }
                } else {
                    cvres.total().embed()
                }
            }
            Err(_why) => UnicornEmbed::error("Failed to get the COVID-19 data."),
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

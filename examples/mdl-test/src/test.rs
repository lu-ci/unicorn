use serenity::http::CacheHttp;

use serenity::builder::CreateMessage;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct TestCommand;

impl TestCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for TestCommand {
    fn command_name(&self) -> &str {
        "test"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn description(&self) -> &str {
        "Just tests some random function."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = CreateMessage::default();
        response.content("Tasty tests tested tastefuly.");
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

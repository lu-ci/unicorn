use serenity::http::CacheHttp;

use unicorn_cache::handler::CacheHandler;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_database::DatabaseHandler;
use unicorn_utility::image::ImageProcessing;

pub struct AvatarCommand;

impl AvatarCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn save_color_to_cache(&self, cache: CacheHandler, url: String, color: u64) {
        cache.set(url, color.to_string());
    }

    fn find_color_in_cache(&self, cache: CacheHandler, url: String) -> Option<u64> {
        match cache.get(url) {
            Some(val) => match val.parse::<u64>() {
                Ok(val) => Some(val),
                Err(_) => None,
            },
            None => None,
        }
    }

    fn save_color_to_db(&self, _db: DatabaseHandler, _url: String, _color: u64) {}

    fn find_color_in_db(&self, _db: DatabaseHandler, _url: String) -> Option<u64> {
        None
    }

    fn save_color(&self, db: DatabaseHandler, cache: CacheHandler, url: String, color: u64) {
        self.save_color_to_db(db, url.clone(), color.clone());
        self.save_color_to_cache(cache, url, color);
    }

    fn find_color(&self, db: DatabaseHandler, cache: CacheHandler, url: String) -> Option<u64> {
        match self.find_color_in_cache(cache.clone(), url.clone()) {
            Some(val) => Some(val),
            None => match self.find_color_in_db(db, url.clone()) {
                Some(val) => {
                    self.save_color_to_cache(cache, url, val.clone());
                    Some(val)
                }
                None => None,
            },
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for AvatarCommand {
    fn command_name(&self) -> &str {
        "avatar"
    }

    fn category(&self) -> &str {
        "utility"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["av", "pfp"]
    }

    fn description(&self) -> &str {
        "Displays the mentioned user's avatar, or the author's if no use is mentioned."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = if !pld.msg.mentions.is_empty() {
            pld.msg.mentions[0].clone()
        } else {
            pld.msg.author.clone()
        };
        let _ = pld.msg.channel_id.send_message(pld.ctx.http(), |m| {
            m.embed(|e| {
                let avatar = match target.avatar_url() {
                    Some(av) => av,
                    None => target.default_avatar_url(),
                };
                let color = match self.find_color(pld.db.clone(), pld.cache.clone(), avatar.clone()) {
                    Some(clr) => clr,
                    None => {
                        let clr = ImageProcessing::from_url(avatar.clone());
                        self.save_color(pld.db.clone(), pld.cache.clone(), avatar.clone(), clr.clone());
                        clr
                    }
                };
                e.color(color);
                e.image(avatar);
                e
            });
            m
        });
        Ok(())
    }
}

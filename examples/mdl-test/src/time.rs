use serenity::http::CacheHttp;

use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

use crate::timezone::TimezoneConfig;

pub struct TimeCommand;

impl TimeCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for TimeCommand {
    fn command_name(&self) -> &str {
        "time"
    }

    fn category(&self) -> &str {
        "utility"
    }

    fn description(&self) -> &str {
        "Displays the current time in UTC."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let time = chrono::Utc::now();
        let mut msg = format!("Time is {} {}", time.format("%d. %m. %Y - %H:%M:%S"), "UTC");
        if let Some(timezone_cfg) = pld.cfg.get_custom::<TimezoneConfig>() {
            if let Some(offset) = chrono::offset::FixedOffset::west_opt(timezone_cfg.offset) {
                let offset_time = time.with_timezone(&offset);
                msg = format!("Time is {} {}", offset_time.format("%d. %m. %Y - %H:%M:%S"), timezone_cfg.timezone);
            }
        }
        let _ = pld.msg.channel_id.send_message(pld.ctx.http(), |m| {
            m.embed(|e| {
                e.color(16_382_457);
                e.title(format!("🕥 {}", msg));
                e
            });
            m
        });
        Ok(())
    }
}

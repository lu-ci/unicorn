use std::collections::HashMap;

use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::guild::Member;
use serenity::model::id::UserId;
use serenity::model::user::User;
use serenity::utils::Colour;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::user::Avatar;

pub const DATE_FMT: &str = "%a, %d. %h. %Y";

#[derive(Default)]
pub struct UserInfoCommand;

impl UserInfoCommand {
    async fn user_to_member(pld: &CommandPayload, user: User) -> Option<Member> {
        if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
            if let Ok(member) = guild.member(pld.ctx.http(), user.id).await {
                Some(member)
            } else {
                None
            }
        } else {
            None
        }
    }

    fn best_member(members: HashMap<UserId, Member>, lookup: &str) -> Option<Member> {
        let mut best = None;
        for (_, member) in &members {
            let user = &member.user.clone();
            let full = format!("{}#{:0>4}", &user.name, &user.discriminator);
            if full.to_lowercase() == lookup.to_lowercase() {
                best = Some(member.clone());
            } else if user.name.to_lowercase() == lookup.to_lowercase() {
                best = Some(member.clone());
            } else if let Some(nick) = &member.nick {
                if nick.to_lowercase() == lookup.to_lowercase() {
                    best = Some(member.clone());
                }
            }
            if best.is_some() {
                break;
            }
        }
        if best.is_none() {
            for (_, member) in &members {
                let user = &member.user.clone();
                if user.name.to_lowercase().contains(&lookup.to_lowercase()) {
                    best = Some(member.clone());
                } else if let Some(nick) = &member.nick {
                    if nick.to_lowercase().contains(&lookup.to_lowercase()) {
                        best = Some(member.clone());
                    }
                }
                if best.is_some() {
                    break;
                }
            }
        }
        best
    }

    async fn search_local(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
            if let Ok(nl) = lookup.parse::<u64>() {
                if let Ok(memb) = guild.member(pld.ctx.http(), nl).await {
                    Some(memb)
                } else {
                    None
                }
            } else {
                Self::best_member(guild.members.clone(), lookup)
            }
        } else {
            None
        }
    }

    async fn search_global(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        let mut best = None;
        let lookup_int = lookup.parse::<u64>();
        for gid in pld.ctx.cache.guilds().await {
            if let Some(guild) = pld.ctx.cache.guild(gid).await {
                if let Ok(nl) = lookup_int {
                    if let Ok(memb) = guild.member(pld.ctx.http(), nl).await {
                        best = Some(memb);
                    }
                }
                if best.is_none() {
                    let members = guild.members.clone();
                    best = Self::best_member(members, lookup);
                    if best.is_some() {
                        break;
                    }
                }
            }
        }
        best
    }

    pub async fn search(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        if let Some(member) = Self::search_local(pld, lookup).await {
            Some(member)
        } else {
            if pld.cfg.discord.owners.contains(&pld.msg.author.id.0) {
                Self::search_global(pld, lookup).await
            } else {
                None
            }
        }
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for UserInfoCommand {
    fn command_name(&self) -> &str {
        "userinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["userinfo", "uinfo"]
    }

    fn example(&self) -> &str {
        "@target"
    }

    fn description(&self) -> &str {
        "Shows information and data on the mentioned user. \
         If no user is mentioned, it will show data for the author."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("target", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = if pld.msg.mentions.is_empty() {
            if pld.args.has_argument("target") {
                Self::search(pld, pld.args.get("target")).await
            } else {
                Self::user_to_member(pld, pld.msg.author.clone()).await
            }
        } else {
            Self::user_to_member(pld, pld.msg.mentions[0].clone()).await
        };
        let mut response = if let Some(target) = target {
            let mut msg = CreateMessage::default();
            let user = target.user.clone();
            let color_option = target.colour(&pld.ctx.cache).await;
            let role_info_option = target.highest_role_info(&pld.ctx.cache).await;
            let role_cache_option = if let Some(role) = role_info_option {
                role.0.to_role_cached(&pld.ctx.cache).await
            } else {
                None
            };
            msg.embed(|e| {
                e.color(color_option.unwrap_or(Colour::from(0)));
                e.author(|a| {
                    let avatar = Avatar::url(target.user.clone());
                    a.name(format!("{}'s Information", target.display_name()));
                    a.icon_url(&avatar);
                    a.url(avatar);
                    a
                });
                e.field(
                    "User Info",
                    format!(
                        "Username: **{}**#{:0>4}\n\
                         ID: **{}**\n\
                         Bot User: **{}**\n\
                         Created: **{}**",
                        &user.name,
                        user.discriminator,
                        user.id.0,
                        if user.bot { "True" } else { "False" },
                        user.created_at().format(DATE_FMT)
                    ),
                    true,
                );
                e.field(
                    "Member Info",
                    format!(
                        "Nickname: **{}**\n\
                         Color: **#{}**\n\
                         Top Role: **{}**\n\
                         Joined: **{}**",
                        if let Some(nick) = &target.nick { nick } else { &user.name },
                        if let Some(color) = color_option {
                            color.hex()
                        } else {
                            "000000".to_owned()
                        },
                        if let Some(role) = role_cache_option {
                            role.name
                        } else {
                            "everyone".to_owned()
                        },
                        if let Some(jd) = target.joined_at {
                            jd.format(DATE_FMT).to_string()
                        } else {
                            "Unknown".to_owned()
                        }
                    ),
                    true,
                );
                e.footer(|f| {
                    f.text(format!(
                        "Use the {}avatar command to see the user's avatar, or click the title.",
                        pld.get_prefix()
                    ));
                    f
                });
                e
            });
            msg
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use std::collections::HashMap;

use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::guild::{Guild, Member, VerificationLevel};
use serenity::model::id::{ChannelId, GuildId, UserId};

use crate::details::user::DATE_FMT;
use serenity::model::channel::{ChannelType, GuildChannel};
use std::collections::hash_map::RandomState;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;

#[derive(Default)]
pub struct GuildInfoCommand;

impl GuildInfoCommand {
    fn count_bots(members: &HashMap<UserId, Member>) -> u64 {
        let mut bots = 0;
        for (_uid, memb) in members {
            let usr = memb.user.clone();
            if usr.bot {
                bots += 1;
            }
        }
        bots
    }

    fn count_channels(channels: &HashMap<ChannelId, GuildChannel, RandomState>) -> (u64, u64, u64) {
        let mut cat = 0;
        let mut text = 0;
        let mut voice = 0;

        for (_cid, carwl) in channels {
            let chn = carwl.clone();
            match chn.kind {
                ChannelType::Text => text += 1,
                ChannelType::Category => cat += 1,
                ChannelType::Voice => voice += 1,
                _ => {}
            }
        }

        (cat, text, voice)
    }

    async fn guild_info(gld: &Guild) -> String {
        let bots = Self::count_bots(&gld.members);
        let (_cats, texts, voices) = Self::count_channels(&gld.channels);
        format!(
            "Name: **{}**\n\
            ID: **{}**\n\
            Roles: **{}**\n\
            Members: **{}** (+{} Bots)\n\
            Channels: **{}** Text; **{}** Voice
            Created: **{}**",
            &gld.name,
            gld.id.0,
            gld.roles.len(),
            gld.member_count - bots,
            bots,
            texts,
            voices,
            gld.id.created_at().format(DATE_FMT)
        )
    }

    async fn owner_info(pld: &CommandPayload, own: &Member) -> String {
        let usr = own.user.clone();
        format!(
            "Name: **{}**#{:0>4}\n\
             Nickname: **{}**\n\
             ID: **{}**\n\
             Color: **#{}**\n\
             Top Role: **{}**\n\
             Created: **{}**\n",
            &usr.name,
            &usr.discriminator,
            if let Some(nick) = &own.nick { nick } else { &usr.name },
            &usr.id.0,
            if let Some(color) = own.colour(&pld.ctx.cache).await {
                color.hex()
            } else {
                "000000".to_owned()
            },
            if let Some(role) = own.highest_role_info(&pld.ctx.cache).await {
                if let Some(role) = role.0.to_role_cached(&pld.ctx.cache).await {
                    role.name
                } else {
                    "everyone".to_owned()
                }
            } else {
                "everyone".to_owned()
            },
            usr.created_at().format(DATE_FMT),
        )
    }

    async fn details(pld: &CommandPayload, gld: &Guild) -> String {
        format!(
            "AFK: **{}** ({}s)\n\
             Emojis: **{}**\n\
             Large: **{}**\n\
             Region: **{}**\n\
             Shard: **{}**\n\
             Verification: **{}**",
            if let Some(afk_cid) = gld.afk_channel_id {
                if let Some(afk_chn) = gld.channels.get(&afk_cid) {
                    afk_chn.name.clone()
                } else {
                    "None".to_owned()
                }
            } else {
                "None".to_owned()
            },
            gld.afk_timeout,
            gld.emojis.len(),
            if gld.is_large() { "Yes" } else { "No" },
            gld.region.to_uppercase(),
            gld.shard_id(&pld.ctx.cache).await,
            match gld.verification_level {
                VerificationLevel::Low => "Low",
                VerificationLevel::Medium => "Medium",
                VerificationLevel::High => "High",
                VerificationLevel::Higher => "Extreme",
                _ => "None",
            }
        )
    }

    async fn search(pld: &CommandPayload, lookup: impl ToString) -> Option<GuildId> {
        let mut best = None;
        let lookup = lookup.to_string();
        let guilds = pld.ctx.cache.guilds().await;
        for gid in &guilds {
            if let Some(guild) = pld.ctx.cache.guild(gid).await {
                if guild.name.to_lowercase() == lookup.to_lowercase() {
                    best = Some(gid.clone());
                    break;
                }
            }
        }
        if best.is_none() {
            for gid in &guilds {
                if let Some(guild) = pld.ctx.cache.guild(gid).await {
                    if guild.name.to_lowercase().contains(&lookup.to_lowercase()) {
                        best = Some(gid.clone());
                        break;
                    }
                }
            }
        }
        best
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GuildInfoCommand {
    fn command_name(&self) -> &str {
        "serverinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["srvinfo", "sinfo", "guildinformation", "guildinfo", "ginfo"]
    }

    fn description(&self) -> &str {
        "Shows information about the guild/server you execute this command on."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let guild = if pld.args.has_argument("lookup") && pld.cfg.discord.owners.contains(&pld.msg.author.id.0) {
            if let Ok(gid) = pld.args.get("lookup").parse::<u64>() {
                pld.ctx.cache.guild(gid).await
            } else {
                if let Some(gid) = Self::search(pld, pld.args.get("lookup")).await {
                    pld.ctx.cache.guild(gid).await
                } else {
                    None
                }
            }
        } else {
            pld.msg.guild(&pld.ctx.cache).await
        };
        let mut response = if let Some(gld) = guild {
            let mut msg = CreateMessage::default();
            let (icon, color) = if let Some(icon) = gld.icon_url() {
                (icon.clone(), ImageProcessing::from_url(icon.clone()))
            } else {
                ("https://i.imgur.com/QnYSlld.png".to_owned(), 0x1b_6f_5f)
            };
            let owner_option = gld.member(pld.ctx.http(), &gld.owner_id).await;
            let owner_info = if let Ok(own) = owner_option {
                Self::owner_info(&pld, &own).await
            } else {
                "Failed to retrieve owner information.".to_owned()
            };
            let details = Self::details(&pld, &gld).await;
            let guild_info = Self::guild_info(&gld).await;
            msg.embed(|e| {
                e.color(color);
                e.author(|a| {
                    a.icon_url(&icon);
                    a.url(&icon);
                    a.name(&gld.name);
                    a
                });
                e.field("Guild Info", guild_info, true);
                e.field("Owner Info", owner_info, true);
                e.field("Details", details, true);
                e
            });
            msg
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

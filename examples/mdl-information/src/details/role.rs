use serenity::http::CacheHttp;

use crate::details::user::DATE_FMT;
use serenity::builder::CreateMessage;
use serenity::model::guild::{Guild, Role};
use serenity::model::id::RoleId;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct RoleInfoCommand;

impl RoleInfoCommand {
    fn count_members(guild: &Guild, role: &Role) -> u64 {
        let mut counter = 0;
        for (_uid, member) in &guild.members {
            for mrole in &member.roles {
                if mrole.0 == role.id.0 {
                    counter += 1;
                    break;
                }
            }
        }
        counter
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn describe(guild: &Guild, role: &Role) -> String {
        format!(
            "Name: **{}**\n\
             ID: **{}**\n\
             Color: **#{}**\n\
             Hoisted: **{}**\n\
             Members: **{}**\n\
             Created: **{}**",
            role.name,
            role.id.0,
            role.colour.hex(),
            if role.hoist { "Yes" } else { "No" },
            if guild.id.0 == role.id.0 {
                guild.member_count
            } else {
                Self::count_members(guild, role)
            },
            role.id.created_at().format(DATE_FMT)
        )
    }

    pub async fn find_role(pld: &CommandPayload) -> (bool, Option<RoleId>) {
        if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            if lookup.starts_with("<@&") && lookup.ends_with(">") {
                if let Ok(rid_num) = lookup.trim_start_matches("<@&").trim_end_matches(">").parse::<u64>() {
                    (false, Some(RoleId(rid_num)))
                } else {
                    (false, None)
                }
            } else {
                let mut best = None;
                if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
                    for (rid, role) in &guild.roles {
                        if role.name.to_lowercase() == lookup.to_lowercase() {
                            best = Some(rid.clone());
                            break;
                        }
                    }
                    if best.is_none() {
                        for (rid, role) in &guild.roles {
                            if role.name.to_lowercase().contains(&lookup.to_lowercase()) {
                                best = Some(rid.clone());
                                break;
                            }
                        }
                    }
                }
                (false, best)
            }
        } else {
            if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
                if let Ok(member) = guild.member(pld.ctx.http(), pld.msg.author.id).await {
                    if let Some((top_role, _)) = member.highest_role_info(&pld.ctx.cache).await {
                        (true, Some(top_role))
                    } else {
                        (true, None)
                    }
                } else {
                    (true, None)
                }
            } else {
                (true, None)
            }
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for RoleInfoCommand {
    fn command_name(&self) -> &str {
        "roleinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["roleinfo", "rinfo"]
    }

    fn example(&self) -> &str {
        "Bouncer"
    }

    fn description(&self) -> &str {
        "Shows information and data on the specified role. \
         Roles mentions do not work here, \
         lookup is done via role name or role mention. \
         If no lookup is given, it will show information about the author's highest role."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let (no_lookup, rid) = Self::find_role(&pld).await;
        let mut response = if let Some(rid) = rid {
            if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
                let guild = guild.clone();
                if let Some(role) = guild.roles.get(&rid) {
                    let mut msg = CreateMessage::default();
                    msg.embed(|e| {
                        e.author(|a| {
                            a.name(format!("{} Information", &role.name));
                            if let Some(icon) = guild.icon_url() {
                                a.icon_url(icon);
                            }
                            a
                        });
                        e.color(role.colour);
                        e.description(Self::describe(&guild, &role));
                        e
                    });
                    msg
                } else {
                    UnicornEmbed::error("Failed getting the role from the guild's cache.")
                }
            } else {
                UnicornEmbed::error("Failed to get guild information.")
            }
        } else {
            if no_lookup {
                UnicornEmbed::error("You don't have any roles.")
            } else {
                UnicornEmbed::not_found("Role not found.")
            }
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

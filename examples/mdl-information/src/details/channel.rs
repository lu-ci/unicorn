use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::id::ChannelId;

use crate::details::user::DATE_FMT;
use crate::utils::ChannelUtils;
use serenity::model::channel::{ChannelType, GuildChannel};
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;

#[derive(Default)]
pub struct ChannelInfoCommand;

impl ChannelInfoCommand {
    async fn search_global(pld: &CommandPayload, lookup: &str) -> Option<ChannelId> {
        let mut best = None;
        let guilds = pld.ctx.cache.guilds().await;
        for gid in &guilds {
            if let Some(guild) = pld.ctx.cache.guild(gid).await {
                best = ChannelUtils::search_guild(guild, lookup);
                if best.is_some() {
                    break;
                }
            }
        }
        best
    }

    async fn search(pld: &CommandPayload, lookup: impl ToString) -> Option<ChannelId> {
        let lookup = lookup.to_string();
        if let Some(cid) = ChannelUtils::search_local(pld, &lookup).await {
            Some(cid)
        } else {
            if pld.cfg.discord.owners.contains(&pld.msg.author.id.0) {
                Self::search_global(pld, &lookup).await
            } else {
                None
            }
        }
    }

    fn describe(chn: &GuildChannel) -> String {
        format!(
            "Name: **{}**\n\
             ID: **{}**\n\
             Type: **{}**\n\
             Position: **{}**\n\
             NSFW: **{}**\n\
             Created: **{}**",
            match chn.kind {
                ChannelType::Text => format!("#{}", chn.name),
                _ => chn.name.clone(),
            },
            chn.id.0,
            match chn.kind {
                ChannelType::Text => format!("Text Channel"),
                ChannelType::Voice => format!("Voice Channel"),
                _ => format!("Weird Channel"),
            },
            chn.position,
            if chn.is_nsfw() { "Yes" } else { "No" },
            chn.id.created_at().format(DATE_FMT)
        )
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ChannelInfoCommand {
    fn command_name(&self) -> &str {
        "channelinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["chinfo", "cinfo", "chinformation", "channelinfo"]
    }

    fn description(&self) -> &str {
        "Shows information about the mentioned channel. \
         If you don't tag a channel, it will target the channel that \
         the the command was executed in."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("channel", true)
                .available_when(|s| s.starts_with("<#") && s.ends_with(">"))
                .anywhere(),
            CommandArgument::new("lookup", false).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mentions = ChannelUtils::mentioned_channels(pld).await;
        let cid = if !mentions.is_empty() {
            Some(mentions[0])
        } else {
            if pld.args.has_argument("lookup") {
                let lookup = pld.args.get("lookup");
                Self::search(&pld, lookup).await
            } else {
                Some(pld.msg.channel_id)
            }
        };
        let mut response = if let Some(cid) = cid {
            if let Some(chn) = cid.to_channel_cached(&pld.ctx.cache).await {
                let is_local = if let Some(gld) = pld.msg.guild(&pld.ctx.cache).await {
                    if gld.channels.contains_key(&cid) {
                        true
                    } else {
                        false
                    }
                } else {
                    false
                };
                if let Some(chn) = chn.guild() {
                    let mut msg = CreateMessage::default();
                    let guild_option = chn.guild(&pld.ctx.cache).await;
                    msg.embed(|e| {
                        let mut color = 0x1b_6f_5f;
                        e.description(Self::describe(&chn));
                        if let Some(guild) = guild_option {
                            e.author(|a| {
                                a.name(if is_local {
                                    format!(
                                        "{}: {}",
                                        guild.name.clone(),
                                        match chn.kind {
                                            ChannelType::Text => format!("#{}", &chn.name),
                                            _ => chn.name.clone(),
                                        }
                                    )
                                } else {
                                    format!(
                                        "{}: {} (External)",
                                        &guild.name,
                                        match chn.kind {
                                            ChannelType::Text => format!("#{}", &chn.name),
                                            _ => chn.name.clone(),
                                        }
                                    )
                                });
                                if let Some(icon) = guild.icon_url() {
                                    a.icon_url(&icon);
                                    color = ImageProcessing::from_url(icon);
                                }
                                a
                            });
                        }
                        e.color(color);
                        e
                    });
                    msg
                } else {
                    UnicornEmbed::error("Missing channel guild entry!")
                }
            } else {
                UnicornEmbed::error("Missing channel cache entry!")
            }
        } else {
            UnicornEmbed::not_found("Channel not found.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

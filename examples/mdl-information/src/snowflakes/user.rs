use serenity::http::CacheHttp;

use crate::details::user::UserInfoCommand;
use serenity::builder::CreateMessage;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct UserIDCommand;

impl UserIDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for UserIDCommand {
    fn command_name(&self) -> &str {
        "userid"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["usrid", "uid"]
    }

    fn description(&self) -> &str {
        "Shows the target user's snowflake (ID). \
         Add the \"--text\" parameter to the command to return it as a \
         regular text messages instead of an embed. Useful if you're on a mobile device."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("--text", true).is_flag(),
            CommandArgument::new("lookup", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = CreateMessage::default();
        let text = pld.args.has_argument("--text");
        let uid = if !pld.msg.mentions.is_empty() {
            Some(pld.msg.mentions[0].id.0.to_string())
        } else {
            if pld.args.has_argument("lookup") {
                let lookup = pld.args.get("lookup");
                if let Some(member) = UserInfoCommand::search(pld, lookup).await {
                    Some(member.user.id.0.to_string())
                } else {
                    None
                }
            } else {
                Some(pld.msg.author.id.0.to_string())
            }
        };
        let uid = if let Some(uid) = uid { uid } else { "User not found.".to_owned() };
        if text {
            response.content(uid);
        } else {
            response.embed(|e| {
                e.title(format!("🔢 {}", uid));
                e.colour(0x3b_88_c3);
                e
            });
        }
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

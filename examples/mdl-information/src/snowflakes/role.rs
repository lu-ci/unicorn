use serenity::http::CacheHttp;

use crate::details::role::RoleInfoCommand;
use serenity::builder::CreateMessage;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct RoleIDCommand;

impl RoleIDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for RoleIDCommand {
    fn command_name(&self) -> &str {
        "roleid"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["rlid", "rid"]
    }

    fn description(&self) -> &str {
        "Shows the mentioned role's snoflake (ID). \
         To avoid pinging users in a mentionable role, use the role name or ID instead. \
         Add the \"--text\" parameter to the command to return it as a \
         regular text messages instead of an embed. Useful if you're on a mobile device."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("--text", true).is_flag(),
            CommandArgument::new("lookup", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = CreateMessage::default();
        let text = pld.args.has_argument("--text");
        let (no_lookup, rid) = RoleInfoCommand::find_role(pld).await;
        if text {
            if let Some(rid) = rid {
                response.content(rid.0.to_string());
            } else {
                if no_lookup {
                    response.content("You don't have any roles.");
                } else {
                    response.content("Role not found.");
                }
            }
        } else {
            if let Some(rid) = rid {
                response.embed(|e| {
                    e.title(format!("🔢 {}", rid));
                    e.colour(0x3b_88_c3);
                    e
                });
            } else {
                if no_lookup {
                    response = UnicornEmbed::error("You don't have any roles.");
                } else {
                    response = UnicornEmbed::not_found("Role not found.");
                }
            }
        }
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

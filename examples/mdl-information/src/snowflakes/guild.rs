use serenity::http::CacheHttp;

use serenity::builder::CreateMessage;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct GuildIDCommand;

impl GuildIDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GuildIDCommand {
    fn command_name(&self) -> &str {
        "guildid"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["gldid", "gid", "serverid", "srvid", "sid"]
    }

    fn description(&self) -> &str {
        "Shows the current guild's snowflake (ID). \
         Add the \"--text\" parameter to the command to return it as a \
         regular text messages instead of an embed. Useful if you're on a mobile device."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("--text", true).is_flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = CreateMessage::default();
        let text = pld.args.has_argument("--text");
        let gid = if let Some(gid) = pld.msg.guild_id {
            gid.0.to_string()
        } else {
            "Unknown".to_owned()
        };
        if text {
            response.content(gid);
        } else {
            response.embed(|e| {
                e.title(format!("🔢 {}", gid));
                e.colour(0x3b_88_c3);
                e
            });
        }
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

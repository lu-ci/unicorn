use serenity::http::CacheHttp;
use serenity::model::channel::ChannelType;

use crate::utils::ChannelUtils;
use serenity::builder::CreateMessage;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct ChannelIDCommand;

impl ChannelIDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ChannelIDCommand {
    fn command_name(&self) -> &str {
        "channelid"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["chnid", "chid", "cid"]
    }

    fn description(&self) -> &str {
        "Shows the tagged channel's snowflake (ID). \
         Add the \"--text\" parameter to the command to return it as a \
         regular text messages instead of an embed. Useful if you're on a mobile device. \
         If you do not tag a channel or enter a lookup, the command with target the channel \
         that it is executed in."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("--text", true).is_flag(),
            CommandArgument::new("lookup", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mentioned = ChannelUtils::mentioned_channels(pld).await;
        let text = pld.args.has_argument("text");
        let target = if !mentioned.is_empty() {
            Some(mentioned[0])
        } else {
            if pld.args.has_argument("lookup") {
                ChannelUtils::search_local(pld, pld.args.get("lookup")).await
            } else {
                Some(pld.msg.channel_id.clone())
            }
        };
        let target = if let Some(cid) = target {
            if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
                if let Some(chn) = guild.channels.get(&cid) {
                    Some(chn.clone())
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        };
        let mut response = if let Some(target) = target {
            let mut msg = CreateMessage::default();
            if text {
                msg.content(target.id.0.to_string());
            } else {
                msg.embed(|e| {
                    e.title(format!(
                        "{} {}",
                        match target.kind {
                            ChannelType::Text => "#️⃣",
                            _ => "🎶",
                        },
                        match target.kind {
                            ChannelType::Text => format!("#{}", &target.name),
                            _ => target.name.clone(),
                        }
                    ));
                    e.description(target.id.0.to_string());
                    e.colour(0x3b_88_c3);
                    e
                });
            }
            msg
        } else {
            UnicornEmbed::not_found("Channel not found.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use crate::common::common::FAHError;
use chrono::Datelike;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct FAHTeamLookupResult {
    pub wus: u64,
    pub credit_cert: String,
    pub name: String,
    pub rank: u64,
    pub credit: u64,
    pub team: u64,
    pub wus_cert: String,
}

impl FAHTeamLookupResult {
    fn api_url(&self) -> String {
        format!("https://stats.foldingathome.org/api/team/{}", &self.team)
    }

    pub fn fetch(&self) -> Result<FAHTeam, FAHError> {
        let resp = reqwest::blocking::get(&self.api_url())?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<FAHTeam>(&body)?)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct FAHTeamLookup {
    pub description: String,
    pub monthly: bool,
    pub results: Vec<FAHTeamLookupResult>,
    pub month: u8,
    pub year: u16,
    pub query: String,
    pub path: String,
}

impl FAHTeamLookup {
    fn is_id(lookup: &str) -> bool {
        lookup.parse::<u64>().is_ok()
    }

    fn id_url(lookup: &str) -> String {
        let now = chrono::Utc::now();
        format!(
            "https://stats.foldingathome.org/api/teams?name=&search_type=exact&passkey=&team={}&month={}&year={}",
            lookup,
            now.month(),
            now.year()
        )
    }

    fn name_url(lookup: &str) -> String {
        let now = chrono::Utc::now();
        format!(
            "https://stats.foldingathome.org/api/teams?name={}&search_type=exact&passkey=&team=&month={}&year={}",
            lookup,
            now.month(),
            now.year()
        )
    }

    fn search_by_id(lookup: &str) -> Result<Self, FAHError> {
        let resp = reqwest::blocking::get(&Self::id_url(lookup))?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<Self>(&body)?)
    }

    fn search_by_name(lookup: &str) -> Result<Self, FAHError> {
        let resp = reqwest::blocking::get(&Self::name_url(lookup))?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<Self>(&body)?)
    }

    pub fn search(lookup: impl ToString) -> Result<Self, FAHError> {
        let lookup = lookup.to_string();
        if Self::is_id(&lookup) {
            Self::search_by_id(&lookup)
        } else {
            Self::search_by_name(&lookup)
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct FAHTeamDonor {
    pub wus: u64,
    pub name: String,
    pub rank: Option<u64>,
    pub credit: u64,
    pub team: u64,
    pub id: u64,
}

impl FAHTeamDonor {
    pub fn url(&self) -> String {
        format!("https://stats.foldingathome.org/donor/{}", self.id)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct FAHTeam {
    pub wus: u64,
    pub donors: Vec<FAHTeamDonor>,
    pub rank: u64,
    pub total_teams: u64,
    pub active_50: u64,
    pub logo: Option<String>,
    pub wus_cert: String,
    pub credit_cert: String,
    pub last: String,
    pub name: String,
    pub credit: u64,
    pub team: u64,
    pub path: String,
    #[serde(rename = "url")]
    pub site: Option<String>,
}

impl FAHTeam {
    pub fn url(&self) -> String {
        format!("https://stats.foldingathome.org/team/{}", self.team)
    }

    pub fn sort_donors(donors: Vec<FAHTeamDonor>) -> Vec<FAHTeamDonor> {
        let mut donors = donors.clone();
        donors.sort_by(|a, b| a.credit.cmp(&b.credit));
        donors.reverse();
        donors
    }
    fn cut_donors(donors: Vec<FAHTeamDonor>) -> Vec<FAHTeamDonor> {
        if donors.len() > 5 {
            let (first, _last) = donors.split_at(5);
            first.to_vec()
        } else {
            donors
        }
    }
    pub fn describe(&self) -> String {
        format!(
            "ID: **{}**\nScore: **{}**\nWork Units: **{}**\nActive CPUs: **{}**\nRanking: **{}** of {}\nLast Work Unit: **{}**",
            self.team, self.credit, self.wus, self.active_50, self.rank, self.total_teams, &self.last
        )
    }

    pub fn describe_donors(&self) -> String {
        let mut donors = Self::sort_donors(self.donors.clone());
        let donor_count = donors.len();
        donors = Self::cut_donors(donors);
        if !donors.is_empty() {
            let mut dstrs = Vec::<String>::new();
            for donor in &donors {
                dstrs.push(format!("[{}]({}): **{}** ({})", donor.name, donor.url(), donor.wus, donor.credit));
            }
            format!("{}\n{} has **{}** donors.", dstrs.join("\n"), &self.name, donor_count)
        } else {
            format!("{} hasn't had any donors so far.", &self.name)
        }
    }
}

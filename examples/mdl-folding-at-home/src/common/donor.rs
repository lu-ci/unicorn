use crate::common::common::FAHError;
use chrono::Datelike;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct FAHDonorResult {
    pub wus: u64,
    pub credit_cert: String,
    pub name: String,
    pub rank: Option<u64>,
    pub credit: u64,
    pub team: Option<u64>,
    pub wus_cert: String,
    pub id: u64,
}

impl FAHDonorResult {
    pub fn fetch(&self) -> Result<FAHDonor, FAHError> {
        let url = format!("https://stats.foldingathome.org/api/donor/{}", self.id);
        let resp = reqwest::blocking::get(&url)?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<FAHDonor>(&body)?)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct FAHDonorLookup {
    pub description: String,
    pub monthly: bool,
    pub results: Vec<FAHDonorResult>,
    pub month: u8,
    pub year: u16,
    pub query: String,
    pub path: String,
}

impl FAHDonorLookup {
    fn api_url(lookup: &str) -> String {
        let now = chrono::Utc::now();
        format!(
            "https://stats.foldingathome.org/api/donors?name={}&search_type=exact&passkey=&team=&month={}&year={}",
            lookup,
            now.month(),
            now.year()
        )
    }

    pub fn search(lookup: impl ToString) -> Result<Self, FAHError> {
        let lookup = lookup.to_string();
        let resp = reqwest::blocking::get(&Self::api_url(&lookup))?;
        let body = resp.text()?;
        Ok(serde_json::from_str::<Self>(&body)?)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct FAHDonorTeam {
    pub wus: u64,
    #[serde(default = "FAHDonorTeam::default_last")]
    pub last: String,
    pub uid: u64,
    pub active_50: u64,
    pub active_7: u64,
    pub credit: u64,
    pub team: u64,
    pub name: String,
}

impl FAHDonorTeam {
    fn default_last() -> String {
        "Unknown".to_owned()
    }

    pub fn url(&self) -> String {
        format!("https://stats.foldingathome.org/team/{}", self.team)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct FAHDonor {
    pub wus: u64,
    pub rank: Option<u64>,
    pub total_users: u64,
    pub active_50: u64,
    pub path: String,
    pub wus_cert: String,
    pub id: u64,
    pub credit_cert: String,
    pub last: String,
    pub name: String,
    pub teams: Vec<FAHDonorTeam>,
    pub active_7: u64,
    pub credit: u64,
}

impl FAHDonor {
    fn sort_teams(teams: Vec<FAHDonorTeam>) -> Vec<FAHDonorTeam> {
        let mut clean = Vec::<FAHDonorTeam>::new();
        for team in &teams {
            if team.team != 0u64 {
                clean.push(team.clone());
            }
        }
        clean.sort_by(|a, b| a.credit.cmp(&b.credit));
        clean.reverse();
        clean
    }

    fn cut_teams(teams: Vec<FAHDonorTeam>) -> Vec<FAHDonorTeam> {
        if teams.len() > 5 {
            let (first, _last) = teams.split_at(5);
            first.to_vec()
        } else {
            teams
        }
    }

    pub fn url(&self) -> String {
        format!("https://stats.foldingathome.org/donor/{}", self.id)
    }

    pub fn describe_teams(&self) -> String {
        let mut teams = Self::sort_teams(self.teams.clone());
        let team_count = teams.len();
        teams = Self::cut_teams(teams);
        if !teams.is_empty() {
            let mut tstrs = Vec::<String>::new();
            for team in &teams {
                tstrs.push(format!("[{}]({}): **{}** ({})", team.name, team.url(), team.wus, team.credit,));
            }
            format!("{}\n{} has participated in **{}** teams.", tstrs.join("\n"), &self.name, team_count)
        } else {
            format!("{} has not participated in any teams.", &self.name)
        }
    }

    pub fn describe(&self) -> String {
        format!(
            "ID: **{}**\n\
            Work Units: **{}**\n\
            Score: **{}**\n\
            Active Clients in 7/50 Days: **{}/{}**\n\
            Ranking: **{}** of {}\n\
            Last Work Unit: **{}**",
            self.id,
            self.wus,
            self.credit,
            self.active_7,
            self.active_50,
            if let Some(rank) = self.rank {
                rank.to_string()
            } else {
                "Unranked".to_owned()
            },
            self.total_users,
            &self.last
        )
    }
}

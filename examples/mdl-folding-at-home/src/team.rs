use log::debug;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;

use crate::common::common::{FAH_COLOR, FAH_ICON};
use crate::common::team::FAHTeamLookup;

#[derive(Default)]
pub struct TeamCommand;

impl TeamCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for TeamCommand {
    fn command_name(&self) -> &str {
        "fahteam"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["f@hteam"]
    }

    fn example(&self) -> &str {
        "Lucia's Cipher"
    }

    fn description(&self) -> &str {
        "Displays the information about a Folding@Home team. \
         The looking can be either the team name or ID."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut wait_emb = UnicornEmbed::info("Please wait, the F@H API can be slow and unstable...");
        let wait_msg = pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut wait_emb).await;
        let mut response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            match FAHTeamLookup::search(lookup) {
                Ok(res) => {
                    if !res.results.is_empty() {
                        match res.results[0].fetch() {
                            Ok(team) => {
                                let mut msg = CreateMessage::default();
                                msg.embed(|e| {
                                    e.color(FAH_COLOR);
                                    if let Some(site) = &team.site {
                                        e.description(format!("This team has a website, you can visit it [here]({}).", site));
                                    }
                                    e.author(|a| {
                                        a.name(format!("Folding@Home: {}", &team.name));
                                        a.icon_url(if let Some(logo) = &team.logo { logo } else { FAH_ICON });
                                        a.url(&team.url());
                                        a
                                    });
                                    e.field("Information", team.describe(), true);
                                    e.field("Top Donors", team.describe_donors(), true);
                                    e
                                });
                                msg
                            }
                            Err(why) => {
                                debug!("{:?}", why);
                                UnicornEmbed::error("Failed to communicate with the F@H team API.")
                            }
                        }
                    } else {
                        UnicornEmbed::not_found("F@H team not found.")
                    }
                }
                Err(why) => {
                    debug!("{:?}", why);
                    UnicornEmbed::error("Failed to communicate with the F@H lookup API.")
                }
            }
        } else {
            UnicornEmbed::error("No team name or ID given.")
        };
        if let Ok(wm) = wait_msg {
            let _ = wm.delete(pld.ctx.http()).await;
        }
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

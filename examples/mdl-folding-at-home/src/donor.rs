use log::debug;

use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;

use crate::common::common::{FAH_COLOR, FAH_ICON};
use crate::common::donor::FAHDonorLookup;

#[derive(Default)]
pub struct DonorCommand;

impl DonorCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for DonorCommand {
    fn command_name(&self) -> &str {
        "fahdonor"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["f@hdonor", "f@huser", "fahuser"]
    }

    fn example(&self) -> &str {
        "AXAz0r"
    }

    fn description(&self) -> &str {
        "Displays the information about a Folding@Home donor/user by their username."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut wait_emb = UnicornEmbed::info("Please wait, the F@H API can be slow and unstable...");
        let wait_msg = pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut wait_emb).await;
        let mut response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            match FAHDonorLookup::search(lookup) {
                Ok(res) => {
                    if !res.results.is_empty() {
                        match res.results[0].fetch() {
                            Ok(usr) => {
                                let mut msg = CreateMessage::default();
                                msg.embed(|e| {
                                    e.color(FAH_COLOR);
                                    e.author(|a| {
                                        a.name(format!("Folding@Home: {}", &usr.name));
                                        a.icon_url(FAH_ICON);
                                        a.url(usr.url());
                                        a
                                    });
                                    e.field("Information", usr.describe(), true);
                                    e.field("Team Participation", usr.describe_teams(), true);
                                    e
                                });
                                msg
                            }
                            Err(why) => {
                                debug!("{:?}", why);
                                UnicornEmbed::error("Failed to communicate with the F@H donor API.")
                            }
                        }
                    } else {
                        UnicornEmbed::not_found("F@H donor not found.")
                    }
                }
                Err(why) => {
                    debug!("{:?}", why);
                    UnicornEmbed::error("Failed to communicate with the F@H lookup API.")
                }
            }
        } else {
            UnicornEmbed::error("No donor name given.")
        };
        if let Ok(wm) = wait_msg {
            let _ = wm.delete(pld.ctx.http()).await;
        }
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use std::borrow::Cow;

use log::debug;
use serenity::builder::CreateMessage;
use serenity::http::{AttachmentType, CacheHttp};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::common::open_api::OpenAPIEntry;
use crate::common::util::Utility;

const API_BASE: &str = "http://api.obutts.ru/butts";
const IMG_BASE: &str = "http://media.obutts.ru";
const API_ICON: &str = "https://i.imgur.com/zjndjaj.png";
const API_NAME: &str = "Open Butts";
const MAX_ROLL: u64 = 7302;
const RESP_CLR: u64 = 0xf9_f9_f9;

const NUM_ERR: &str = "Invalid image number given.";
const FAIL_ERR: &str = "Failed to communicate with the butt service.";
const EMPTY_ERR: &str = "This lookup gave no results.";

#[derive(Default)]
pub struct ButtsCommand;

impl ButtsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ButtsCommand {
    fn command_name(&self) -> &str {
        "butts"
    }

    fn category(&self) -> &str {
        "nsfw"
    }

    fn nsfw(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["ass"]
    }

    fn description(&self) -> &str {
        "Outputs a random NSFW image focusing on the posterior of the model. \
         You can specify the ID number of the image if you want a specific one."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("index", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let num = OpenAPIEntry::get_num(&pld.args, MAX_ROLL);
        let mut response = match num {
            Some(num) => {
                let key = Utility::cache_key(self.category(), self.command_name(), num);
                let url = format!("{}/{}", API_BASE, num);
                let (fresh, oae) = match OpenAPIEntry::from_cache(pld, key.clone()) {
                    Ok(oae) => match oae {
                        Some(oae) => (false, Ok(Some(oae))),
                        None => (true, OpenAPIEntry::new(&url)),
                    },
                    Err(_) => (true, OpenAPIEntry::new(&url)),
                };
                match oae {
                    Ok(oar) => {
                        if let Some(oar) = oar {
                            if fresh {
                                oar.save_cache(pld, key);
                            }
                            let mut r = CreateMessage::default();
                            let img_name = Utility::get_img_name(&oar.get_image_url(IMG_BASE), pld.msg.id.0);
                            let downloaded = match Utility::get_img_bytes(&oar.get_image_url(IMG_BASE)) {
                                Ok(bytes) => {
                                    if bytes.len() < 8_388_608 {
                                        r.add_file(AttachmentType::Bytes {
                                            data: Cow::from(bytes),
                                            filename: img_name.clone(),
                                        });
                                        true
                                    } else {
                                        debug!("Image too large to upload to discord.");
                                        false
                                    }
                                }
                                Err(why) => {
                                    debug!("Image byte pulling failed: {}", why);
                                    false
                                }
                            };
                            r.embed(|e| {
                                e.color(RESP_CLR);
                                e.author(|a| {
                                    a.name(API_NAME);
                                    a.icon_url(API_ICON);
                                    a
                                });
                                if downloaded {
                                    e.image(format!("attachment://{}", &img_name));
                                } else {
                                    e.image(oar.get_image_url(IMG_BASE));
                                }
                                e.footer(|f| {
                                    f.text(format!("ID: {} | Model: {} | Ranking: {}", num, oar.get_model_name(), oar.rank));
                                    f
                                });
                                e
                            });
                            r
                        } else {
                            UnicornEmbed::error(EMPTY_ERR)
                        }
                    }
                    Err(why) => {
                        debug!("Failed getting {} image: {}", self.command_name(), why);
                        UnicornEmbed::error(FAIL_ERR)
                    }
                }
            }
            None => UnicornEmbed::error(NUM_ERR),
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use std::io::Read;

use unicorn_utility::hash::UnicornHasher;

use crate::common::error::NSFWError;

pub struct Utility;

impl Utility {
    pub fn cache_key(cat: &str, name: &str, num: u64) -> String {
        format!("cmd:{}:{}:{}", cat, name, num)
    }

    pub fn sort_tags(mut tags: Vec<&str>) -> Vec<&str> {
        tags.sort();
        tags
    }

    pub fn tag_key(tags: Vec<&str>) -> u64 {
        let sorted = Self::sort_tags(tags);
        let stringed = sorted.join("-");
        UnicornHasher::calculate_u64_hash(&stringed)
    }

    pub fn get_img_name(url: &str, mid: u64) -> String {
        let mut pieces = url.split(".").collect::<Vec<&str>>();
        let ext = pieces.remove(pieces.len() - 1);
        format!("{}.{}", mid, ext)
    }

    pub fn get_img_bytes(url: &str) -> Result<Vec<u8>, NSFWError> {
        let mut resp = reqwest::blocking::get(url)?;
        let mut data = Vec::<u8>::new();
        resp.read_to_end(&mut data)?;
        Ok(data)
    }
}

use rand::Rng;
use serde::{Deserialize, Serialize};

use crate::common::error::NSFWError;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DanbooruEntry {
    pub id: u64,
    pub file_url: String,
}

impl DanbooruEntry {
    pub fn new(url: &str) -> Result<Vec<Self>, NSFWError> {
        let resp = reqwest::blocking::get(url)?;
        let body = resp.text()?;
        Self::from_body(body)
    }

    pub fn from_body(body: String) -> Result<Vec<Self>, NSFWError> {
        let items = serde_json::from_str::<Vec<DanbooruEntry>>(&body)?;
        Ok(items)
    }

    pub fn vec_to_body(items: Vec<Self>) -> Result<String, serde_json::Error> {
        serde_json::to_string(&items)
    }

    pub fn random_item(items: &mut Vec<Self>) -> Option<Self> {
        if items.is_empty() {
            None
        } else {
            let mut rng = rand::thread_rng();
            let index = rng.gen_range(0, items.len());
            Some(items.remove(index))
        }
    }
}

use std::borrow::Cow;

use log::debug;
use serenity::builder::CreateMessage;
use serenity::http::{AttachmentType, CacheHttp};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::common::danbooru::DanbooruEntry;
use crate::common::util::Utility;

const API_BASE: &str = "https://danbooru.donmai.us/posts.json?tags=";
const URL_BASE: &str = "https://danbooru.donmai.us/posts/";
const API_ICON: &str = "https://i.imgur.com/ytMyEyr.png";
const API_NAME: &str = "Danbooru";
const RESP_CLR: u64 = 0x15_2f_56;

const FAIL_ERR: &str = "Failed to communicate with Danbooru.";
const EMPTY_ERR: &str = "This lookup gave no results.";

#[derive(Default)]
pub struct DanbooruCommand;

impl DanbooruCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for DanbooruCommand {
    fn command_name(&self) -> &str {
        "danbooru"
    }

    fn category(&self) -> &str {
        "nsfw"
    }

    fn nsfw(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["danb"]
    }

    fn example(&self) -> &str {
        "knot"
    }

    fn description(&self) -> &str {
        "Searches Danbooru for the specified tag. \
         If no tag is given, the latest posts will be used. \
         Separate different tags with a space and replace spaces \
         within a single tag with underscores \"_\"."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("tags", true).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let key = Utility::cache_key(
            self.category(),
            self.command_name(),
            Utility::tag_key(pld.args.get("tags").split(" ").collect()),
        );
        let url = if pld.args.is_empty() {
            API_BASE.to_owned()
        } else {
            format!("{}&tags={}", API_BASE, pld.args.get("tags").replace(" ", "+"))
        };
        let entries = match pld.cache.get(key.clone()) {
            Some(body) => {
                if let Ok(items) = DanbooruEntry::from_body(body) {
                    if !items.is_empty() {
                        Ok(items)
                    } else {
                        DanbooruEntry::new(&url)
                    }
                } else {
                    DanbooruEntry::new(&url)
                }
            }
            None => DanbooruEntry::new(&url),
        };
        let mut response = match entries {
            Ok(mut dse) => {
                if !dse.is_empty() {
                    if let Some(dsi) = DanbooruEntry::random_item(&mut dse) {
                        if let Ok(serbody) = DanbooruEntry::vec_to_body(dse) {
                            pld.cache.set(key, serbody);
                        }
                        let post = format!("{}/{}", URL_BASE, dsi.id);
                        let mut r = CreateMessage::default();
                        let img_name = Utility::get_img_name(&dsi.file_url, pld.msg.id.0);
                        let downloaded = match Utility::get_img_bytes(&dsi.file_url) {
                            Ok(bytes) => {
                                if bytes.len() < 8_388_608 {
                                    r.add_file(AttachmentType::Bytes {
                                        data: Cow::from(bytes),
                                        filename: img_name.clone(),
                                    });
                                    true
                                } else {
                                    debug!("Image too large to upload to discord.");
                                    false
                                }
                            }
                            Err(why) => {
                                debug!("Image byte pulling failed: {}", why);
                                false
                            }
                        };
                        r.embed(|e| {
                            e.color(RESP_CLR);
                            e.author(|a| {
                                a.name(format!("{}: {}", API_NAME, dsi.id));
                                a.icon_url(API_ICON);
                                a.url(post);
                                a
                            });
                            if downloaded {
                                e.image(format!("attachment://{}", &img_name));
                            } else {
                                e.image(dsi.file_url);
                            }
                            e
                        });
                        r
                    } else {
                        UnicornEmbed::error(EMPTY_ERR)
                    }
                } else {
                    UnicornEmbed::error(EMPTY_ERR)
                }
            }
            Err(why) => {
                debug!("Failed getting Danbooru posts: {}", why);
                UnicornEmbed::error(FAIL_ERR)
            }
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

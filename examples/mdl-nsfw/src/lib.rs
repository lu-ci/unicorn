use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};
use unicorn_callable::module::UnicornModule;

use crate::boobs::BoobsCommand;
use crate::butts::ButtsCommand;
use crate::danbooru::DanbooruCommand;
use crate::e621::E621Command;
use crate::gelbooru::GelbooruCommand;
use crate::konachan::KonachanCommand;
use crate::rule34::Rule34Command;
use crate::xbooru::XbooruCommand;
use crate::yandere::YandereCommand;

mod common;

mod boobs;
mod butts;
mod danbooru;
mod e621;
mod gelbooru;
mod konachan;
mod rule34;
mod xbooru;
mod yandere;

#[derive(Default)]
pub struct NSFWModule;

impl UnicornModule for NSFWModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            BoobsCommand::boxed(),
            ButtsCommand::boxed(),
            DanbooruCommand::boxed(),
            E621Command::boxed(),
            KonachanCommand::boxed(),
            YandereCommand::boxed(),
            GelbooruCommand::boxed(),
            Rule34Command::boxed(),
            XbooruCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "NSFW"
    }
}

define_module!(NSFWModule, Default::default);

use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "🍰";
const INTERACTION_COLOR: u64 = 0xf9_f9_f9;

#[derive(Default)]
pub struct FeedCommand;

impl FeedCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for FeedCommand {
    fn command_name(&self) -> &str {
        "feed"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Care to share some of your food with someone cute?"
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld).await;
        let action = match target {
            Some(target) => format!("{} feeds {}.", actor.name, target.name),
            None => format!("{} eats.", actor.name),
        };
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action).await;
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};
use unicorn_callable::module::UnicornModule;

use crate::{
    action::{
        dance::DanceCommand, facepalm::FacepalmCommand, hide::HideCommand, highfive::HighFiveCommand, hug::HugCommand, kiss::KissCommand,
        lick::LickCommand, pat::PatCommand, poke::PokeCommand, shrug::ShrugCommand, sleep::SleepCommand, sniff::SniffCommand, wave::WaveCommand,
    },
    addinteraction::AddInteractionCommand,
    aggression::{
        bite::BiteCommand, explode::ExplodeCommand, punch::PunchCommand, shoot::ShootCommand, slap::SlapCommand, spank::SpankCommand,
        stab::StabCommand, stare::StareCommand, tackle::TackleCommand,
    },
    consumption::{drink::DrinkCommand, feed::FeedCommand, sip::SipCommand},
    emotion::{blush::BlushCommand, cry::CryCommand, laugh::LaughCommand, pout::PoutCommand},
    nsfw::fuck::FuckCommand,
};

mod addinteraction;
mod common;
mod entry;

mod action;
mod aggression;
mod consumption;
mod emotion;
mod nsfw;

#[derive(Default)]
pub struct InteractionsModule;

impl UnicornModule for InteractionsModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            // Controls
            AddInteractionCommand::boxed(),
            // Action
            DanceCommand::boxed(),
            FacepalmCommand::boxed(),
            HideCommand::boxed(),
            HighFiveCommand::boxed(),
            HugCommand::boxed(),
            KissCommand::boxed(),
            LickCommand::boxed(),
            PatCommand::boxed(),
            PokeCommand::boxed(),
            ShrugCommand::boxed(),
            SleepCommand::boxed(),
            SniffCommand::boxed(),
            WaveCommand::boxed(),
            // Aggression
            BiteCommand::boxed(),
            ExplodeCommand::boxed(),
            PunchCommand::boxed(),
            ShootCommand::boxed(),
            SlapCommand::boxed(),
            SpankCommand::boxed(),
            StabCommand::boxed(),
            StareCommand::boxed(),
            TackleCommand::boxed(),
            // Consumption
            DrinkCommand::boxed(),
            FeedCommand::boxed(),
            SipCommand::boxed(),
            // Emotion
            BlushCommand::boxed(),
            CryCommand::boxed(),
            LaughCommand::boxed(),
            PoutCommand::boxed(),
            // NSFW
            FuckCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Interactions"
    }
}

define_module!(InteractionsModule, Default::default);

use rand::Rng;
use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "👃";
const INTERACTION_COLOR: u64 = 0xff_cc_4d;
const INTERACTION_TARGETS: [&str; 4] = ["themself", "something off the table", "panties", "glue"];

#[derive(Default)]
pub struct SniffCommand;

impl SniffCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for SniffCommand {
    fn command_name(&self) -> &str {
        "sniff"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["smell"]
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Smells nice? Sniff it!"
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld).await;
        let action = format!(
            "{} sniffs {}.",
            actor.name,
            match target {
                Some(target) => target.name,
                None => {
                    let mut rng = rand::thread_rng();
                    INTERACTION_TARGETS[rng.gen_range(0, INTERACTION_TARGETS.len())].to_owned()
                }
            }
        );
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action).await;
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

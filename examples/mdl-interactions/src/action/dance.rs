use rand::Rng;
use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICONS: [&str; 2] = ["💃", "🕺"];
const INTERACTION_COLOR: u64 = 0xdd_2e_44;

#[derive(Default)]
pub struct DanceCommand;

impl DanceCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for DanceCommand {
    fn command_name(&self) -> &str {
        "dance"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Feel alive? Like you just wanna... boogie? Let's dance!"
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld).await;
        let action = match target {
            Some(target) => format!("{} dances with {}.", actor.name, target.name),
            None => format!("{} dances like nobody's watching!", actor.name),
        };
        let icon = INTERACTION_ICONS[rand::thread_rng().gen_range(0, INTERACTION_ICONS.len())];
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, icon, action).await;
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

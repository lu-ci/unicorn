use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "💥";
const INTERACTION_COLOR: u64 = 0xcf_47_37;

#[derive(Default)]
pub struct ExplodeCommand;

impl ExplodeCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ExplodeCommand {
    fn command_name(&self) -> &str {
        "explode"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["detonate"]
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "When all other means fail, it's time to bring out the carpet bomb squadrons."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld).await;
        let action = match target {
            Some(target) => format!("{} blows up {}.", actor.name, target.name),
            None => format!("{} explodes.", actor.name),
        };
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action).await;
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

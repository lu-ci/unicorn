use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "🖐";
const INTERACTION_COLOR: u64 = 0xff_cc_4d;

#[derive(Default)]
pub struct SlapCommand;

impl SlapCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for SlapCommand {
    fn command_name(&self) -> &str {
        "slap"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["smack"]
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Punching is too barbaric, a slap should be elegant enough."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld).await;
        let action = match target {
            Some(target) => format!("{} slaps {}.", actor.name, target.name),
            None => format!("{} slaps themself.", actor.name),
        };
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action).await;
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

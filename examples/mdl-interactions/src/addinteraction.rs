use std::io::Read;

use bson::*;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::hash::UnicornHasher;

use crate::entry::InteractionEntry;

#[derive(Default)]
pub struct AddInteractionCommand;

impl AddInteractionCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn validate_url(url: &str) -> (bool, Vec<u8>) {
        let mut success = false;
        let mut data = Vec::<u8>::new();
        if let Ok(mut resp) = reqwest::blocking::get(url) {
            if resp.status().is_success() {
                if let Err(_) = resp.read_to_end(&mut data) {
                    success = false;
                    data = Vec::<u8>::new();
                } else {
                    success = true;
                }
            }
        }
        (success, data)
    }

    fn get_interactions(&self, pld: &CommandPayload) -> Vec<String> {
        let mut names = Vec::<String>::new();
        if let Some(intmod) = pld.meta.get(self.category()) {
            for icmd in intmod.commands.clone() {
                if &icmd.name != self.command_name() {
                    names.push(icmd.name);
                }
            }
        }
        names
    }
}

#[serenity::async_trait]
impl UnicornCommand for AddInteractionCommand {
    fn command_name(&self) -> &str {
        "addinteraction"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["addreact", "addinteract"]
    }

    fn example(&self) -> &str {
        "interaction my.gif.link/fancy.gif"
    }

    fn description(&self) -> &str {
        "Adds new GIF to the specified interaction type. Accepted types are \
         pout, highfive, hug, kiss, pat, shoot, bite, slap, lick, feed, stare, sip, fuck, \
         spank, drink, stab, poke, punch, shrug, facepalm, cry, wave, sleep, laugh, blush, \
         dance, explode, sniff and tackle."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("interaction", false), CommandArgument::new("url", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let interaction = pld.args.get("interaction");
        let url = pld.args.get("url");
        let interactions = self.get_interactions(pld);
        let mut response = if interactions.contains(&interaction.to_owned()) {
            let (valid, data) = Self::validate_url(&url);
            if valid && !data.is_empty() {
                let hashed = UnicornHasher::calculate_md5_hash(data);
                let lookup = doc! {"hash": &hashed};
                let exists = if let Some(cli) = pld.db.get_client() {
                    if let Ok(counter) = cli
                        .database(&pld.db.cfg.database.name)
                        .collection("interactions")
                        .count_documents(lookup, None)
                    {
                        counter > 0
                    } else {
                        false
                    }
                } else {
                    false
                };
                if !exists {
                    let imgur_url = if url.contains("i.imgur.com") { Some(url) } else { None };
                    if let Some(img_url) = imgur_url {
                        let _entry = InteractionEntry::new(pld, interaction, img_url, hashed);
                        UnicornEmbed::info("Function unfinished, this did nothing.")
                    } else {
                        UnicornEmbed::error("Failed relaying the image to Imgur.")
                    }
                } else {
                    UnicornEmbed::error("This interaction has already been submited.")
                }
            } else {
                UnicornEmbed::error("Failed to validate the given link.")
            }
        } else {
            UnicornEmbed::error("Unrecognized interaction.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

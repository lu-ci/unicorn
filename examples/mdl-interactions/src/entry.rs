use bson::Bson;
use serde::{Deserialize, Serialize};
use serenity::builder::CreateMessage;
use serenity::client::Context;

use unicorn_callable::payload::CommandPayload;
use unicorn_utility::token::UnicornToken;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct InteractionEntry {
    pub name: String,
    pub user_id: i64,
    pub user_name: Option<String>,
    pub server_id: i64,
    pub server_name: Option<String>,
    pub url: String,
    pub hash: String,
    pub interaction_id: String,
    pub message_id: Option<i64>,
    pub reported: bool,
    pub active: bool,
}

impl InteractionEntry {
    pub async fn new(pld: &CommandPayload, name: impl ToString, url: impl ToString, hash: impl ToString) -> Self {
        Self {
            name: name.to_string(),
            user_id: pld.msg.author.id.0 as i64,
            user_name: Some(pld.msg.author.name.clone()),
            server_id: if let Some(gid) = pld.msg.guild_id { gid.0 as i64 } else { 0i64 },
            server_name: Some(if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
                guild.name
            } else {
                "<No Guild>".to_owned()
            }),
            url: url.to_string(),
            hash: hash.to_string(),
            interaction_id: UnicornToken::hex(8),
            message_id: None,
            reported: false,
            active: false,
        }
    }

    pub fn vector_json(items: Vec<Self>) -> Result<String, serde_json::Error> {
        serde_json::to_string(&items)
    }

    pub fn _to_bson(&self) -> Result<Bson, bson::EncoderError> {
        bson::to_bson(self)
    }

    pub fn placeholder() -> InteractionEntry {
        Self {
            name: "void".to_owned(),
            user_id: 0,
            user_name: None,
            server_id: 0,
            server_name: None,
            url: "https://i.imgur.com/m59E4nx.gif".to_owned(),
            hash: "hashless".to_owned(),
            interaction_id: "None".to_owned(),
            message_id: None,
            reported: true,
            active: true,
        }
    }

    async fn footer(&self, ctx: &Context) -> String {
        let user_name = match ctx.cache.user(self.user_id as u64).await {
            Some(user) => user.name,
            None => self.user_name.clone().unwrap_or("Unknown User".to_owned()),
        };

        let server_name = match ctx.cache.guild(self.server_id as u64).await {
            Some(guild) => guild.name,
            None => self.server_name.clone().unwrap_or("Unknown Server".to_owned()),
        };

        format!("[{}] | Submitted by {} from {}.", &self.interaction_id, user_name, server_name)
    }

    pub async fn embed<'a>(&self, ctx: &Context, color: u64, icon: impl ToString, action: impl ToString) -> CreateMessage<'a> {
        let footer = self.footer(ctx).await;
        let mut m = CreateMessage::default();
        m.embed(|e| {
            if self.name == "void" {
                e.description("It seems this interaction has no gifs yet...");
            }
            e.color(color);
            e.image(&self.url);
            e.title(format!("{} {}", icon.to_string(), action.to_string()));
            e.footer(|f| {
                f.text(&footer);
                f
            });
            e
        });
        m
    }
}

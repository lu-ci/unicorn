use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "🍆";
const INTERACTION_COLOR: u64 = 0x74_4e_aa;

#[derive(Default)]
pub struct FuckCommand;

impl FuckCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for FuckCommand {
    fn command_name(&self) -> &str {
        "fuck"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn nsfw(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["fucc", "succ"]
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Don't question my modules... Yes (º﹃º)"
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one();
        let (actor, target) = InteractionCore::get_users(pld).await;
        let action = match target {
            Some(target) => format!("{} fucks {}.", actor.name, target.name),
            None => format!("{} fucks.", actor.name),
        };
        let mut response = interaction.embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action).await;
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

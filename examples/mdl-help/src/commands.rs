use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::paginator::Paginator;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::perms::common::{PermissionParams, UnicornPermissions};
use unicorn_embed::UnicornEmbed;
use unicorn_information::{CommandInformation, ModuleInformation};

#[derive(Default)]
pub struct CommandsCommand;

impl CommandsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    async fn check_permission(&self, pld: &CommandPayload, command: &CommandInformation) -> bool {
        let params = PermissionParams::new(
            command.name.to_owned(),
            command.category.to_owned(),
            command.dm,
            command.nsfw,
            command.owner,
            pld.clone(),
        );
        let mut perms = UnicornPermissions::new(params);
        perms.check().await;
        perms.ok()
    }

    fn count_commands(&self, pld: CommandPayload) -> u64 {
        let mut count = 0u64;
        for (_, info) in &pld.meta {
            count += info.commands.len() as u64;
        }
        count
    }

    fn module_information<'a>(&self, pld: &mut CommandPayload) -> CreateMessage<'a> {
        let info = &pld.meta;
        let module_count = info.len();
        let mut module_list = Vec::new();
        for m in info.values() {
            module_list.push(m.name.to_owned());
        }
        module_list.sort();
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1B6F5F);
            e.field("Modules", format!("There are **{}** modules.", module_count), false);
            e.field(
                "Module List",
                format!(
                    "```yml\n- {}\n```\n{}",
                    module_list.join("\n- "),
                    format!("There are **{}** commands in total.", self.count_commands(pld.clone()))
                ),
                false,
            );
            e.footer(|f| {
                f.text(format!(
                    "Type {}{} [module] to see commands in that module.",
                    pld.cfg.preferences.prefix,
                    self.command_name()
                ));
                f
            })
        });
        msg
    }

    fn best_match(pld: &CommandPayload, lookup: String) -> Option<ModuleInformation> {
        if lookup.len() >= 3 {
            if let Some(info) = pld.meta.get(&lookup) {
                Some(info.clone())
            } else {
                let mut best = None;
                for (_, meta) in &pld.meta {
                    if meta.name.to_lowercase() == lookup.to_lowercase() {
                        best = Some(meta.clone());
                        break;
                    }
                }
                if best.is_none() {
                    for (_, meta) in &pld.meta {
                        if meta.name.to_lowercase().contains(&lookup.to_lowercase()) {
                            best = Some(meta.clone());
                            break;
                        }
                    }
                }
                best
            }
        } else {
            None
        }
    }

    async fn command_information<'a>(&self, pld: &mut CommandPayload) -> CreateMessage<'a> {
        let lookup = pld.args.get("command");
        let response = match Self::best_match(pld, lookup.to_owned()) {
            Some(info) => {
                let commands = &info.commands.clone();
                let mut command_list = Vec::new();
                for c in commands {
                    let mut command_name = c.name.clone();
                    if !c.aliases.is_empty() {
                        command_name.push_str(format!(" [{}]", c.aliases.join(", ")).as_str());
                    }
                    if !self.check_permission(&pld, c).await {
                        command_name = format!("⛔ {}", command_name);
                    }
                    command_list.push(command_name);
                }
                command_list.sort();
                let total_commands = &command_list.len();
                let paginator = Paginator::new(pld, command_list);
                let page = paginator.get_page();
                let command_count = page.len();
                let mut msg = CreateMessage::default();
                msg.embed(|e| {
                    e.color(0x1B6F5F);
                    e.field(
                        format!("{} Commands", info.name.to_uppercase()),
                        format!(
                            "Showing {} out of {} commands in the {} module.",
                            command_count, total_commands, &info.name
                        ),
                        false,
                    );
                    e.field(
                        if paginator.page_count == 1 {
                            "Commands List".to_owned()
                        } else {
                            format!("Commands List | Page {} of {}", paginator.page, paginator.page_count)
                        },
                        format!("```yml\n- {}\n```", page.join("\n- ")),
                        false,
                    );
                    e.footer(|f| {
                        f.text(format!(
                            "Type {}help [command] to see details of that command.",
                            pld.cfg.preferences.prefix
                        ));
                        f
                    })
                });
                msg
            }
            None => UnicornEmbed::not_found(format!("Module {} not found.", lookup)),
        };
        response
    }
}

#[serenity::async_trait]
impl UnicornCommand for CommandsCommand {
    fn command_name(&self) -> &str {
        "commands"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["modules", "cmds", "mdls"]
    }

    fn example(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Lists all of Sigma's available modules. To view the commands within a specific module, add the module's name as an argument."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        [vec![CommandArgument::new("command", true).allows_spaces()], Paginator::pagination_args()].concat()
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.is_empty() {
            self.module_information(pld)
        } else {
            self.command_information(pld).await
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

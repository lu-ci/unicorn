use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::user::User;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_information::CommandInformation;
use unicorn_utility::user::Avatar;

#[derive(Default)]
pub struct HelpCommand;

impl HelpCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn print_command<'a>(&self, pld: &CommandPayload, command: &CommandInformation) -> CreateMessage<'a> {
        let mut msg = CreateMessage::default();

        msg.embed(|e| {
            e.title(format!(
                "📄 [{}] {} Usage and Information",
                command.category.to_uppercase(),
                command.name.to_uppercase()
            ));
            e.color(0x1B6F5F);
            e.field(
                "Usage Example",
                format!("`{}{} {}`", pld.get_prefix(), command.name, command.example),
                false,
            );
            e.field("Command Description", format!("```\n{}\n```", command.description), false);
            if !command.aliases.is_empty() {
                e.field("Command Aliases", format!("```\n{}\n```", command.aliases.join(", ")), true);
            }
            e
        });
        msg
    }

    fn command_help<'a>(&self, pld: &mut CommandPayload) -> CreateMessage<'a> {
        let lookup = pld.args.get("command");
        for module in pld.meta.values() {
            for command in &module.commands {
                if lookup == &command.name {
                    return self.print_command(&pld, command);
                } else {
                    for alias in &command.aliases {
                        if lookup == alias {
                            return self.print_command(&pld, command);
                        }
                    }
                }
            }
        }
        UnicornEmbed::not_found(format!("Command {} not found", lookup))
    }

    async fn general_help<'a>(&self, pld: &mut CommandPayload) -> CreateMessage<'a> {
        let bot = pld.ctx.cache.current_user().await;
        let sigma_title = "Apex Sigma: The Database Giant";
        let patreon_url = "https://www.patreon.com/ApexSigma";
        let paypal_url = "https://www.paypal.me/AleksaRadovic";
        let support_url = "https://discordapp.com/invite/aEUCHwX".to_owned();
        let invite_url = format!("https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8", bot.id);
        let mut support_text = format!("**Add Me**: [Link]({})", invite_url).to_owned();
        support_text = format!("{} | **Commands**: [Link]({}/commands)", support_text, pld.cfg.preferences.website);
        support_text = format!("{} | **Support**: [Link]({})", support_text, &support_url);
        support_text = format!(
            "{}\nWanna help? **Patreon**: [Link]({}) | **PayPal**: [Link]({})",
            support_text, &patreon_url, &paypal_url
        );

        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1B6F5F);
            e.author(|a| {
                a.name(sigma_title);
                a.icon_url(Avatar::url(User::from(bot.clone())));
                a
            });
            e.thumbnail(Avatar::url(User::from(bot.clone())));
            e.field(format!("Help"), support_text, false);
            e.footer(|f| {
                f.text("© by Lucia\'s Cipher. Released under the GPLv3 license.");
                f.icon_url("https://i.imgur.com/KVgdtNg.png");
                f
            })
        });
        msg
    }
}

#[serenity::async_trait]
impl UnicornCommand for HelpCommand {
    fn command_name(&self) -> &str {
        "help"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["h"]
    }

    fn example(&self) -> &str {
        "fish"
    }

    fn description(&self) -> &str {
        "Lists all of Sigma's available modules. To view the commands within a specific module, \
         add the module's name as an argument."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("command", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.is_empty() {
            self.general_help(pld).await
        } else {
            self.command_help(pld)
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

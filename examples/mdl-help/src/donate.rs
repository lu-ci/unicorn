use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct DonateCommand;

impl DonateCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn donate_information<'a>(&self) -> CreateMessage<'a> {
        let donation_url = "https://www.patreon.com/ApexSigma";
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1B6F5F);
            e.title("Sigma Donation Information");
            e.description(format!("Care to help out? Come [support]({}) Sigma!", donation_url))
        });
        msg
    }
}

#[serenity::async_trait]
impl UnicornCommand for DonateCommand {
    fn command_name(&self) -> &str {
        "donate"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn description(&self) -> &str {
        "Shows donation information for Sigma."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = self.donate_information();
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

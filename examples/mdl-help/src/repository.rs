use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct RepositoryCommand;

impl RepositoryCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn text_invite<'a>(&self, url: String) -> CreateMessage<'a> {
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1B6F5F);
            e.description(format!("Apex Sigma: The Database Giant: <{}>", url))
        });
        msg
    }

    fn link_invite<'a>(&self, url: String) -> CreateMessage<'a> {
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1B6F5F);
            e.author(|a| {
                a.name("Apex Sigma: The Database Giant");
                a.icon_url("https://framablog.org/wp-content/uploads/2016/01/gitlab.png");
                a.url(url);
                a
            })
        });
        msg
    }
}

#[serenity::async_trait]
impl UnicornCommand for RepositoryCommand {
    fn command_name(&self) -> &str {
        "repository"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["repo", "project"]
    }

    fn description(&self) -> &str {
        "Shows the link to the project's repository page. \
         You can add \"text\" to the command to show just the link as pure text."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("text", true).is_flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let invite_url = "https://gitlab.com/lu-ci/sigma/apex-sigma".to_string();
        let mut response = if pld.args.has_argument("text") {
            self.text_invite(invite_url)
        } else {
            self.link_invite(invite_url)
        };

        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

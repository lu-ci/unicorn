use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};
use unicorn_callable::module::UnicornModule;

use crate::commands::CommandsCommand;
use crate::donate::DonateCommand;
use crate::help::HelpCommand;
use crate::invite::InviteCommand;
use crate::repository::RepositoryCommand;

mod commands;
mod donate;
mod help;
mod invite;
mod repository;

#[derive(Default)]
pub struct HelpModule;

impl UnicornModule for HelpModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            CommandsCommand::boxed(),
            HelpCommand::boxed(),
            DonateCommand::boxed(),
            InviteCommand::boxed(),
            RepositoryCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Help"
    }
}

define_module!(HelpModule, Default::default);

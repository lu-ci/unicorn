use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::user::CurrentUser;
use serenity::model::user::User;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::image::ImageProcessing;
use unicorn_utility::user::Avatar;

#[derive(Default)]
pub struct InviteCommand;

impl InviteCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    async fn text_invite<'a>(&self, pld: &CommandPayload, url: String) -> Result<(), CallableError> {
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1B6F5F);
            e.description(format!("Click the following link to invite me: <{}>", url))
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut msg).await?;
        Ok(())
    }

    async fn link_invite<'a>(&self, pld: &CommandPayload, bot: CurrentUser, url: String) -> Result<(), CallableError> {
        let mut msg = CreateMessage::default();
        let avatar_url = Avatar::url(User::from(bot.clone()));
        let color = ImageProcessing::from_url(avatar_url.clone());
        msg.embed(|e| {
            e.color(color);
            e.author(|f| {
                f.name(format!("{}: Project Unicorn", bot.name));
                f.icon_url(avatar_url.clone());
                f.url(url);
                f
            });
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut msg).await?;
        Ok(())
    }
}

#[serenity::async_trait]
impl UnicornCommand for InviteCommand {
    fn command_name(&self) -> &str {
        "invite"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["inv"]
    }

    fn description(&self) -> &str {
        "Provides Sigma's invitation link to add her to your server. \
         If you can't click/tap embed title URLs, add the word \"text\" \
         to the command to get the invite in a plain text format."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("text", true).is_flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let bot = pld.ctx.cache.current_user().await;
        let invite_url = format!("https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8", bot.id);
        if pld.args.has_argument("text") {
            self.text_invite(pld, invite_url).await?;
        } else {
            self.link_invite(pld, bot, invite_url).await?;
        };
        Ok(())
    }
}

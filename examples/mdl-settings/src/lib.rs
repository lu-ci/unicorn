use crate::preferences::prefix::PrefixCommand;
use crate::preferences::unflip::UnflipCommand;
use unicorn_callable::module::UnicornModule;
use unicorn_callable::{define_module, UnicornCommand, UnicornEvent};

pub mod preferences;

#[derive(Default)]
pub struct SettingsModule;

impl UnicornModule for SettingsModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![PrefixCommand::boxed(), UnflipCommand::boxed()]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Settings"
    }
}

define_module!(SettingsModule, Default::default);

use serenity::model::Permissions;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct UnflipCommand;

impl UnflipCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for UnflipCommand {
    fn command_name(&self) -> &str {
        "unflip"
    }

    fn category(&self) -> &str {
        "settings"
    }

    fn description(&self) -> &str {
        "Toggles if any flipped tables should be unflipped (and sometimes thrown)."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(gld) = pld.msg.guild(&pld.ctx.cache).await {
            let perms: Permissions = gld.member_permissions(&pld.msg.author.id).await;
            perms.manage_guild()
        } else {
            false
        }
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if let Some(mut settings) = pld.settings.clone() {
            let active = if let Some(unf) = settings.unflip { unf } else { false };
            settings.unflip = Some(!active);
            settings.save(&pld.db);
            let state = if active { "disabled" } else { "enabled" };
            UnicornEmbed::ok(format!("Table unflipping has been {}", state))
        } else {
            UnicornEmbed::error("Failed getting the guild's settings!")
        };
        pld.msg.channel_id.send_message(&pld.ctx.http, |_| &mut response).await?;
        Ok(())
    }
}

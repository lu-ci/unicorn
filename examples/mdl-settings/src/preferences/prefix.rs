use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct PrefixCommand;

impl PrefixCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for PrefixCommand {
    fn command_name(&self) -> &str {
        "prefix"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["pfx"]
    }

    fn description(&self) -> &str {
        "Sets the prefix that Sigma should respond to.
        This will be bound to your server and you can set it to anything you'd like.
        However, the prefix must can not contain spaces, they will be automatically removed.
        This command requires the Manage Server permission."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(gld) = pld.msg.guild(&pld.ctx.cache).await {
            let perms = gld.member_permissions(pld.msg.author.id).await;
            perms.manage_guild()
        } else {
            false
        }
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("prefix", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("prefix") {
            if let Some(mut settings) = pld.settings.clone() {
                let pfx = pld.args.get("prefix").to_lowercase().replace("\n", "");
                if !pfx.is_empty() {
                    if pfx != pld.get_prefix() {
                        settings.prefix = if pfx == pld.cfg.preferences.prefix { None } else { Some(pfx) };
                        settings.save(&pld.db);
                        UnicornEmbed::ok(format!("New prefix set."))
                    } else {
                        UnicornEmbed::warn("That is already the active prefix.")
                    }
                } else {
                    UnicornEmbed::error(format!("That prefix seems to be empty... somehow."))
                }
            } else {
                UnicornEmbed::error("Failed getting the guild's settings!")
            }
        } else {
            UnicornEmbed::info(format!("`{}` is the current prefix.", pld.get_prefix()))
        };
        pld.msg.channel_id.send_message(&pld.ctx.http, |_| &mut response).await?;
        Ok(())
    }
}

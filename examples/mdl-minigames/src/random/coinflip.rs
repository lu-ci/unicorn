use rand::Rng;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

pub struct CoinFlipCommand;

impl CoinFlipCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for CoinFlipCommand {
    fn command_name(&self) -> &str {
        "coinflip"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["cf", "tosscoin"]
    }

    fn example(&self) -> &str {
        "Heads"
    }

    fn description(&self) -> &str {
        "Flips a coin. Nothing complex. You can try guessing the results by typing either Heads or Tails."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("face", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let choice = pld.args.get("face");

        let result = if rand::thread_rng().gen_bool(0.5) {
            ("heads", "https://i.imgur.com/qLPkn7k.png")
        } else {
            ("tails", "https://i.imgur.com/Xx5dY4M.png")
        };
        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(0x1B6F5F);
            if pld.args.has_argument("face") {
                e.title(if choice.to_lowercase() == result.0 {
                    "☑ Nice guess!"
                } else {
                    "🇽 Better luck next time!"
                });
            }
            e.image(result.1);
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

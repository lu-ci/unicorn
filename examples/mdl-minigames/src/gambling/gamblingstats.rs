use bson::*;
use inflector::cases::titlecase::to_title_case;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use std::collections::HashMap;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_database::DatabaseHandler;
use unicorn_resource::UnicornResource;

struct Account {
    pub name: String,
    pub gain: i64,
    pub loss: i64,
}

impl Account {
    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            gain: 0,
            loss: 0,
        }
    }

    pub fn net(&self) -> i64 {
        self.gain - self.loss
    }
}

pub struct GamblingStatsCommand;

impl GamblingStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
    fn target_text(text: &str, total: i64, global: bool, target: &str) -> String {
        if total > 0 {
            if global {
                to_title_case(text)
            } else {
                format!("{} for {}", to_title_case(text), target)
            }
        } else {
            format!("No {} yet...", text)
        }
    }
    fn gains_and_losses(db: &DatabaseHandler, target: Option<u64>, names: Vec<&str>) -> Vec<Account> {
        let mut result = Vec::new();
        match target {
            Some(id) => {
                let resource = db.get_resource("currency", id);
                for name in &names {
                    let mut account = Account::new(name);
                    if let Some(gain) = resource.origins.functions.get::<str>(name) {
                        account.gain = *gain;
                    }
                    if let Some(loss) = resource.expenses.functions.get::<str>(name) {
                        account.loss = *loss;
                    }
                    if account.gain > 0 || account.loss > 0 {
                        result.push(account);
                    }
                }
            }
            None => {
                let mut queries = Vec::new();
                let mut accounts = HashMap::new();
                for name in &names {
                    queries.push(doc! {format!("expenses.functions.{}", name): {"gt": 0}});
                    queries.push(doc! {format!("origins.functions.{}", name): {"gt": 0}});
                    accounts.insert(name, Account::new(name));
                }

                if let Some(cli) = db.get_client() {
                    if let Ok(cursor) = cli
                        .database(&db.cfg.database.name)
                        .collection("currency_resource")
                        .find(doc! {"$or": queries}, None)
                    {
                        for entry in cursor {
                            if let Ok(doc) = entry {
                                if let Ok(item) = from_bson::<UnicornResource>(bson::Bson::Document(doc)) {
                                    for name in &names {
                                        if let Some(account) = accounts.get_mut(name) {
                                            if let Some(gain) = item.origins.functions.get::<str>(name) {
                                                account.gain = *gain;
                                            }
                                            if let Some(loss) = item.expenses.functions.get::<str>(name) {
                                                account.loss = *loss;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (_, account) in accounts.drain() {
                    result.push(account);
                }
            }
        }
        result
    }
}

#[serenity::async_trait]
impl UnicornCommand for GamblingStatsCommand {
    fn command_name(&self) -> &str {
        "gamblingstats"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["gambstats"]
    }

    fn example(&self) -> &str {
        "@user"
    }

    fn description(&self) -> &str {
        "Shows total gains and losses from gambling minigames for the specified user. \
         You can view the global gains and losses by adding \"global\" as an argument."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("global", true).is_flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = match pld.msg.mentions.get(0) {
            Some(user) => user,
            None => &pld.msg.author,
        };

        let is_author = target.id == pld.msg.author.id;
        let global = pld.args.has_argument("global");
        let accounts = if global {
            Self::gains_and_losses(&pld.db, None, vec!["slots", "roulette"])
        } else {
            Self::gains_and_losses(&pld.db, Some(u64::from(target.id)), vec!["slots", "roulette"])
        };

        let mut response = if accounts.len() > 0 {
            let mut msg = CreateMessage::default();

            let mut gains = String::new();
            let mut losses = String::new();
            let mut total_gains = 0;
            let mut total_losses = 0;
            for account in &accounts {
                if account.gain > 0 {
                    total_gains += account.gain;
                    gains += &format!("{}: **{}**\n", to_title_case(&account.name), account.gain);
                }
                if account.loss > 0 {
                    total_losses += account.loss;
                    losses += &format!("{}: **{}**\n", to_title_case(&account.name), account.loss);
                }
            }

            let gain_target = Self::target_text("gains", total_gains, global, &target.name);
            let loss_target = Self::target_text("losses", total_losses, global, &target.name);
            gains += &format!("Total: **{}**", total_gains);
            losses += &format!("Total: **{}**", total_losses);

            let mut net = String::new();
            let mut total_net = 0;
            for account in &accounts {
                net += &format!("{}: **{}**\n", to_title_case(&account.name), account.net());
                total_net += account.net();
            }
            net += &format!("Total: **{}**\n", total_net);

            msg.embed(|e| {
                e.color(0xbe1931);
                e.title("🎰 Gambling Gains and Losses");
                e.field(gain_target, gains, true);
                e.field(loss_target, losses, true);
                e.field("Net Changes", net, true);
                e
            });
            msg
        } else {
            let starter = if global {
                "No".to_owned()
            } else {
                if is_author {
                    "You have no".to_owned()
                } else {
                    format!("{} has no", target.name)
                }
            };
            unicorn_embed::UnicornEmbed::small_embed("💸", 0xc6e4b5, format!("{} gains or losses...", starter))
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

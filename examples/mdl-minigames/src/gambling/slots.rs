use rand::Rng;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_resource::origin::ResourceOrigin;
use unicorn_utility::user::Avatar;

pub struct SlotItem {
    pub symbol: String,
    pub value: f64,
}

impl SlotItem {
    pub fn new(symbol: &str, value: f64) -> Self {
        Self {
            symbol: symbol.to_string(),
            value,
        }
    }
}

#[derive(Default)]
pub struct SlotsCommand {
    symbols: Vec<SlotItem>,
}

impl SlotsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(SlotsCommand::new())
    }
    pub fn new() -> Self {
        Self { symbols: Self::symbols() }
    }

    pub fn symbols() -> Vec<SlotItem> {
        vec![
            SlotItem::new("🍆", 50.0),
            SlotItem::new("🍒", 55.0),
            SlotItem::new("⚓", 60.0),
            SlotItem::new("🏵", 70.0),
            SlotItem::new("💖", 75.0),
            SlotItem::new("🏮", 80.0),
            SlotItem::new("🍥", 85.0),
            SlotItem::new("💵", 90.0),
            SlotItem::new("💳", 95.0),
            SlotItem::new("🎁", 100.0),
            SlotItem::new("🐬", 105.0),
            SlotItem::new("🐦", 110.0),
            SlotItem::new("🌟", 115.0),
            SlotItem::new("🦊", 120.0),
            SlotItem::new("🦋", 125.0),
            SlotItem::new("🐍", 130.0),
            SlotItem::new("🍬", 135.0),
            SlotItem::new("💎", 140.0),
            SlotItem::new("🔰", 145.0),
            SlotItem::new("⚜", 150.0),
        ]
    }

    fn set_cooldown(&self, pld: &mut CommandPayload, key: String) {
        let profile = pld.db.get_profile(u64::from(pld.msg.author.id));
        let base_cooldown = 60 as f32;
        let casino = profile.get_upgrade("casino") as f32;
        let mut cooldown = (base_cooldown - ((base_cooldown / 100.0) * ((casino * 0.5) / (1.25 + (0.01 * casino))))) as i32;
        if cooldown < 5 {
            cooldown = 5;
        }
        pld.cd.set_cooldown(key, u64::from(pld.msg.author.id), cooldown);
    }

    fn generate_slots(&self) -> Vec<Vec<&SlotItem>> {
        let mut out_list = Vec::new();
        let mut rng = rand::thread_rng();
        let symbol_max = self.symbols.len() as u64;
        for _x in 0..3 {
            let mut temp_list = Vec::new();
            for _y in 0..3 {
                temp_list.push(&self.symbols[rng.gen_range(0u64, symbol_max) as usize]);
            }
            out_list.push(temp_list);
        }
        out_list
    }

    fn slots<'a>(&self, pld: &mut CommandPayload) -> CreateMessage<'a> {
        let currency = pld.cfg.preferences.currency.common.clone();
        let cash = &pld.db.get_resource("currency", u64::from(pld.msg.author.id));
        let bet = pld.args.get_or_default("bet", 10);
        let response = if cash.current >= bet {
            let key = format!("{}:{}", self.command_name(), pld.msg.author.id);

            let cdi = pld.cd.get_cooldown(key.clone());
            if cdi.active {
                return UnicornEmbed::small_embed("🕙", 0x696969, format!("You can spin again in {} seconds.", cdi.duration));
            }

            self.set_cooldown(pld, key);

            pld.db.del_resource(
                "currency",
                u64::from(pld.msg.author.id),
                bet,
                self.command_name(),
                ResourceOrigin::from(pld.msg.author.id),
                true,
            );

            let out_list = self.generate_slots();

            let slot_lines = format!(
                "{}{}{}",
                format!("⏸{}⏸\n", out_list[0].iter().map(|s| s.symbol.clone()).collect::<String>()),
                format!("▶{}◀\n", out_list[1].iter().map(|s| s.symbol.clone()).collect::<String>()),
                format!("⏸{}⏸", out_list[2].iter().map(|s| s.symbol.clone()).collect::<String>())
            );

            let combination = &out_list[1];
            let three_comb = combination[0].symbol == combination[1].symbol && combination[0].symbol == combination[2].symbol;
            let two_comb_one = combination[0].symbol == combination[1].symbol;
            let two_comb_two = combination[0].symbol == combination[2].symbol;
            let two_comb_three = combination[1].symbol == combination[2].symbol;

            let mut win = true;
            let winnings = if three_comb {
                (bet as f64 * (combination[0].value / 6.66666) * 0.95) as i64
            } else if two_comb_one || two_comb_two || two_comb_three {
                let multiplier = if two_comb_one {
                    combination[0].value
                } else if two_comb_two {
                    combination[0].value
                } else if two_comb_three {
                    combination[1].value
                } else {
                    0.0
                };
                (bet as f64 * (multiplier / 6.66666) * 0.45) as i64
            } else {
                win = false;
                0
            };

            let mut colour = 0x232323;
            let mut title = "💣 Oh my, you lost...";
            let mut footer = format!("{} {} {} has been deducted.", currency.icon.clone(), bet, currency.name.single);
            if win {
                pld.db.add_resource(
                    "currency",
                    u64::from(pld.msg.author.id),
                    winnings,
                    self.command_name(),
                    ResourceOrigin::from(pld.msg.author.id),
                    true,
                );
                colour = 0x5dadec;
                title = "💎 Congrats, you won!";
                footer = format!("{} {} {} has been awarded.", currency.icon.clone(), winnings, currency.name.single);
            }
            let mut msg = CreateMessage::default();
            msg.embed(|e| {
                e.color(colour);
                e.field(title, slot_lines, false);
                e.footer(|f| {
                    f.text(footer);
                    f
                });
                e.author(|a| {
                    a.name(pld.msg.author.name.clone());
                    a.icon_url(Avatar::url(pld.msg.author.clone()));
                    a
                })
            });
            msg
        } else {
            UnicornEmbed::small_embed("💸", 0xa7d28b, format!("You don't have {} {}.", bet, currency.name.single))
        };
        response
    }
}

#[serenity::async_trait]
impl UnicornCommand for SlotsCommand {
    fn command_name(&self) -> &str {
        "slots"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn owner(&self) -> bool {
        false
    }

    fn example(&self) -> &str {
        "52"
    }

    fn description(&self) -> &str {
        "Spin the slot machine, maybe you win, maybe you don't. Who knows? \
         The default bet is 10 but you can specify any amount you want. \
         The rewards are based on how many of the same icon you get in the middle row. \
         Rewards are different for each icon. The slots can be spun only once every 60 seconds."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("bet", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = self.slots(pld);
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

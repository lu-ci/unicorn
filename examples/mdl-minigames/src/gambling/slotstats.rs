use crate::gambling::slots::SlotsCommand;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use std::ops::Bound;
use std::ops::RangeBounds;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

pub struct SlotStatsCommand;

impl SlotStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn payout_list(range: impl RangeBounds<usize>, bet: f64) -> String {
        let mut result = String::new();
        let symbols = SlotsCommand::symbols();
        let start = match range.start_bound() {
            Bound::Included(s) => *s,
            Bound::Excluded(s) => s + 1,
            Bound::Unbounded => 0,
        };
        let end = match range.end_bound() {
            Bound::Included(s) => s + 1,
            Bound::Excluded(s) => *s,
            Bound::Unbounded => symbols.len(),
        };
        for slot_item in symbols[start..end].into_iter().rev() {
            let combination2 = (bet * (slot_item.value / 6.66666) * 0.45) as u32;
            let combination3 = (bet * (slot_item.value / 6.66666) * 0.95) as u32;
            result += &format!("{} - {}/**{}**\n", slot_item.symbol, combination2, combination3);
        }
        result
    }
}

#[serenity::async_trait]
impl UnicornCommand for SlotStatsCommand {
    fn command_name(&self) -> &str {
        "slotstats"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn example(&self) -> &str {
        "13000"
    }

    fn description(&self) -> &str {
        "Shows payout rates for the slot machine for the specified bet. This does not cost anything."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("bet", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let bet = pld.args.get_or_default("bet", 10.0);

        let column_one = Self::payout_list(..10, bet);
        let column_two = Self::payout_list(10.., bet);
        let mut response = CreateMessage::default();

        response.embed(|e| {
            e.color(0x5dadec);
            e.title(format!("{} Slot Machine Payout", pld.cfg.preferences.currency.common.icon));
            e.field("Matches (double/**triple**)", column_one, true);
            e.field("Continued...", column_two, true);
            e.footer(|f| {
                f.text(format!(
                    "Payouts for a bet of {} {}.",
                    bet, pld.cfg.preferences.currency.common.name.single
                ))
            });
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

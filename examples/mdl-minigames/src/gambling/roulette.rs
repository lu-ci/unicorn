use rand::Rng;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_resource::origin::ResourceOrigin;
use unicorn_utility::user::Avatar;

#[derive(Clone)]
struct RouletteSlot {
    pub number: i8,
}

impl RouletteSlot {
    pub fn new(number: i8) -> Self {
        Self { number }
    }

    pub fn parity(&self) -> String {
        let parity = if self.number % 2 == 1 { "odd" } else { "even" };
        parity.to_owned()
    }

    pub fn color(&self) -> String {
        let name = if vec![2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35].contains(&self.number) {
            "black"
        } else if self.number == 0 {
            "green"
        } else {
            "red"
        };
        name.to_owned()
    }

    pub fn color_icon(&self) -> String {
        let symbol = if self.color() == "red" {
            "🔴"
        } else if self.color() == "black" {
            "⚫"
        } else {
            "🔵"
        };
        symbol.to_owned()
    }

    pub fn column(&self) -> String {
        if self.number == 0 {
            0.to_string()
        } else if self.number % 3 == 0 {
            3.to_string()
        } else {
            (self.number % 3).to_string()
        }
    }

    fn get_horizontal(&self, block_size: i8) -> i8 {
        if self.number == 0 {
            0
        } else {
            ((self.number - 1) / block_size) + 1
        }
    }

    pub fn dozen(&self) -> String {
        self.get_horizontal(12).to_string()
    }

    pub fn half(&self) -> String {
        self.get_horizontal(18).to_string()
    }

    pub fn street(&self) -> String {
        self.get_horizontal(3).to_string()
    }
    pub fn line(&self) -> String {
        self.get_horizontal(6).to_string()
    }

    pub fn board(&self) -> String {
        let mut board = if self.number == 0 {
            "|      ⚪      |".to_owned()
        } else {
            "|       0      |\n".to_owned()
        };
        for i in 1..37 {
            if i == self.number {
                board += "| ⚪ ";
            } else {
                board += format!("| {:>2} ", i).as_str();
            }
            if i % 3 == 0 {
                board += "|\n";
            }
        }
        board
    }

    pub fn description(&self) -> String {
        format!(
            "{} {}: Column: {} | Dozen: {} | Half: {}",
            self.color_icon(),
            self.number,
            self.column(),
            self.dozen(),
            self.half()
        )
    }
}

#[derive(Default)]
pub struct RouletteCommand {}

impl RouletteCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(RouletteCommand::new())
    }
    pub fn new() -> Self {
        Self {}
    }

    fn set_cooldown(&self, pld: &mut CommandPayload) {
        let key = format!("{}:{}", self.command_name(), pld.msg.author.id);
        let base_cooldown = 60 as f32;
        let profile = pld.db.get_profile(u64::from(pld.msg.author.id));
        let stamina = profile.get_upgrade("stamina") as f32;
        let mut cooldown = (base_cooldown - ((base_cooldown / 100.0) * ((stamina * 0.5) / (1.25 + (0.01 * stamina))))) as i32;
        if cooldown < 5 {
            cooldown = 5;
        }
        pld.cd.set_cooldown(key, u64::from(pld.msg.author.id), cooldown);
    }

    fn get_odds(place: &(String, String), slot: RouletteSlot) -> i64 {
        match place.0.as_str() {
            "type" => {
                if place.1.to_lowercase() == slot.parity() {
                    1
                } else {
                    0
                }
            }
            "color" | "colour" => {
                if place.1.to_lowercase() == slot.color() {
                    1
                } else {
                    0
                }
            }
            "half" => {
                if place.1 == slot.half() {
                    1
                } else {
                    0
                }
            }
            "dozen" => {
                if place.1 == slot.dozen() {
                    2
                } else {
                    0
                }
            }
            "column" => {
                if place.1 == slot.column() {
                    2
                } else {
                    0
                }
            }
            "street" => {
                if place.1 == slot.street() {
                    11
                } else {
                    0
                }
            }
            "line" => {
                if place.1 == slot.line() {
                    5
                } else {
                    0
                }
            }
            "number" => {
                if place.1 == slot.number.to_string() {
                    35
                } else {
                    0
                }
            }
            _ => 0,
        }
    }

    fn in_range(value: &String, range: std::ops::RangeInclusive<i32>) -> bool {
        if let Ok(val) = value.parse::<i32>() {
            range.contains(&val)
        } else {
            false
        }
    }

    fn validate_place(&self, place: &(String, String)) -> Result<(), String> {
        match place.0.as_str() {
            "type" => {
                if (place.1.to_lowercase() == "odd") || (place.1.to_lowercase() == "even") {
                    Ok(())
                } else {
                    Err("value must be odd or even".to_owned())
                }
            }
            "color" | "colour" => {
                if (place.1.to_lowercase() == "red") || (place.1.to_lowercase() == "black") {
                    Ok(())
                } else {
                    Err("value must be red or black".to_owned())
                }
            }
            "column" => {
                if RouletteCommand::in_range(&place.1, 1..=3) {
                    Ok(())
                } else {
                    Err("column must be between 1 and 3".to_owned())
                }
            }
            "dozen" => {
                if RouletteCommand::in_range(&place.1, 1..=3) {
                    Ok(())
                } else {
                    Err("dozen must be between 1 and 3".to_owned())
                }
            }
            "half" => {
                if RouletteCommand::in_range(&place.1, 1..=2) {
                    Ok(())
                } else {
                    Err("half must be 1 or 2".to_owned())
                }
            }
            "line" => {
                if RouletteCommand::in_range(&place.1, 1..=11) {
                    Ok(())
                } else {
                    Err("line must be between 1 and 11".to_owned())
                }
            }
            "street" => {
                if RouletteCommand::in_range(&place.1, 1..=12) {
                    Ok(())
                } else {
                    Err("street must be between 1 and 12".to_owned())
                }
            }
            "number" => {
                if RouletteCommand::in_range(&place.1, 1..=36) {
                    Ok(())
                } else {
                    Err("number must be between 1 and 36".to_owned())
                }
            }
            _ => Err("unrecognised argument".to_owned()),
        }
    }

    fn spin_the_wheel<'a>(&self, pld: &mut CommandPayload) -> CreateMessage<'a> {
        if pld.args.satisfied() {
            self.set_cooldown(pld);
            let currency = pld.cfg.preferences.currency.common.clone();
            let kud = &pld.db.get_resource("currency", u64::from(pld.msg.author.id));
            let bet = pld.args.get_or_default("bet", 10);
            if let Some(place) = pld.args.get_group("place") {
                match self.validate_place(place) {
                    Ok(_) => {
                        let mut rng = rand::thread_rng();
                        let slot = RouletteSlot::new(rng.gen_range(0, 37));
                        let odds = RouletteCommand::get_odds(place, slot.clone());
                        if kud.current >= bet {
                            pld.db.del_resource(
                                "currency",
                                u64::from(pld.msg.author.id),
                                bet,
                                self.command_name(),
                                ResourceOrigin::from(pld.msg.author.id),
                                false,
                            );
                            let mut resp_color = 0xBE1931;
                            let place_text = format!("{} {}", place.0, place.1);
                            let win_lose_text = if odds > 0 {
                                let winnings = bet + (bet * odds);
                                pld.db.add_resource(
                                    "currency",
                                    u64::from(pld.msg.author.id),
                                    winnings.into(),
                                    self.command_name(),
                                    ResourceOrigin::from(pld.msg.author.id),
                                    false,
                                );
                                resp_color = 0x66cc66;
                                format!("{} You won {} {} on {}", currency.icon, winnings - bet, currency.name.single, place_text)
                            } else {
                                format!("You lost {} {} on {}", bet, currency.name.single, place_text)
                            };
                            let mut msg = CreateMessage::default();
                            msg.embed(|e| {
                                e.color(resp_color);
                                e.author(|a| {
                                    a.name(pld.msg.author.name.clone());
                                    a.icon_url(Avatar::url(pld.msg.author.clone()));
                                    a
                                });
                                e.title(slot.description());
                                e.description(format!("\n```hs\n{}\n```", slot.board()));
                                e.footer(|f| {
                                    f.text(win_lose_text);
                                    f
                                })
                            });
                            msg
                        } else {
                            UnicornEmbed::small_embed("💸", 0xa7d28b, format!("You don\'t have {} {}.", bet, currency.name.single))
                        }
                    }
                    Err(error) => UnicornEmbed::error(error),
                }
            } else {
                UnicornEmbed::error("What do you want to place your bet on?")
            }
        } else {
            UnicornEmbed::error("What do you want to place your bet on?")
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for RouletteCommand {
    fn command_name(&self) -> &str {
        "roulette"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn owner(&self) -> bool {
        false
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["roul"]
    }

    fn example(&self) -> &str {
        "52 color red"
    }

    fn description(&self) -> &str {
        "Spin the roulette wheel and maybe you'll win. \
         The default bet is 10 Kud but you can specify any amount you want. \
         The syntax of playing is the bet amount first, it can be ommitted \
         however, and then the roulette spot choice in the form of [selector] [value]. \
         Accepted specifiers are color black or number 15, column 2, dozen 1, \
         half 1, line 7, street 12 and type odd. Good luck."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("bet", true),
            CommandArgument::new("type", true).with_value().group("place"),
            CommandArgument::new("color", true).with_value().group("place"),
            CommandArgument::new("colour", true).with_value().group("place"),
            CommandArgument::new("column", true).with_value().group("place"),
            CommandArgument::new("dozen", true).with_value().group("place"),
            CommandArgument::new("half", true).with_value().group("place"),
            CommandArgument::new("line", true).with_value().group("place"),
            CommandArgument::new("street", true).with_value().group("place"),
            CommandArgument::new("number", true).with_value().group("place"),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = self.spin_the_wheel(pld);
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

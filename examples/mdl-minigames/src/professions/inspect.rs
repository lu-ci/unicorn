use inflector::cases::titlecase::to_title_case;
use log::error;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::user::User;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_callable::reaction::UnicornReaction;
use unicorn_config::config::Configuration;
use unicorn_database::DatabaseHandler;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::string::UnicornString;
use unicorn_utility::user::Avatar;

use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::item::Item;
use crate::professions::recipe::Recipe;

#[derive(Default)]
pub struct InspectCommand;

impl InspectCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    pub fn rboxed() -> Box<dyn UnicornReaction> {
        Box::new(Self)
    }

    pub fn create_inspect_message<'a>(db: &DatabaseHandler, cfg: &Configuration, user: &User, item: Item) -> CreateMessage<'a> {
        let mut item_details = to_title_case(
            format!(
                "{} {} {}",
                UnicornString::connector(item.rarity().as_str()),
                item.rarity(),
                item.item_type
            )
            .as_str(),
        );
        item_details += format!("\n It is valued at {} {}", item.value, cfg.preferences.currency.common.name.single).as_str();
        let recipes = Recipe::get_recipes_for_item(&db, &item.file_id);
        if recipes.len() > 0 {
            let mut recipe_names = String::new();
            let mut is_first = true;
            for recipe in recipes {
                if !is_first {
                    is_first = false;
                    recipe_names += ", ";
                }
                recipe_names += &recipe.name;
                if Recipe::get_recipes_for_item(&db, &recipe.file_id).len() > 0 {
                    recipe_names += "*";
                }
            }
            item_details += format!("\n Used In: {}", recipe_names).as_str();
        }

        let mut stat_count = 0;
        let stats = Item::get_statistics_for_user(&db, u64::from(user.id));
        for stat in stats {
            let (file_id, count) = stat;
            if file_id == item.file_id {
                stat_count = count;
                break;
            }
        }
        let item_total = Item::get_statistics_for_item(&db, &item);

        let mut footer_text = format!("You found: {} | Total Found: {}", stat_count, item_total);
        if let Some(item) = Inventory::get_item_by_file_id(&db, u64::from(user.id), &item.file_id) {
            let inventory = Inventory::get_for_player(&db, u64::from(user.id));
            let count = inventory.items.iter().filter(|i| i.file_id == item.file_id).count();
            footer_text += format!(" | Owned: {}", count).as_str();
        }

        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(item.color);
            e.field(format!("{} {}", item.icon, item.name), item_details, false);
            e.field("Item Description", item.description, false);
            e.author(|a| {
                a.name(user.name.clone());
                a.icon_url(Avatar::url(user.clone()));
                a
            });
            e.footer(|f| {
                f.text(footer_text);
                f
            });
            e
        });
        response
    }
}

#[serenity::async_trait]
impl UnicornCommand for InspectCommand {
    fn command_name(&self) -> &str {
        "inspect"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["finditem"]
    }

    fn example(&self) -> &str {
        "Nabfischz"
    }

    fn description(&self) -> &str {
        "Shows the name, value, and description of the specified item \
         If you have this item in your inventory an Item ID will be in the footer. \
         Also shows how many times you've caught this item."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("item", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.satisfied() {
            match Item::get_by_name(&pld.db, pld.args.get("item")) {
                Some(item) => {
                    if item.rarity < 0 {
                        UnicornEmbed::error("Sorry but that's trash")
                    } else {
                        Self::create_inspect_message(&pld.db, &pld.cfg, &pld.msg.author, item)
                    }
                }
                None => UnicornEmbed::not_found("Item not found"),
            }
        } else {
            UnicornEmbed::not_found("Inspect what?")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

#[serenity::async_trait]
impl UnicornReaction for InspectCommand {
    async fn execute(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        if let Ok(message) = pld.reaction.message(pld.ctx.http()).await {
            if let Ok(user) = pld.reaction.user(pld.ctx.http()).await {
                for embed in message.embeds {
                    if let Some(author) = embed.author {
                        if author.name == user.name {
                            if let Some(footer) = embed.footer {
                                if footer.text.starts_with("item id: ") && (footer.text.len() > 9) {
                                    let item_id = footer.text.split_at(9).1;
                                    if let Some(item) = Inventory::get_item_by_id(&pld.db, u64::from(pld.reaction.user_id), item_id) {
                                        if let Some(actual_item) = Item::get_by_id(&pld.db, item.file_id.as_str()) {
                                            if let Err(why) = pld.reaction.delete(pld.ctx.http()).await {
                                                error!("{}", why);
                                            }
                                            let mut response = Self::create_inspect_message(&pld.db, &pld.cfg, &user, actual_item);
                                            pld.reaction.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }

    fn emojis(&self) -> Option<Vec<&str>> {
        Some(vec!["🔍"])
    }
}

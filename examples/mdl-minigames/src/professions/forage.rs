use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_callable::reaction::UnicornReaction;
use unicorn_utility::string::UnicornString;

use crate::professions::find_item::FindItem;

pub struct ForageCommand {
    item_finder: FindItem,
}

impl ForageCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(ForageCommand::new())
    }
    pub fn rboxed() -> Box<dyn UnicornReaction> {
        Box::new(ForageCommand::new())
    }

    pub fn new() -> Self {
        let mut item_finder = FindItem::new();
        item_finder.item_type = "plant".to_owned();
        item_finder.command_name = "forage".to_owned();
        item_finder.cooldown_message = Box::new(|x| format!("You are resting for another {} seconds.", x));
        item_finder.trash_message = Box::new(|i| format!("You found {} {} and threw it away", UnicornString::connector(i.name.as_str()), i.name));
        item_finder.search_message = Box::new(|i| format!("{} Quick! Get the correct {}", i.icon.as_str(), i.item_type));
        item_finder.no_items_message = Box::new(|| "There are no plants here".to_owned());
        item_finder.timeout_message = Box::new(|i| format!("You forgot where the {} {} is...", i.rarity(), i.item_type));
        item_finder.success_message = Box::new(|i| format!("You found {} {} {}", UnicornString::connector(i.rarity().as_str()), i.rarity(), i.name));
        item_finder.failure_message = Box::new(|_| "Oh no... You dug too hard and hurt the plant...".to_owned());
        Self { item_finder }
    }
}
#[serenity::async_trait]
impl UnicornReaction for ForageCommand {
    async fn execute(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        self.item_finder.check_reaction(pld).await
    }

    fn emojis(&self) -> Option<Vec<&str>> {
        None
    }
}

#[serenity::async_trait]
impl UnicornCommand for ForageCommand {
    fn command_name(&self) -> &str {
        "forage"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Go hiking and search nature for all the delicious bounties it has. \
         Look for plants that you might want to use for cooking in the future. \
         You can forage once every 60 seconds, hiking is really tiring."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("rarity", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        self.item_finder.find_item(pld).await?;
        Ok(())
    }
}

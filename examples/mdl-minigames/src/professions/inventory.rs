use std::cmp::Ordering;

use bson::*;
use log::error;
use serde::{Deserialize, Serialize};
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::{CommandArgument, CommandArguments};
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::paginator::Paginator;
use unicorn_callable::payload::CommandPayload;
use unicorn_database::DatabaseHandler;
use unicorn_utility::table::{ColumnAlignment, Table, TableFormatting};

use crate::professions::db::DBCollection;
use crate::professions::inventory_item::InventoryItem;
use crate::professions::item::Item;
use crate::professions::recipe::Recipe;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Inventory {
    uid: i64,
    pub items: Vec<InventoryItem>,
}

impl Inventory {
    fn new(uid: u64) -> Self {
        Self {
            uid: uid as i64,
            items: Vec::new(),
        }
    }

    fn to_doc(&self) -> Option<Document> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs.as_document()?.clone()),
            Err(why) => {
                error!("Failed encoding inventory: {}", why);
                None
            }
        }
    }

    fn get_inventory_from_db(db: &DatabaseHandler, uid: u64) -> Option<Inventory> {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"uid": uid};
            if let Ok(Some(doc)) = cli.database(&db.cfg.database.name).collection("inventory").find_one(lookup, None) {
                if let Ok(items) = bson::from_bson::<Inventory>(bson::Bson::Document(doc)) {
                    Some(items)
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    }

    fn write_to_db(db: &DatabaseHandler, inventory: Inventory) {
        if let Some(doc) = inventory.to_doc() {
            if let Some(cli) = db.get_client() {
                let update = doc! {"$set": doc.clone()};
                if let Some(inventory) = Inventory::get_inventory_from_db(db, inventory.uid as u64) {
                    if let Err(why) =
                        cli.database(&db.cfg.database.name)
                            .collection("inventory")
                            .update_one(doc! {"uid": inventory.uid}, update, None)
                    {
                        error!("Could not update inventory: {}", why);
                    }
                } else {
                    if let Err(why) = cli.database(&db.cfg.database.name).collection("inventory").insert_one(doc, None) {
                        error!("Could not insert inventory: {}", why);
                    }
                }
            }
        }
    }

    pub fn get_for_player(db: &DatabaseHandler, uid: u64) -> Inventory {
        if let Some(items) = Inventory::get_inventory_from_db(db, uid) {
            items
        } else {
            Inventory::new(uid)
        }
    }

    pub fn add_item(db: &DatabaseHandler, uid: u64, item: InventoryItem) {
        let mut inventory = Inventory::get_for_player(db, uid);
        inventory.items.push(item);
        Inventory::write_to_db(db, inventory);
    }

    pub fn get_item_by_id(db: &DatabaseHandler, uid: u64, item_id: &str) -> Option<InventoryItem> {
        let inventory = Inventory::get_for_player(db, uid);
        for item in inventory.items {
            if item.item_id == item_id {
                return Some(item);
            }
        }
        None
    }

    pub fn get_item_by_file_id(db: &DatabaseHandler, uid: u64, item_id: &str) -> Option<InventoryItem> {
        let inventory = Inventory::get_for_player(db, uid);
        for item in inventory.items {
            if item.file_id == item_id {
                return Some(item);
            }
        }
        None
    }

    pub fn del_item(db: &DatabaseHandler, uid: u64, item: InventoryItem) {
        let mut inventory = Inventory::get_for_player(db, uid);
        let mut idx = -1i64;
        for (index, inv_item) in inventory.items.clone().into_iter().enumerate() {
            if inv_item.item_id == item.item_id {
                idx = index as i64;
                break;
            }
        }
        if idx >= 0 {
            inventory.items.remove(idx as usize);
            Inventory::write_to_db(db, inventory);
        }
    }
}

#[derive(Default)]
pub struct InventoryCommand {}

impl InventoryCommand {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }

    fn matches_filter(args: &CommandArguments, item: &Item) -> bool {
        if args.has_argument("filter") {
            let filter_string = args.get("filter").to_lowercase();
            item.rarity().to_lowercase().contains(&filter_string)
                || item.name.to_lowercase().contains(&filter_string)
                || item.description.to_lowercase().contains(&filter_string)
        } else {
            true
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for InventoryCommand {
    fn command_name(&self) -> &str {
        "inventory"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["backpack", "storage", "bag", "i"]
    }

    fn example(&self) -> &str {
        "2 @person"
    }

    fn description(&self) -> &str {
        "Shows the current inventory of the mentioned user. \
         If no user is mentioned, it will show the author's inventory. \
         The inventory has 64 slots at the start but can be upgraded in the shop. \
         You can also specify the page number you want to see. \
         The inventory is sorted by item rarity and items used in \
         recipes are marked with an asterisk."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![Paginator::pagination_args(), vec![CommandArgument::new("filter", true).allows_spaces()]].concat()
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = match pld.msg.mentions.get(0) {
            Some(user) => user,
            None => &pld.msg.author,
        };
        let profile = pld.db.get_profile(u64::from(target.id));
        let storage_level = profile.get_upgrade("storage");
        let bag_limit = 64 + (storage_level * 8);
        let bag = Inventory::get_for_player(&pld.db, u64::from(target.id));

        let mut all_items = Vec::new();
        let mut value = 0;
        for item in bag.items {
            if let Some(real_item) = Item::get_by_id(&pld.db, &item.file_id) {
                if Self::matches_filter(&pld.args, &real_item) {
                    value += real_item.value;
                    all_items.push(real_item);
                }
            }
        }
        let mut response = if all_items.len() > 0 {
            all_items.sort_unstable_by(|a, b| {
                if let Some(order) = a.rarity.partial_cmp(&b.rarity) {
                    if order == Ordering::Equal {
                        a.name.partial_cmp(&b.name).unwrap()
                    } else {
                        order.reverse()
                    }
                } else {
                    Ordering::Equal
                }
            });
            let item_count = all_items.len();
            let paginator = Paginator::new(pld, all_items);
            let page = paginator.get_page();
            let headers = vec!["Type", "Item", "Value", "Rarity"];
            let mut page_contents = Vec::new();
            for item in page {
                let mut item_name = item.name.clone();
                if Recipe::get_recipes_for_item(&pld.db, &item.file_id).len() > 0 {
                    item_name += "*";
                }
                page_contents.push(vec![item.item_type.clone(), item_name, item.value.to_string(), item.rarity()]);
            }
            let mut formatting = TableFormatting::new();
            formatting.alignment = Some(vec![
                ColumnAlignment::Left,
                ColumnAlignment::Left,
                ColumnAlignment::Right,
                ColumnAlignment::Left,
            ]);
            let output = Table::format_table(Some(headers), page_contents, Some(formatting));
            let pronoun1 = if target.id == pld.msg.author.id { "You" } else { "They" };
            let pronoun2 = if target.id == pld.msg.author.id { "your" } else { "their" };
            let currency = &pld.cfg.preferences.currency.common.name.single;
            let stats_text = format!(
                "Showing items: {}-{}.\n{} have {}/{} items in {} inventory.\nTotal value of {} inventory is {} {}",
                paginator.start_range, paginator.end_range, pronoun1, item_count, bag_limit, pronoun2, pronoun2, value, currency
            );
            let mut inv_message = CreateMessage::default();
            inv_message.embed(|e| {
                e.color(0xc16a4f);
                e.field("📦 Inventory Stats", format!("```py\n{}\n```", stats_text), true);
                e.field(
                    format!("📋 Items Currently On Page {}", paginator.page),
                    format!("```py\n{}\n```", output),
                    false,
                );
                e
            });
            inv_message
        } else {
            unicorn_embed::UnicornEmbed::small_embed("💸", 0xc6e4b5, "You got nothin'")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

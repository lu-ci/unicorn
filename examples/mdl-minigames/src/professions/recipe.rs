use std::cmp::Ordering;

use bson::*;
use log::error;
use serde::{Deserialize, Serialize};
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::paginator::Paginator;
use unicorn_callable::payload::CommandPayload;
use unicorn_database::DatabaseHandler;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::table::{ColumnAlignment, Table, TableFormatting};

use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::item::Item;
use crate::professions::yaml::YamlRecipe;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Recipe {
    pub name: String,
    pub color: u64,
    pub value: i32,
    pub icon: String,
    pub recipe_type: String,
    pub description: String,
    pub file_id: String,
    pub ingredients: Vec<String>,
}

impl Recipe {
    pub fn cook_icons(id: &str) -> &str {
        match id {
            "drink" => "🍶",
            "meal" => "🍱",
            "dessert" => "🍰",
            _ => "",
        }
    }
    pub fn cook_colors(id: &str) -> u64 {
        match id {
            "drink" => 0x55ACEE,
            "meal" => 0xDD2E44,
            "dessert" => 0xF9F9F9,
            _ => 0x000000,
        }
    }
    pub fn cook_quality<'a>(quality: i32) -> &'a str {
        match quality {
            0 => "Terrible",
            1 => "Ropey",
            2 => "Ok",
            3 => "Normal",
            4 => "Delicious",
            5 => "Exquisite",
            6 => "Marvelous",
            _ => "Inedible",
        }
    }

    pub fn get_recipes_for_item(db: &DatabaseHandler, item_id: &str) -> Vec<Recipe> {
        let cache_key = format!("recipes_for:{}", item_id);

        let cache_items = if let Some(body) = db.cache.get(cache_key.clone()) {
            match serde_json::from_str::<Vec<Self>>(&body) {
                Ok(items) => Some(items),
                Err(why) => {
                    error!("Failed desererializing items cache: {}", why);
                    None
                }
            }
        } else {
            None
        };

        if let Some(items) = cache_items {
            items
        } else {
            let items = Self::get_recipes_for_item_from_db(&db, item_id);
            if !items.is_empty() {
                if let Ok(body) = serde_json::to_string(&items) {
                    db.cache.set(cache_key, body)
                }
            }
            items
        }
    }

    pub fn get_recipes_for_item_from_db(db: &DatabaseHandler, item_id: &str) -> Vec<Recipe> {
        let mut result = Vec::new();
        if let Some(cli) = db.get_client() {
            if let Ok(cursor) = cli
                .database(&db.cfg.database.name)
                .collection("recipes")
                .find(doc! {"ingredients": item_id}, None)
            {
                for entry in cursor {
                    if let Ok(doc) = entry {
                        match bson::from_bson::<YamlRecipe>(bson::Bson::Document(doc)) {
                            Ok(item) => result.push(Recipe::from(item)),
                            Err(why) => {
                                let reason = format!("{}", why);
                                error!("{}", reason);
                            }
                        }
                    }
                }
            }
        }
        result
    }
}

impl DBCollection for Recipe {
    fn collection_name() -> &'static str {
        "recipes"
    }
    fn get_id(&self) -> &str {
        self.file_id.as_str()
    }

    fn get_name(&self) -> &str {
        self.name.as_str()
    }

    fn get_from_db_by_id(db: &DatabaseHandler, id: &str) -> Option<Recipe> {
        let mut result = None;
        if let Some(cli) = db.get_client() {
            if let Ok(db_result) = cli
                .database(&db.cfg.database.name)
                .collection("recipes")
                .find_one(doc! {"file_id": id}, None)
            {
                if let Some(doc) = db_result {
                    if let Ok(yaml_item) = bson::from_bson::<YamlRecipe>(Bson::from(doc)) {
                        result = Some(Recipe::from(yaml_item));
                    }
                }
            }
        }
        result
    }

    fn get_all_from_db(db: &DatabaseHandler) -> Vec<Recipe> {
        let mut items = Vec::<Recipe>::new();
        if let Some(cli) = db.get_client() {
            if let Ok(cursor) = cli.database(&db.cfg.database.name).collection("recipes").find(None, None) {
                for entry in cursor {
                    if let Ok(doc) = entry {
                        match bson::from_bson::<YamlRecipe>(bson::Bson::Document(doc)) {
                            Ok(yaml_item) => {
                                let mut item = Recipe::from(yaml_item);
                                if let Some(real_item) = Item::get_by_id(&db, &item.file_id) {
                                    item.value = real_item.calculate_value(&db);
                                    item.icon = Self::cook_icons(&item.recipe_type).to_string();
                                    item.color = Self::cook_colors(&item.recipe_type)
                                }
                                items.push(item);
                            }
                            Err(why) => {
                                let reason = format!("{}", why);
                                error!("{}", reason);
                            }
                        }
                    }
                }
            }
        }
        items
    }
}

impl From<YamlRecipe> for Recipe {
    fn from(yaml: YamlRecipe) -> Self {
        let colour = Recipe::cook_colors(&yaml.file_id);
        let icon = Recipe::cook_icons(&yaml.file_id);
        Self {
            name: yaml.name,
            color: colour,
            icon: icon.to_owned(),
            recipe_type: yaml.recipe_type,
            description: yaml.description,
            file_id: yaml.file_id,
            ingredients: yaml.ingredients,
            value: 0,
        }
    }
}

#[derive(Default)]
pub struct RecipeCommand {}

impl RecipeCommand {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornCommand for RecipeCommand {
    fn command_name(&self) -> &str {
        "viewrecipe"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["vrec", "recipe"]
    }

    fn example(&self) -> &str {
        "Shade Tea"
    }

    fn description(&self) -> &str {
        "Shows details on the specified recipe, such as the \
         ingredients required, value of the item, and its description."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("name", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut message = if pld.args.satisfied() {
            if let Some(recipe) = Recipe::get_by_name(&pld.db, pld.args.get("name")) {
                let inventory = Inventory::get_for_player(&pld.db, u64::from(pld.msg.author.id));
                let mut recipe_complete = true;
                let mut ingredients = String::new();
                let mut ingredient_icon = "💩".to_owned();
                let mut value = 0;
                for ingredient in recipe.ingredients {
                    let mut found_item = false;
                    for item in &inventory.items {
                        if item.file_id == ingredient {
                            found_item = true;
                            break;
                        }
                    }
                    let mut inventory_mark = "☒";
                    let mut ingredient_name = ingredient.clone();
                    if let Some(item) = Item::get_by_id(&pld.db, &ingredient) {
                        if value == 0 {
                            ingredient_icon = item.icon;
                        }
                        ingredient_name = item.name;
                        value += item.value;
                        if !found_item {
                            inventory_mark = "☐";
                            recipe_complete = false;
                        }
                    } else {
                        error!("Recipe ingredient {} doesn't exist!", ingredient);
                    }
                    ingredients += &format!("\n{} **{}**", inventory_mark, ingredient_name);
                }
                if let Some(item) = Item::get_by_id(&pld.db, &recipe.file_id) {
                    let currency = &pld.cfg.preferences.currency.common.name.single;
                    let item_description = format!(
                        "Type: **{}**\nValue: **{} {}**\
                         \nHave Ingredients: **{}**\
                         \nIngredient Value: **{} {}**",
                        item.item_type, item.value, currency, recipe_complete, value, currency
                    );
                    let mut msg = CreateMessage::default();
                    msg.embed(|e| {
                        e.color(item.color);
                        e.field(format!("{} {}", item.icon, item.name), item_description, true);
                        e.field(format!("{} Ingredients", ingredient_icon), ingredients, true);
                        e.field("📰 Description", item.description, false);
                        e
                    });
                    msg
                } else {
                    UnicornEmbed::not_found(format!("The item of the recipe for {} was not found", pld.args.get("recipe")))
                }
            } else {
                UnicornEmbed::not_found(format!("The recipe for {} was not found", pld.args.get("name")))
            }
        } else {
            UnicornEmbed::not_found("Recipe for what?")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut message).await?;
        Ok(())
    }
}

#[derive(Default)]
pub struct RecipesCommand {}

impl RecipesCommand {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornCommand for RecipesCommand {
    fn command_name(&self) -> &str {
        "recipes"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["cookbook"]
    }

    fn example(&self) -> &str {
        "2 meals craftable"
    }

    fn description(&self) -> &str {
        "Lists all recipes available for making. You can filter the \
         recipes by type by adding \"desserts\", \"meals\", or \"drinks\" \
         as an argument. You can also filter the recipes by whether you \
         have all the ingredients or not by adding \"craftable\" \
         as an argument. All filters go after the page number, \
         if you specify one. The recipe list is limited to \
         10 items per page. You can specify the page number you want to see."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            Paginator::pagination_args(),
            vec![CommandArgument::new("type", true), CommandArgument::new("craftable", true).is_flag()],
        ]
        .concat()
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut all_items = Recipe::get_all(&pld.db);
        if pld.args.has_argument("type") {
            let item_type = pld.args.get("type");
            all_items = all_items
                .into_iter()
                .filter(|f| f.recipe_type.to_lowercase() == item_type.to_lowercase())
                .collect();
        }
        all_items.sort_unstable_by(|a, b| {
            if let Some(order) = a.value.partial_cmp(&b.value) {
                if order == Ordering::Equal {
                    a.name.partial_cmp(&b.name).unwrap()
                } else {
                    order.reverse()
                }
            } else {
                Ordering::Equal
            }
        });
        let mut matching_recipes = Vec::new();
        for item in all_items {
            let mut item_name = item.name.clone();
            if Recipe::get_recipes_for_item(&pld.db, &item.file_id).len() > 0 {
                item_name += "*";
            }
            let mut i_count = 0;
            for ingredient in &item.ingredients {
                if let Some(_) = Inventory::get_item_by_id(&pld.db, u64::from(pld.msg.author.id), &ingredient) {
                    i_count += 1;
                }
            }
            let ingredients = format!("{}/{}", i_count, item.ingredients.len());
            if i_count == item.ingredients.len() || !pld.args.has_argument("craftable") {
                matching_recipes.push(vec![item.recipe_type.clone(), item_name, item.value.to_string(), ingredients]);
            }
        }

        let item_count = matching_recipes.len();
        let mut response = if item_count > 0 {
            let paginator = Paginator::new(pld, matching_recipes);
            let page = paginator.get_page();
            let headers = vec!["Type", "Item", "Value", "Ingr."];
            let mut page_contents = Vec::new();
            for item in page {
                page_contents.push(item.clone());
            }
            let mut formatting = TableFormatting::new();
            formatting.alignment = Some(vec![
                ColumnAlignment::Left,
                ColumnAlignment::Left,
                ColumnAlignment::Right,
                ColumnAlignment::Left,
            ]);
            let output = Table::format_table(Some(headers), page_contents, Some(formatting));
            let stats_text = format!(
                "Showing recipes: {}-{}.\nThere are a total of {} recipes.",
                paginator.start_range, paginator.end_range, item_count
            );
            let mut recipe_response = CreateMessage::default();
            recipe_response.embed(|e| {
                e.color(0xc16a4f);
                e.field("📦 Recipe Stats", format!("```py\n{}\n```", stats_text), true);
                e.field(
                    format!("📰 Recipes Currently On Page {}", paginator.page),
                    format!("```py\n{}\n```", output),
                    false,
                );
                e
            });
            recipe_response
        } else {
            UnicornEmbed::not_found("No recipes match the given filter.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_callable::reaction::UnicornReaction;
use unicorn_utility::string::UnicornString;

use crate::professions::find_item::FindItem;

pub struct FishCommand {
    item_finder: FindItem,
}

impl FishCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(FishCommand::new())
    }
    pub fn rboxed() -> Box<dyn UnicornReaction> {
        Box::new(FishCommand::new())
    }

    pub fn new() -> Self {
        let mut item_finder = FindItem::new();
        item_finder.item_type = "fish".to_owned();
        item_finder.command_name = "fish".to_owned();
        item_finder.cooldown_message = Box::new(|x| format!("Your new bait will be ready in {} seconds.", x));
        item_finder.trash_message = Box::new(|i| format!("You caught {} {} and threw it away", UnicornString::connector(i.name.as_str()), i.name));
        item_finder.search_message = Box::new(|i| format!("{} Quick! Get the correct {}", i.icon.as_str(), i.item_type));
        item_finder.no_items_message = Box::new(|| "There are no fish here".to_owned());
        item_finder.timeout_message = Box::new(|i| format!("Oh no... The {} {} escaped...", i.rarity(), i.item_type));
        item_finder.success_message =
            Box::new(|i| format!("You caught {} {} {}!", UnicornString::connector(i.rarity().as_str()), i.rarity(), i.name));
        item_finder.failure_message = Box::new(|_| "Oh no... You pulled too hard and the line broke...".to_owned());
        Self { item_finder }
    }
}

#[serenity::async_trait]
impl UnicornReaction for FishCommand {
    async fn execute(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        self.item_finder.check_reaction(pld).await
    }

    fn emojis(&self) -> Option<Vec<&str>> {
        None
    }
}

#[serenity::async_trait]
impl UnicornCommand for FishCommand {
    fn command_name(&self) -> &str {
        "fish"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Cast a lure and try to catch some fish. You can fish once every 60 seconds, \
         better not scare the fish away."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("rarity", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        self.item_finder.find_item(pld).await?;
        Ok(())
    }
}

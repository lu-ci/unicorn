use std::cmp::Ordering;

use inflector::cases::titlecase::to_title_case;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::user::User;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::paginator::Paginator;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::table::{ColumnAlignment, Table, TableFormatting};
use unicorn_utility::user::Avatar;

use crate::professions::db::DBCollection;
use crate::professions::item::Item;
use crate::professions::recipe::Recipe;

#[derive(Default)]
pub struct AllItemsCommand;

impl AllItemsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for AllItemsCommand {
    fn command_name(&self) -> &str {
        "allitems"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn example(&self) -> &str {
        "desserts"
    }

    fn description(&self) -> &str {
        "Shows the entire item pool. You can specify a type to only show items of that type. \
         You can also specify the page number you want to see, this goes after the item type \
         if you give one. The item pool is sorted by item rarity. \
         Items used in recipes are marked with an asterisk."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![Paginator::pagination_args(), vec![CommandArgument::new("type", true).with_value()]].concat()
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut all_items = Item::get_all(&pld.db);
        if pld.args.has_argument("type") {
            let item_type = pld.args.get("type");
            all_items = all_items
                .into_iter()
                .filter(|f| f.item_type.to_lowercase() == item_type.to_lowercase())
                .collect();
        }
        all_items.sort_unstable_by(|a, b| {
            if let Some(order) = a.rarity.partial_cmp(&b.rarity) {
                if order == Ordering::Equal {
                    a.name.partial_cmp(&b.name).unwrap()
                } else {
                    order.reverse()
                }
            } else {
                Ordering::Equal
            }
        });
        let mut total_value = 0;
        for item in &all_items {
            total_value += item.value;
        }
        let item_count = all_items.len();
        let paginator = Paginator::new(pld, all_items);
        let page = paginator.get_page();
        let headers = vec!["Type", "Item", "Value", "Rarity"];
        let mut page_contents = Vec::new();

        for item in page {
            let mut item_name = item.name.clone();
            if Recipe::get_recipes_for_item(&pld.db, &item.file_id).len() > 0 {
                item_name += "*";
            }
            page_contents.push(vec![
                item.item_type.clone(),
                item_name,
                item.value.to_string(),
                to_title_case(item.rarity().as_str()),
            ]);
        }
        let currency_name = &pld.cfg.preferences.currency.common.name.single;
        let mut formatting = TableFormatting::new();
        formatting.alignment = Some(vec![
            ColumnAlignment::Left,
            ColumnAlignment::Left,
            ColumnAlignment::Right,
            ColumnAlignment::Left,
        ]);
        let output = Table::format_table(Some(headers), page_contents, Some(formatting));
        let inv_text = format!(
            "Showing items {}-{} out of {}.\nThe total value of this pool is {} {}.",
            paginator.start_range, paginator.end_range, item_count, total_value, currency_name
        );
        let bot = pld.ctx.cache.current_user().await;
        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(0xc16a4f);
            e.author(|a| {
                a.name("Sigma");
                a.icon_url(Avatar::url(User::from(bot.clone())));
                a
            });
            e.field("📦 Item Pool Stats", format!("```py\n{}\n```", inv_text), true);
            e.field(
                format!("📋 Items Currently On Page {}", paginator.page),
                format!("```py\n{}\n```", output),
                false,
            );
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

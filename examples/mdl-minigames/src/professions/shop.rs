use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::id::UserId;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::dialogue::{Dialogue, DialogResult};
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, };
use unicorn_config::config::Configuration;
use unicorn_database::DatabaseHandler;
use unicorn_resource::origin::ResourceOrigin;

#[derive(Clone)]
struct Upgrade {
    id: String,
    name: String,
    cost: f64,
    description: String,
}

impl Upgrade {
    fn new(id: &str, name: &str, cost: f64, description: &str) -> Self {
        Self {
            id: id.to_owned(),
            name: name.to_owned(),
            cost: cost.to_owned(),
            description: description.to_owned(),
        }
    }

    fn get_price(&self, current_level: i32) -> i64 {
        if current_level == 0 {
            self.cost as i64
        } else {
            let price_mod = (self.cost * current_level as f64 * (1.10 + (0.075 * current_level as f64))) as i64;
            price_mod + (price_mod / 2)
        }
    }
}

#[derive(Default)]
pub struct ShopCommand {
}

impl ShopCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(ShopCommand::default())
    }

    fn get_upgrade_from_arg(&self, arg: &str) -> Option<Upgrade> {
        let mut result = None;
        for upgrade in ShopCommand::upgrades() {
            if upgrade.id == arg || upgrade.name == arg {
                result = Some(upgrade.clone())
            }
        }
        result
    }

    fn buy<'a>(command_name: &str, db: &DatabaseHandler, cfg: &Configuration, user: UserId, upgrade: &Upgrade) -> CreateMessage<'a> {
        let currency = &cfg.preferences.currency.common;
        let mut profile = db.get_profile(user.into());
        let upgrade_level = profile.get_upgrade(&upgrade.id);
        let upgrade_price = upgrade.get_price(upgrade_level);
        let cash = db.get_resource("currency", user.into());
        let response = if cash.current > upgrade_price {
            profile.set_upgrade(&upgrade.id, upgrade_level + 1);
            profile.save(&db);
            db.del_resource("currency", user.into(), upgrade_price, command_name, ResourceOrigin::from(user), true);
            unicorn_embed::UnicornEmbed::ok(format!("Upgraded your {} to Level {}.", upgrade.name, upgrade_level + 1))
        } else {
            unicorn_embed::UnicornEmbed::small_embed(
                "💸",
                0xa7d28b,
                format!("You need {} more {}", upgrade_price - cash.current, currency.name.single),
            )
        };
        response
    }

    async fn quick_buy(&self, pld: &mut CommandPayload, upgrade: Upgrade) -> Result<(), CallableError> {
        let mut response = Self::buy(self.command_name(), &pld.db, &pld.cfg, pld.msg.author.id, &upgrade);
        pld.msg.channel_id.send_message(&pld.ctx, |_| &mut response).await?;
        Ok(())
    }

    fn upgrades() -> Vec<Upgrade> {
        vec![
            Upgrade::new("stamina", "Endurance Training", 300.0, "Reduces professsion cool-downs"),
            Upgrade::new("luck", "Lucky Enchantment", 400.0, "Increases the chance for rare items"),
            Upgrade::new("storage", "Inventory Expansion", 100.0, "Increases your inventory space by 8."),
            Upgrade::new("oven", "Super Heated Oven", 150.0, "Decreases the cookie giving cool-down."),
            Upgrade::new("casino", "Casino Investment", 300.0, "Reduces the slot machine cool-down."),
            Upgrade::new("harem", "Harem Expansion", 500.0, "Increases your harem size by 1."),
        ]
    }

    fn list_upgrades(pld: &mut CommandPayload) -> String {
        let profile = &pld.db.get_profile(u64::from(pld.msg.author.id));
        let mut upgrade_text = "".to_owned();
        let mut upgrade_index = 0;
        let currency = &pld.cfg.preferences.currency.common.name.single;
        for upgrade in ShopCommand::upgrades() {
            upgrade_index += 1;
            let level = profile.get_upgrade(&upgrade.id);
            let cost = upgrade.get_price(level);
            upgrade_text += &format!(
                "\n**{}**: Level {} {} - {} {}\n > {}",
                upgrade_index,
                level + 1,
                upgrade.name,
                cost,
                currency,
                upgrade.description
            );
        }
        upgrade_text
    }

    async fn number_chosen(&self, pld: &CommandPayload, result: DialogResult) -> Result<bool, CallableError> {
        if let DialogResult::Number(choice) = result {
            if let Some(upgrade) = Self::upgrades().get(choice - 1) {
                let mut response = Self::buy("buyupgrade", &pld.db, &pld.cfg, pld.msg.author.id, &upgrade);
                pld.msg.channel_id.send_message(&pld.ctx, |_| &mut response).await?;
            }
        }
        Ok(true)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ShopCommand {
    fn command_name(&self) -> &str {
        "buyupgrade"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["shop"]
    }

    fn description(&self) -> &str {
        "Opens Sigma's profession upgrade shop."
    }
    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("upgrade", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        if let Some(upgrade) = self.get_upgrade_from_arg(pld.args.get("upgrade")) {
            self.quick_buy(pld, upgrade).await

        } else {
            let mut dlg = Dialogue::numbered_options("buyupgrade", 6);
            dlg.title = Some("🛍 Profession Upgrade Shop".to_string());
            dlg.footer = Some("Please input the number of the upgrade you want.".to_string());
            dlg.description = Some(Box::new(|pld| ShopCommand::list_upgrades(pld)));
            if !dlg.is_open(pld, pld.msg.author.id) {
               let dialog_result = dlg.show_dialog(pld).await? ;
                    self.number_chosen(pld, dialog_result).await?;
                Ok(())
            } else {
                let mut response = unicorn_embed::UnicornEmbed::error("You already have a shop open");
                pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
                Ok(())
            }
        }
    }
}

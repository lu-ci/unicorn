use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::item::Item;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use std::collections::HashMap;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::paginator::Paginator;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::table::{ColumnAlignment, Table, TableFormatting};
use unicorn_utility::user::Avatar;

#[derive(Default)]
pub struct ItemStatsCommand;

impl ItemStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ItemStatsCommand {
    fn command_name(&self) -> &str {
        "itemstatistics"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["itemstats"]
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Shows the statistics of your item history. How much of which item you've caught or made, that is. \
         It's sorted by the most caught to the least caught items. Items that you've never obtained are not shown. \
         You can view another person's statistics by mentioning them."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![Paginator::pagination_args(), vec![CommandArgument::new("current", true).is_flag()]].concat()
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = match pld.msg.mentions.get(0) {
            Some(user) => user,
            None => &pld.msg.author,
        };
        let all_items = Item::get_all(&pld.db);

        let all_stats = if pld.args.has_argument("current") {
            let bag = Inventory::get_for_player(&pld.db, u64::from(target.id));
            let mut map = HashMap::new();
            for inv_item in bag.items {
                for item in &all_items {
                    if item.file_id == inv_item.file_id {
                        if let Some(count) = map.get_mut(&item.name) {
                            *count += 1;
                        } else {
                            map.insert(item.name.clone(), 1);
                        }
                    }
                }
            }

            map.into_iter()
                .map(|(key, value)| vec![key, value.to_string()])
                .collect::<Vec<Vec<String>>>()
        } else {
            let mut item_stats = Item::get_statistics_for_user(&pld.db, u64::from(target.id));
            item_stats.sort_by(|a, b| a.1.cmp(&b.1).reverse());
            let mut stats = Vec::new();
            for item in item_stats {
                let (file_id, count) = item;
                if file_id != "_id" && file_id != "user_id" {
                    for item in &all_items {
                        if item.file_id == file_id {
                            stats.push(vec![item.name.clone(), count.to_string()]);
                            break;
                        }
                    }
                }
            }
            stats
        };
        let item_count = all_stats.len();
        let total_count = all_items.len();
        let paginator = Paginator::new(pld, all_stats);
        let page = paginator.get_page();
        let headers = vec!["Item", "Count"];
        let mut formatting = TableFormatting::new();
        formatting.alignment = Some(vec![ColumnAlignment::Left, ColumnAlignment::Right]);
        let output = Table::format_table(Some(headers), page.to_vec(), Some(formatting));
        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(0xc16a4f);
            e.author(|a| {
                a.name(format!("{}'s Item Statistics", &pld.msg.author.name));
                a.icon_url(Avatar::url(pld.msg.author.clone()));
                a
            });
            e.description(format!("```hs\n{}\n```", output));
            e.footer(|f| {
                f.text(format!(
                    "[Page {}] {} has found {} out of {} items.",
                    paginator.page, target.name, item_count, total_count,
                ));
                f
            });
            e
        });

        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

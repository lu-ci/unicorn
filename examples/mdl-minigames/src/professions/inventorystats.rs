use crate::professions::allitemstats::AllItemStatsCommand;
use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::item::Item;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use std::cmp::Ordering;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::user::Avatar;

#[derive(Default)]
pub struct InventoryStatsCommand;

impl InventoryStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for InventoryStatsCommand {
    fn command_name(&self) -> &str {
        "inventorystats"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["invstats", "bagstats"]
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Shows the statistics of the your inventory. The number of items per type and per rarity. \
         You can view another person's stats by mentioning them."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = match pld.msg.mentions.get(0) {
            Some(user) => user,
            None => &pld.msg.author,
        };
        let bag = Inventory::get_for_player(&pld.db, u64::from(target.id));
        let items = Item::get_all(&pld.db);

        let mut player_items = Vec::new();

        for inv_item in bag.items {
            for item in &items {
                if item.file_id == inv_item.file_id {
                    player_items.push(item.clone());
                    break;
                }
            }
        }

        //filter out trash
        player_items.sort_by(|a, b| {
            if let Some(order) = a.rarity.partial_cmp(&b.rarity) {
                order.reverse()
            } else {
                Ordering::Equal
            }
        });
        let by_type_output = AllItemStatsCommand::items_by_type(&player_items);
        let by_type_rarity = AllItemStatsCommand::items_by_rarity(&player_items);
        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(0xc16a4f);
            e.author(|a| {
                a.name(&target.name);
                a.icon_url(Avatar::url(target.clone()));
                a
            });
            e.field("Items by Type", format!("```py\n{}\n```", by_type_output), false);
            e.field("Items by Rarity", format!("```py\n{}\n```", by_type_rarity), false);
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

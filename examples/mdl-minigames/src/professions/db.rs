use log::error;
use serde::{de::DeserializeOwned, Serialize};

use unicorn_database::DatabaseHandler;

pub trait DBCollection
    where
        Self: Sized + DeserializeOwned + Serialize + Clone,
{
    fn get_id(&self) -> &str;
    fn get_name(&self) -> &str;

    fn collection_name() -> &'static str;

    fn cache_key() -> String {
        format!("{}:all", Self::collection_name())
    }

    fn item_cache_key(id: &str) -> String {
        format!("{}:{}", Self::collection_name(), id)
    }

    fn get_from_db_by_id(db: &DatabaseHandler, id: &str) -> Option<Self>;
    fn get_all_from_db(db: &DatabaseHandler) -> Vec<Self>;

    fn get_from_cache_by_id(db: &DatabaseHandler, id: &str) -> Option<Self> {
        let body = db.cache.get(Self::item_cache_key(id))?;
        match serde_json::from_str::<Self>(&body) {
            Ok(item) => Some(item),
            Err(why) => {
                error!("Failed desererializing item from cache: {}", why);
                None
            }
        }
    }

    fn get_all_from_cache(db: &DatabaseHandler) -> Option<Vec<Self>> {
        let body = db.cache.get(Self::cache_key())?;
        match serde_json::from_str::<Vec<Self>>(&body) {
            Ok(items) => Some(items),
            Err(why) => {
                error!("Failed desererializing items cache: {}", why);
                None
            }
        }
    }

    fn update_cache(db: &DatabaseHandler, items: Vec<Self>) {
        if !items.is_empty() {
            if let Ok(body) = serde_json::to_string(&items) {
                db.cache.set(Self::cache_key(), body)
            }
        }
    }

    fn cache_item(db: &DatabaseHandler, item: Self) {
        if let Ok(body) = serde_json::to_string(&item) {
            db.cache.set(Self::item_cache_key(item.get_id()), body)
        }
    }

    fn get_all(db: &DatabaseHandler) -> Vec<Self> {
        if let Some(items) = Self::get_all_from_cache(db) {
            items
        } else {
            let items = Self::get_all_from_db(db);
            Self::update_cache(db, items.clone());
            items
        }
    }

    fn get_by_id(db: &DatabaseHandler, id: &str) -> Option<Self> {
        if let Some(item) = Self::get_from_cache_by_id(db, id) {
            Some(item)
        } else {
            let item = Self::get_from_db_by_id(db, id);
            if let Some(found_item) = item {
                Self::cache_item(db, found_item.clone());
                Some(found_item)
            } else {
                None
            }
        }
    }

    fn get_by_name(db: &DatabaseHandler, name: &str) -> Option<Self> {
        for item in Self::get_all(db) {
            if item.get_name().to_lowercase() == name.to_lowercase() {
                return Some(item);
            }
        }
        return None;
    }
}

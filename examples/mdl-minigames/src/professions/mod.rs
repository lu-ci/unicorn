use unicorn_callable::{UnicornCommand, UnicornReaction};

use crate::professions::{
    allitems::AllItemsCommand, allitemstats::AllItemStatsCommand, chances::ChancesCommand, cook::CookCommand, fish::FishCommand,
    forage::ForageCommand, hunt::HuntCommand, inspect::InspectCommand, inventory::InventoryCommand, recipe::{RecipeCommand, RecipesCommand},
    sell::SellCommand, shop::ShopCommand, upgrades::UpgradesCommand, inventorystats::InventoryStatsCommand, itemstats::ItemStatsCommand,
};

mod allitems;
mod allitemstats;
mod chances;
mod cook;
mod db;
mod find_item;
mod fish;
mod forage;
mod hunt;
mod inspect;
mod inventory;
mod inventory_item;
mod inventorystats;
mod item;
mod itemstats;
mod recipe;
mod sell;
mod shop;
mod upgrades;

pub mod yaml;


pub struct ProfessionCommands;

impl ProfessionCommands {
    pub fn commands() -> Vec<Box<dyn UnicornCommand>> {
        vec![
            AllItemsCommand::boxed(),
            AllItemStatsCommand::boxed(),
            ChancesCommand::boxed(),
            CookCommand::boxed(),
            FishCommand::boxed(),
            ForageCommand::boxed(),
            HuntCommand::boxed(),
            InspectCommand::boxed(),
            InventoryCommand::boxed(),
            InventoryStatsCommand::boxed(),
            ItemStatsCommand::boxed(),
            RecipeCommand::boxed(),
            RecipesCommand::boxed(),
            SellCommand::boxed(),
            ShopCommand::boxed(),
            UpgradesCommand::boxed(),
        ]
    }

    pub fn reactions() -> Vec<Box<dyn UnicornReaction>> {
        vec![
            FishCommand::rboxed(),
            ForageCommand::rboxed(),
            HuntCommand::rboxed(),
            InspectCommand::rboxed(),
            SellCommand::rboxed(),
        ]
    }
}

use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;

use crate::professions::chances::Chances;

#[derive(Default)]
pub struct UpgradesCommand {}

impl UpgradesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(UpgradesCommand::default())
    }

    fn calculate_chances(upgrade_level: f64) -> Vec<f64> {
        let mut chance_rows = Vec::new();
        let chances = Chances::create_roll_range(upgrade_level);
        let mut last_chance = 0;
        if let Some(max) = chances.last() {
            for k in 0..10 {
                if let Some(this_chance) = chances.get(k) {
                    if k > 1 && chances.get(k - 1).is_some() {
                        last_chance = *chances.get(k - 1).unwrap()
                    }
                    let size = (this_chance - last_chance) as f64;
                    let chance = size / *max as f64;
                    chance_rows.push(chance);
                }
            }
        }
        chance_rows
    }

    fn calculate_luck_chance(upgrade_level: f64) -> i32 {
        let new = Self::calculate_chances(upgrade_level);
        let old = Self::calculate_chances(0.0);
        let mut all = Vec::new();
        for i in 1..new.len() {
            if let Some(better) = new.get(i) {
                if let Some(worse) = old.get(i) {
                    all.push(better / worse);
                }
            }
        }
        let mut sum = 0.0;
        let mut count = 0.0;
        for i in all {
            sum += i;
            count += 1.0;
        }
        (((sum / count) - 1.0) * 100.0) as i32
    }

    fn calculate_upgrade(upgrade: &str, level: f64) -> String {
        match upgrade {
            "stamina" => format!(
                "**Level {}** Endurance Training: **{} Seconds**",
                level,
                -(60 - ((60.0 - ((60.0 / 100.0) * ((level * 0.5) / (1.25 + (0.01 * level))))) as i32))
            ),
            "luck" => format!(
                "**Level {}** Lucky Enchantment: **{} % Luckier**",
                level,
                Self::calculate_luck_chance(level)
            ),
            "storage" => format!("**Level {}** Inventory Expansion: **{} Spaces**", level, 64 + (level as i32 * 8)),
            "oven" => format!(
                "**Level {}** Super Heated Oven: **{} Seconds**",
                level,
                -(3600 - ((3600.0 - ((3600.0 / 100.0) * (level / (1.25 + (0.01 * level))))) as i32))
            ),
            "casino" => format!(
                "**Level {}** Casino Investment: **{} Seconds**",
                level,
                -(60 - ((60.0 - ((60.0 / 100.0) * ((level * 0.5) / (1.25 + (0.01 * level))))) as i32))
            ),
            "harem" => format!("**Level {}** Harem Expansion: **{} Wives**", level, 10 + level as i32),
            _ => format!("{} is not an upgrade", upgrade),
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for UpgradesCommand {
    fn command_name(&self) -> &str {
        "upgrades"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Shows a table with your item chance statistics."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("level", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = match pld.msg.mentions.get(0) {
            Some(user) => user,
            None => &pld.msg.author,
        };

        let profile = pld.db.get_profile(target.id.into());
        let mut upgrade_text = String::new();
        for upgrade in vec!["stamina", "luck", "storage", "oven", "casino", "harem"] {
            let mut upgrade_level = profile.get_upgrade("luck");
            if pld.cfg.discord.owners.contains(&u64::from(pld.msg.author.id)) {
                let requested_level = pld.args.get_or_default("level", -1);
                if requested_level >= 0 {
                    upgrade_level = requested_level;
                }
            }
            let calculated_upgrade = Self::calculate_upgrade(upgrade, upgrade_level as f64);
            upgrade_text += &format!("\n{}", calculated_upgrade);
        }

        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0xF9F9F9);
            e.title(format!("🛍 {}\'s Sigma Upgrades", target.name));
            e.description(upgrade_text)
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut msg).await?;
        Ok(())
    }
}

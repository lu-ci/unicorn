use std::cmp::Ordering;
use std::collections::HashMap;

use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::user::User;

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::table::{ColumnAlignment, Table, TableFormatting};
use unicorn_utility::user::Avatar;

use crate::professions::db::DBCollection;
use crate::professions::item::Item;

#[derive(Default)]
pub struct AllItemStatsCommand;

impl AllItemStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    pub fn items_by_type(all_items: &Vec<Item>) -> String {
        let mut groups_by_type = HashMap::new();
        for item in all_items {
            if item.rarity >= 0 {
                match groups_by_type.get_mut(&item.item_type) {
                    Some(c) => *c += 1,
                    None => {
                        groups_by_type.insert(&item.item_type, 1);
                    }
                }
            }
        }

        let mut items_by_type = Vec::new();
        let item_type_order = vec!["Fish", "Plant", "Animal", "Meal", "Drink", "Dessert"];
        for group in item_type_order {
            let group_entry = groups_by_type.get(&group.to_string());
            if let Some(group_value) = group_entry {
                items_by_type.push(vec![group.to_string(), group_value.to_string()]);
            }
        }
        let mut by_type_formatting = TableFormatting::new();
        by_type_formatting.alignment = Some(vec![ColumnAlignment::Left, ColumnAlignment::Right]);

        Table::format_table(None, items_by_type, Some(by_type_formatting))
    }

    pub fn items_by_rarity(all_items: &Vec<Item>) -> String {
        let mut groups_by_rarity = HashMap::<i32, Vec<&Item>>::new();
        for item in all_items {
            if item.rarity >= 0 && item.rarity < 10 {
                match groups_by_rarity.get_mut(&item.rarity) {
                    Some(v) => v.push(item),
                    None => {
                        groups_by_rarity.insert(item.rarity, vec![item]);
                    }
                }
            }
        }

        let mut items_by_rarity = Vec::new();
        for rarity in 0..10 {
            let mut rarity_by_type = HashMap::new();
            let mut sum = 0;
            if let Some(group) = groups_by_rarity.get(&rarity) {
                for item in group {
                    match rarity_by_type.get_mut(&item.item_type) {
                        Some(c) => *c += 1,
                        None => {
                            rarity_by_type.insert(&item.item_type, 1);
                        }
                    }
                    sum += 1;
                }
                let mut row = Vec::new();
                row.push(Item::get_rarity_name(rarity).to_uppercase());
                let types = vec!["Animal", "Fish", "Plant"];
                for item_type in types {
                    if let Some(count) = rarity_by_type.get::<String>(&item_type.to_string()) {
                        row.push(count.to_string());
                    }
                }
                row.push(sum.to_string());
                items_by_rarity.push(row);
            }
        }
        let mut by_rarity_formatting = TableFormatting::new();
        by_rarity_formatting.alignment = Some(vec![
            ColumnAlignment::Left,
            ColumnAlignment::Right,
            ColumnAlignment::Right,
            ColumnAlignment::Right,
            ColumnAlignment::Right,
        ]);
        let headers = vec!["Rarity", "Animals", "Fish", "Plants", "Total"];
        Table::format_table(Some(headers), items_by_rarity, Some(by_rarity_formatting))
    }
}

#[serenity::async_trait]
impl UnicornCommand for AllItemStatsCommand {
    fn command_name(&self) -> &str {
        "allitemstats"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Shows the statistics of the entire item pool. The number of items per type and per rarity."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut all_items = Item::get_all(&pld.db);
        //filter out trash
        all_items.sort_by(|a, b| {
            if let Some(order) = a.rarity.partial_cmp(&b.rarity) {
                order.reverse()
            } else {
                Ordering::Equal
            }
        });
        let by_type_output = AllItemStatsCommand::items_by_type(&all_items);
        let by_type_rarity = AllItemStatsCommand::items_by_rarity(&all_items);
        let bot = pld.ctx.cache.current_user().await;
        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(0xc16a4f);
            e.author(|a| {
                a.name("All Item Statistics");
                a.icon_url(Avatar::url(User::from(bot.clone())));
                a
            });
            e.field("Items by Type", format!("```py\n{}\n```", by_type_output), false);
            e.field("Items by Rarity", format!("```py\n{}\n```", by_type_rarity), false);
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

use bson::*;
use log::error;
use serde::{Deserialize, Serialize};

use unicorn_database::DatabaseHandler;

use crate::professions::db::DBCollection;
use crate::professions::recipe::Recipe;
use crate::professions::yaml::YamlItem;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Item {
    pub name: String,
    pub icon: String,
    pub color: u64,
    pub item_type: String,
    pub description: String,
    pub rarity: i32,
    pub value: i32,
    pub file_id: String,
}

impl Item {
    pub fn rarity(&self) -> String {
        Item::get_rarity_name(self.rarity)
    }

    pub fn get_rarity_name(rarity: i32) -> String {
        match rarity {
            -1 => "trash",
            0 => "common",
            1 => "uncommon",
            2 => "rare",
            3 => "legendary",
            4 => "prime",
            5 => "spectral",
            6 => "ethereal",
            7 => "antimatter",
            8 => "omnipotent",
            9 => "material",
            10 => "delicacy",
            _ => "unknown",
        }
        .to_string()
    }

    pub fn icons(&self) -> Vec<String> {
        Item::icon_for_type(&self.item_type.clone())
    }

    fn icon_for_type(item_type: &String) -> Vec<String> {
        match item_type.to_lowercase().as_str() {
            "fish" => vec!["🦐", "🐟", "🐡", "🦑", "🐠", "👻", "🏵", "✴", "🔰"]
                .iter()
                .map(|&s| s.into())
                .collect(),
            "plant" => vec!["☘", "🌾", "🌻", "🌺", "🌹", "🎃", "🍥", "☄", "🏮"]
                .iter()
                .map(|&s| s.into())
                .collect(),
            "animal" => vec!["🐓", "🐖", "🐇", "🐄", "🐏", "🐍", "🐉", "🦈", "🦄"]
                .iter()
                .map(|&s| s.into())
                .collect(),
            _ => Vec::new(),
        }
    }

    fn colours_for_type(item_type: &String) -> Vec<u64> {
        match item_type.to_lowercase().as_str() {
            "fish" => vec![0xc16a4f, 0x55acee, 0xd99e82, 0xea596e, 0xffcc4d, 0xe1e8ed, 0x553788, 0x292f33, 0x47ded4],
            "plant" => vec![0x77b255, 0xffcc4d, 0x732700, 0xea596e, 0xbe1931, 0xf18f26, 0xd372b8, 0xbdddf4, 0xbe1931],
            "animal" => vec![0xe1e8ed, 0xf4abba, 0x99aab5, 0x8e969d, 0x99aab5, 0x77b255, 0x3e721d, 0x66757f, 0x9266cc],
            _ => Vec::new(),
        }
    }

    fn value(rarity: i32) -> i32 {
        match rarity {
            -1 => 0,
            0 => 25,
            1 => 40,
            2 => 70,
            3 => 250,
            4 => 650,
            5 => 1000,
            6 => 2100,
            7 => 3500,
            8 => 10000,
            _ => 0,
        }
    }

    pub fn get_statistics_for_user(db: &DatabaseHandler, uid: u64) -> Vec<(String, i32)> {
        let mut result = Vec::new();
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"user_id": uid};
            match cli.database(&db.cfg.database.name).collection("item_statistics").find_one(lookup, None) {
                Ok(stats) => {
                    if let Some(player_stats) = stats {
                        for stat in player_stats {
                            let (name, value) = stat;
                            if let Some(count) = value.as_i32() {
                                result.push((name, count));
                            }
                        }
                    }
                }
                Err(why) => error!("Can't get stats for user {}: {}", uid, why),
            }
        }
        result
    }

    pub fn get_statistics_for_item(db: &DatabaseHandler, item: &Item) -> i32 {
        let mut result = 0;
        if let Some(cli) = db.get_client() {
            let lookup = doc! {&item.file_id: {"$exists": true}};
            match cli.database(&db.cfg.database.name).collection("item_statistics").find(lookup, None) {                
                Ok(cursor) => {
                    for stats in cursor {
                        if let Ok(player_stats) = stats {
                            if let Some(count) = player_stats.get(&item.file_id) {
                                if let Some(value) = count.as_i32() {
                                    result += value;
                                }
                            }
                        }
                    }
                }
                Err(why) => error!("Can't get stats for item {}: {}", &item.file_id, why),
            }
        }
        result
    }

    pub fn add_statistic(db: &DatabaseHandler, item: Item, uid: u64) {
        if let Some(cli) = db.get_client() {
            let lookup = doc! {"user_id": uid};
            match cli
                .database(&db.cfg.database.name)
                .collection("item_statistics")
                .find_one(lookup.clone(), None)
            {
                Ok(stats) => {
                    let mut item_count = 0;
                    match stats {
                        Some(stat) => {
                            if let Some(count) = stat.get(item.file_id.as_str()) {
                                if let Some(value) = count.as_i32() {
                                    item_count = value;
                                }
                            }
                        }
                        None => {
                            let _ = cli
                                .database(&db.cfg.database.name)
                                .collection("item_statistics")
                                .insert_one(lookup.clone(), None);
                        }
                    }
                    item_count += 1;
                    let update = doc! {"$set": {item.file_id: item_count}};
                    let _ = cli
                        .database(&db.cfg.database.name)
                        .collection("item_statistics")
                        .update_one(lookup.clone(), update, None);
                }
                Err(why) => error!("Could not update inventory: {}", why),
            };
        }
    }

    pub fn calculate_value(&self, db: &DatabaseHandler) -> i32 {
        if self.rarity < 10 {
            Item::value(self.rarity)
        } else {
            if let Some(recipe) = Recipe::get_by_id(db, self.get_id()) {
                let mut value = 0.0;
                let mut rarity = 0.0;
                for ingredient in recipe.ingredients {
                    if let Some(item) = Item::get_by_id(db, ingredient.as_str()) {
                        value += item.calculate_value(db) as f64;
                        rarity += (item.rarity + 1) as f64;
                    }
                }
                std::cmp::max(100, (value * (3.0 * (0.075 * rarity)) / 100.0) as i32 * 100)
            } else {
                0
            }
        }
    }
}

impl DBCollection for Item {
    fn get_id(&self) -> &str {
        self.file_id.as_str()
    }

    fn get_name(&self) -> &str {
        self.name.as_str()
    }

    fn collection_name() -> &'static str {
        "items"
    }
    fn get_from_db_by_id(db: &DatabaseHandler, id: &str) -> Option<Item> {
        let mut result = None;
        if let Some(cli) = db.get_client() {
            if let Ok(db_result) = cli
                .database(&db.cfg.database.name)
                .collection("items")
                .find_one(doc! {"file_id": id}, None)
            {
                if let Some(doc) = db_result {
                    if let Ok(yaml_item) = bson::from_bson::<YamlItem>(Bson::from(doc)) {
                        result = Some(Item::from(yaml_item));
                    }
                }
            }
        }
        result
    }

    fn get_all_from_db(db: &DatabaseHandler) -> Vec<Item> {
        let mut items = Vec::<Item>::new();
        if let Some(cli) = db.get_client() {
            if let Ok(cursor) = cli.database(&db.cfg.database.name).collection("items").find(None, None) {
                for entry in cursor {
                    if let Ok(doc) = entry {
                        match bson::from_bson::<YamlItem>(bson::Bson::Document(doc)) {
                            Ok(yaml_item) => {
                                let mut item = Item::from(yaml_item);
                                if item.rarity == 10 {
                                    item.value = item.calculate_value(db);
                                    item.icon = Recipe::cook_icons(&item.file_id).to_string();
                                    item.color = Recipe::cook_colors(&item.file_id);
                                }
                                items.push(item);
                            }
                            Err(why) => {
                                let reason = format!("{}", why);
                                error!("{}", reason);
                            }
                        }
                    }
                }
            }
        }
        items
    }
}

impl From<YamlItem> for Item {
    fn from(yaml: YamlItem) -> Self {
        let rarity = match yaml.rarity {
            Some(rar) => rar - 1,
            None => 10,
        };
        let item_type = yaml.item_type;
        let icons = Item::icon_for_type(&item_type);
        let icon_maybe = icons.get(rarity as usize);
        let icon = match icon_maybe {
            Some(i) => i.clone(),
            None => "🗑".to_string(),
        };
        let colors = Item::colours_for_type(&item_type);
        let color = colors.get(rarity as usize).unwrap_or(&0x67757f);
        let value = Item::value(rarity);
        Self {
            name: yaml.name,
            item_type: item_type.clone(),
            description: yaml.description,
            file_id: yaml.file_id,
            rarity,
            icon: icon.to_string(),
            color: *color,
            value: value,
        }
    }
}

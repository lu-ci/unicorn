use log::error;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_resource::origin::ResourceOrigin;
use unicorn_utility::string::UnicornString;

use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::inventory_item::InventoryItem;
use crate::professions::item::Item;
use crate::professions::recipe::Recipe;

#[derive(Default)]
pub struct CookCommand {}

impl CookCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(CookCommand::default())
    }
}

#[serenity::async_trait]
impl UnicornCommand for CookCommand {
    fn command_name(&self) -> &str {
        "cook"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["make"]
    }

    fn example(&self) -> &str {
        "Shade Tea"
    }

    fn description(&self) -> &str {
        "Uses a recipe to create an item from raw resources that you've gathered. \
         You can see all available recipes with the recipes command."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("recipe", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut message = if pld.args.satisfied() {
            if let Some(recipe) = Recipe::get_by_name(&pld.db, pld.args.get("recipe")) {
                let mut used_ingredients = Vec::new();
                let mut missing_ingredients = Vec::new();
                let inventory = Inventory::get_for_player(&pld.db, u64::from(pld.msg.author.id));
                let mut recipe_complete = true;
                for ingredient in recipe.ingredients {
                    let mut found_item = false;
                    for item in &inventory.items {
                        if item.file_id == ingredient {
                            used_ingredients.push(item.clone());
                            found_item = true;
                            break;
                        }
                    }
                    if !found_item {
                        recipe_complete = false;
                        if let Some(missing_item) = Item::get_by_id(&pld.db, &ingredient) {
                            missing_ingredients.push(missing_item);
                        } else {
                            error!("Recipe ingredient {} doesn't exist!", ingredient);
                        }
                    }
                }
                if recipe_complete {
                    if let Some(item) = Item::get_by_id(&pld.db, &recipe.file_id) {
                        let mut item_data = InventoryItem::create_from_item(&item);
                        let pid = u64::from(pld.msg.author.id);
                        Item::add_statistic(&pld.db, item.clone(), pid);
                        pld.db
                            .add_resource("items", pid, 1, "cook", ResourceOrigin::from(pld.msg.author.id), true);
                        for item in used_ingredients {
                            if item.transferred {
                                item_data.transferred = true;
                            }
                            Inventory::del_item(&pld.db, pid, item);
                        }
                        Inventory::add_item(&pld.db, pid, item_data.clone());

                        let quality = Recipe::cook_quality(item_data.quality);
                        let connector = UnicornString::connector(quality);
                        UnicornEmbed::small_embed(&recipe.icon, recipe.color, format!("You made {} {} {}", connector, quality, recipe.name))
                    } else {
                        UnicornEmbed::not_found(format!("The item of the recipe for {} was not found", pld.args.get("recipe")))
                    }
                } else {
                    UnicornEmbed::not_found(format!(
                        "You're missing the following required ingredients: {}",
                        missing_ingredients.into_iter().map(|i| i.name).collect::<Vec<String>>().join(", ")
                    ))
                }
            } else {
                UnicornEmbed::not_found(format!("The recipe for {} was not found", pld.args.get("recipe")))
            }
        } else {
            UnicornEmbed::not_found("Make what?")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut message).await?;
        Ok(())
    }
}

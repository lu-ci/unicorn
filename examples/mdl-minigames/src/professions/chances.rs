use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::table::Table;
use unicorn_utility::user::Avatar;

use crate::professions::item::Item;

pub struct Chances {}

impl Chances {
    pub fn create_roll_range(luck: f64) -> Vec<u64> {
        let chances = vec![32.0, 26.00, 20.00, 15.00, 5.000, 2.000, 1.100, 0.500, 0.300, 0.100];
        let modifiers = vec![0.1600, 0.1300, 0.1000, 0.0750, 0.0250, 0.0100, 0.0055, 0.0025, 0.0015, 0.0005];
        let mut rarities = Vec::new();
        let mut max_boundary = 0;
        for (index, chance) in chances.iter().enumerate() {
            let modified_chance;
            if let Some(modifier) = modifiers.get(index) {
                if index == 0 {
                    modified_chance = (chance - ((luck * modifier) / (1.5 + (0.005 * luck)))) / 100.0
                } else {
                    modified_chance = (chance + ((luck * modifier) / (1.5 + (0.005 * luck)))) / 100.0
                }
                let roll_boundary = (999999999999f64 * modified_chance) as u64;
                max_boundary += roll_boundary;
                rarities.push(max_boundary);
            }
        }
        rarities
    }
}

#[derive(Default)]
pub struct ChancesCommand {}

impl ChancesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(ChancesCommand::default())
    }
}

#[serenity::async_trait]
impl UnicornCommand for ChancesCommand {
    fn command_name(&self) -> &str {
        "chances"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Shows a table with your item chance statistics."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("level", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = match pld.msg.mentions.get(0) {
            Some(user) => user,
            None => &pld.msg.author,
        };
        let profile = pld.db.get_profile(target.id.into());
        let mut upgrade_level = profile.get_upgrade("luck");
        if pld.cfg.discord.owners.contains(&u64::from(pld.msg.author.id)) {
            let requested_level = pld.args.get_or_default("level", -1);
            if requested_level >= 0 {
                upgrade_level = requested_level;
            }
        }

        let header = vec!["Rarity", "Chance"];

        let mut chance_rows = Vec::new();
        let chances = Chances::create_roll_range(upgrade_level as f64);
        let mut last_chance = 0;
        if let Some(max) = chances.last() {
            for k in 0..10 {
                if let Some(this_chance) = chances.get(k) {
                    if k > 1 && chances.get(k - 1).is_some() {
                        last_chance = *chances.get(k - 1).unwrap()
                    }
                    let size = (this_chance - last_chance) as f64;
                    let chance = size / *max as f64;
                    chance_rows.push(vec![Item::get_rarity_name(k as i32 - 1), format!("{}%", chance)])
                }
            }
        }

        let table = Table::format_table(Some(header), chance_rows, None);
        let mut msg = CreateMessage::default();
        msg.embed(|e| {
            e.color(0x1b6f5f);
            e.author(|a| {
                a.name(&target.name);
                a.icon_url(Avatar::url(target.clone()));
                a
            });
            e.field("Luck", format!("Your Luck is level {}", upgrade_level), false);
            e.field("Chances", format!("```bat\n{}\n```", table), false);
            e
        });
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut msg).await?;
        Ok(())
    }
}

use log::error;
use rand::Rng;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;
use serenity::model::channel::ReactionType;

use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_database::profile::UserProfile;
use unicorn_database::DatabaseHandler;
use unicorn_embed::UnicornEmbed;
use unicorn_resource::origin::ResourceOrigin;
use unicorn_utility::user::Avatar;

use crate::professions::chances::Chances;
use crate::professions::db::DBCollection;
use crate::professions::inventory::Inventory;
use crate::professions::inventory_item::InventoryItem;
use crate::professions::item::Item;

pub struct FindItem {
    pub command_name: String,
    pub item_type: String,
    pub empty_is_trash: bool,
    pub cooldown_message: Box<dyn Fn(i32) -> String + Send + Sync>,
    pub trash_message: Box<dyn Fn(Item) -> String + Send + Sync>,
    pub search_message: Box<dyn Fn(Item) -> String + Send + Sync>,
    pub no_items_message: Box<dyn Fn() -> String + Send + Sync>,
    pub timeout_message: Box<dyn Fn(Item) -> String + Send + Sync>,
    pub success_message: Box<dyn Fn(Item) -> String + Send + Sync>,
    pub failure_message: Box<dyn Fn(Item) -> String + Send + Sync>,
}

impl FindItem {
    pub fn new() -> Self {
        Self {
            command_name: "unitialised".to_owned(),
            item_type: "unknown".to_owned(),
            empty_is_trash: false,
            cooldown_message: Box::new(|x| format!("{}", x)),
            trash_message: Box::new(|i| format!("{}", i.name)),
            search_message: Box::new(|i| format!("{}", i.name)),
            no_items_message: Box::new(|| "Nothing here".to_owned()),
            timeout_message: Box::new(|_| "Too slow".to_owned()),
            success_message: Box::new(|_| "Well done".to_owned()),
            failure_message: Box::new(|_| "Better luck next time".to_owned()),
        }
    }

    fn set_cooldown(&self, pld: &mut CommandPayload, key: String, profile: &UserProfile) {
        let base_cooldown = 60 as f32;
        let stamina = profile.get_upgrade("stamina") as f32;
        let mut cooldown = (base_cooldown - ((base_cooldown / 100.0) * ((stamina * 0.5) / (1.25 + (0.01 * stamina))))) as i32;
        if cooldown < 5 {
            cooldown = 5;
        }
        pld.cd.set_cooldown(key, u64::from(pld.msg.author.id), cooldown);
    }

    fn roll_rarity(&self, profile: &UserProfile) -> i32 {
        let mut rng = rand::thread_rng();

        let luck = profile.get_upgrade("luck") as f64;

        let rarities = Chances::create_roll_range(luck);
        if let Some(max) = rarities.last() {
            let roll = rng.gen_range(0u64, max);

            let mut lowest = 0;
            for rarity in rarities {
                if rarity < roll {
                    lowest += 1;
                } else {
                    break;
                }
            }
            lowest
        } else {
            0
        }
    }

    fn pick_item_in_rarity(&self, db: &DatabaseHandler, item_type: &str, rarity: i32) -> Option<Item> {
        let mut in_rarity = Vec::new();
        for item in Item::get_all(db) {
            if item.item_type.to_lowercase() == item_type {
                if item.rarity == rarity - 1 {
                    in_rarity.push(item);
                }
            }
        }
        if in_rarity.len() > 0 {
            let index = rand::thread_rng().gen_range(0, in_rarity.len());
            Some(in_rarity[index].clone())
        } else {
            error!("No items of type {} found for rarity: {}", item_type, rarity);
            None
        }
    }

    pub async fn find_item(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let key = format!("{}:{}", self.command_name, pld.msg.author.id);
        let cdi = pld.cd.get_cooldown(key.clone());
        if cdi.active {
            let mut response = UnicornEmbed::small_embed("🕙", 0x696969, (self.cooldown_message)(cdi.duration));
            pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
            return Ok(());
        }

        let profile = &pld.db.get_profile(u64::from(pld.msg.author.id));

        self.set_cooldown(pld, key, profile);

        let storage = profile.get_upgrade("storage");
        let inv_limit = 64 + (8 * storage) as usize;
        let inventory = Inventory::get_for_player(&pld.db, u64::from(pld.msg.author.id));
        if inventory.items.len() < inv_limit {
            let mut rarity = self.roll_rarity(profile);
            if pld.args.has_argument("rarity") && pld.cfg.discord.owners.contains(&u64::from(pld.msg.author.id)) {
                rarity = pld.args.get_or_default("rarity", rarity);
            }

            if let Some(item) = self.pick_item_in_rarity(&pld.db, self.item_type.as_str(), rarity) {
                if rarity == 0 {
                    let mut response = UnicornEmbed::small_embed("🗑", 0x67757f, (self.trash_message)(item));
                    pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
                } else {
                    let mut icon_list = Vec::new();
                    for icon in item.icons() {
                        if icon != item.icon {
                            icon_list.push(icon);
                        }
                    }
                    let mut possible_proto = vec![item.icon.clone()];
                    let mut option_count = rand::thread_rng().gen_range(3, 5) as i32;
                    while option_count >= 0 {
                        if let Some(icon_option) = icon_list.get(rand::thread_rng().gen_range(0, icon_list.len())) {
                            possible_proto.push(icon_option.clone().to_string());
                        }
                        option_count -= 1;
                    }

                    let mut response = CreateMessage::default();
                    response.embed(|e| {
                        e.color(item.clone().color);
                        e.title((self.search_message)(item.clone()));
                        e.author(|a| {
                            a.name(pld.msg.author.name.clone());
                            a.icon_url(Avatar::url(pld.msg.author.clone()));
                            a
                        });
                        e
                    });

                    let mut options = Vec::new();
                    while possible_proto.len() > 0 {
                        options.push(possible_proto.remove(rand::thread_rng().gen_range(0, possible_proto.len())));
                    }

                    if let Ok(message) = pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await {
                        let cd_key = format!("user{}:message{}:{}", u64::from(pld.msg.author.id), u64::from(message.id), self.item_type);
                        pld.cache.set(cd_key.clone(), format!("{}:{}", item.file_id, options.join(",")));

                        let http = pld.ctx.http.clone();
                        let message_text = (self.timeout_message)(item);
                        let channel = pld.msg.channel_id.clone();

                        for option in options {
                            message.react(pld.ctx.http(), ReactionType::Unicode(option.to_owned())).await?;
                        }

                        pld.timeouts.register(&cd_key, 30, move || {
                            let mut response = UnicornEmbed::small_embed("🕙", 0x696969, message_text.clone());
                            let _ = message.delete(http.clone());
                            let _ = channel.send_message(http.clone(), |_| &mut response);
                        });
                    }
                }
            } else {
                if self.empty_is_trash {
                    let mut response = UnicornEmbed::small_embed("🗑", 0x67757f, (self.trash_message)(Item::default()));
                    pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
                } else {
                    let mut response = UnicornEmbed::error((self.no_items_message)());
                    pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
                }
            }
        } else {
            let mut response = UnicornEmbed::error("Your inventory is full.");
            pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        }
        Ok(())
    }

    pub async fn check_reaction(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        if let ReactionType::Unicode(emoji) = pld.reaction.emoji.clone() {
            let cd_key = format!(
                "user{}:message{}:{}",
                u64::from(pld.reaction.user_id),
                u64::from(pld.reaction.message_id),
                self.item_type
            );
            if pld.timeouts.cancel(&cd_key) {
                if let Some(active_find) = pld.cache.get(cd_key.clone()) {
                    let mut split = active_find.split(":");
                    if let Some(target) = split.next() {
                        if let Some(options) = split.next() {
                            for option in options.split(",") {
                                if option == emoji {
                                    pld.cache.del(cd_key.clone());
                                    if let Some(target_item) = Item::get_by_id(&pld.db, target) {
                                        let target_emoji = target_item.icon.clone();
                                        if target_emoji == emoji {
                                            let data_for_inv = InventoryItem::create_from_item(&target_item);
                                            Inventory::add_item(&pld.db, u64::from(pld.reaction.user_id), data_for_inv.clone());
                                            if let Ok(author) = pld.reaction.user(pld.ctx.http()).await {
                                                Item::add_statistic(&pld.db, target_item.clone(), u64::from(author.id));
                                                pld.db.add_resource(
                                                    "items",
                                                    u64::from(pld.reaction.user_id),
                                                    1,
                                                    self.item_type.as_str(),
                                                    ResourceOrigin::from(pld.reaction.user_id),
                                                    true,
                                                );
                                                let mut response = CreateMessage::default();
                                                let _ = pld.reaction.channel_id.delete_message(pld.ctx.http(), pld.reaction.message_id);
                                                response.embed(|e| {
                                                    e.color(target_item.color);
                                                    e.title((self.success_message)(target_item));
                                                    e.author(|a| {
                                                        a.name(author.name.clone());
                                                        a.icon_url(Avatar::url(author.clone()));
                                                        a
                                                    });
                                                    e.footer(|f| {
                                                        f.text(format!("item id: {}", data_for_inv.item_id));
                                                        f
                                                    });
                                                    e
                                                });
                                                if let Ok(message) = pld.reaction.channel_id.send_message(pld.ctx.http(), |_| &mut response).await {
                                                    message.react(pld.ctx.http(), ReactionType::Unicode("💸".into())).await?;
                                                    message.react(pld.ctx.http(), ReactionType::Unicode("🔍".into())).await?;
                                                }
                                            }
                                        } else {
                                            let mut response = UnicornEmbed::small_embed("❌", 0xBE1931, (self.failure_message)(target_item));
                                            pld.reaction.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }
}

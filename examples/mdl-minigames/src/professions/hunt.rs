use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, ReactionPayload};
use unicorn_callable::reaction::UnicornReaction;
use unicorn_utility::string::UnicornString;

use crate::professions::find_item::FindItem;

pub struct HuntCommand {
    item_finder: FindItem,
}

impl HuntCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(HuntCommand::new())
    }
    pub fn rboxed() -> Box<dyn UnicornReaction> {
        Box::new(HuntCommand::new())
    }

    pub fn new() -> Self {
        let mut item_finder = FindItem::new();
        item_finder.item_type = "animal".to_owned();
        item_finder.command_name = "hunt".to_owned();
        item_finder.empty_is_trash = true;
        item_finder.cooldown_message = Box::new(|x| format!("You are resting for another {} seconds.", x));
        item_finder.trash_message = Box::new(|_| "You hunted for a while but found nothing...".to_owned());
        item_finder.search_message = Box::new(|i| format!("{} Quick! Get the correct {}", i.icon.as_str(), i.item_type));
        item_finder.no_items_message = Box::new(|| "There are no animals here".to_owned());
        item_finder.timeout_message = Box::new(|i| format!("Oh no... The {} {} escaped...", i.rarity(), i.item_type));
        item_finder.success_message = Box::new(|i| format!("You caught {} {} {}", UnicornString::connector(i.rarity().as_str()), i.rarity(), i.name));
        item_finder.failure_message = Box::new(|_| "Oh no... The feisty little thing slipped out of your grasp...".to_owned());
        Self { item_finder }
    }
}

#[serenity::async_trait]
impl UnicornReaction for HuntCommand {
    async fn execute(&self, pld: &mut ReactionPayload) -> Result<(), CallableError> {
        self.item_finder.check_reaction(pld).await
    }

    fn emojis(&self) -> Option<Vec<&str>> {
        None
    }
}

#[serenity::async_trait]
impl UnicornCommand for HuntCommand {
    fn command_name(&self) -> &str {
        "hunt"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn description(&self) -> &str {
        "Go into the wilderness and hunt for game. You can hunt once every 60 seconds, everyone needs rest."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("rarity", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        self.item_finder.find_item(pld).await?;
        Ok(())
    }
}

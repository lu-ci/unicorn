use serenity::http::CacheHttp;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_resource::origin::ResourceOrigin;

#[derive(Default)]
pub struct GenerateItemCommand;

impl GenerateItemCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GenerateItemCommand {
    fn command_name(&self) -> &str {
        "generateitem"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["generateres", "genres", "genr"]
    }

    fn category(&self) -> &str {
        "owner"
    }

    fn owner(&self) -> bool {
        true
    }

    fn example(&self) -> &str {
        "5000 currency @person"
    }

    fn description(&self) -> &str {
        "Awards the mentioned user with the specified amount of the specified resource. \
        The resource type goes first and then the user mention, \
        and then the amount as shown in the example.
        (Bot Owner Only)"
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("amount", false), CommandArgument::new("resource", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.satisfied() {
            if let Ok(num) = pld.args.get("amount").parse::<i64>() {
                let amount = num;
                let mut resource = pld.args.get("resource");
                match pld.msg.mentions.len() {
                    0 => UnicornEmbed::error(format!("Give {} {} to who?", amount, resource)),
                    _ => {
                        let mut accepted = Vec::new();
                        let mut rejected = Vec::new();
                        for target in &pld.msg.mentions {
                            if !target.bot {
                                let currency_name = &pld.cfg.preferences.currency.common.name.single;
                                if resource.to_lowercase() == currency_name.to_lowercase() {
                                    resource = "currency";
                                }
                                pld.db.add_resource(
                                    resource,
                                    u64::from(target.id),
                                    amount,
                                    self.command_name(),
                                    ResourceOrigin::from(pld.msg.author.id),
                                    false,
                                );
                                accepted.push(target.name.clone());
                            } else {
                                rejected.push(format!("{} (because they're a bot) ", target.name));
                            };
                        }
                        let message = if accepted.len() > 0 {
                            UnicornEmbed::ok(format!(
                                "Gave {} {} to {} {}",
                                amount,
                                resource,
                                match accepted.len() {
                                    1 => accepted[0].clone(),
                                    _ => format!("{} and {}", accepted[..accepted.len() - 2].join(", "), accepted[accepted.len() - 1]),
                                },
                                match rejected.len() {
                                    0 => "".to_owned(),
                                    1 => format!(" but not {}", rejected[0]),
                                    _ => format!(
                                        " but not {} and {}",
                                        accepted[..accepted.len() - 2].join(", "),
                                        accepted[accepted.len() - 1]
                                    ),
                                }
                            ))
                        } else {
                            UnicornEmbed::error(format!(
                                "Couldn't give {} {} to {}",
                                amount,
                                resource,
                                match rejected.len() {
                                    0 => "".to_owned(),
                                    1 => format!(" {}", rejected[0]),
                                    _ => format!(" {} or {}", accepted[..accepted.len() - 2].join(", "), accepted[accepted.len() - 1]),
                                }
                            ))
                        };
                        message
                    }
                }
            } else {
                UnicornEmbed::error("Invalid amount")
            }
        } else {
            UnicornEmbed::error("Resource name, amount and target needed.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

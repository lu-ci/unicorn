use unicorn_callable::UnicornCommand;

mod connect4;
mod gameboard;
mod minmax;
pub struct AICommands;

impl AICommands {
    pub fn commands() -> Vec<Box<dyn UnicornCommand>> {
        vec![connect4::Connect4Command::boxed()]
    }
}

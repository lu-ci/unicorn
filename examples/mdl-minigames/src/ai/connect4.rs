use crate::ai::gameboard::GameBoard;
use serde::{Deserialize, Serialize};
use serenity::http::CacheHttp;
use unicorn_cache::handler::CacheHandler;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::dialogue::{Dialogue, DialogResult};
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::{CommandPayload, };

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Connect4Board {
    pub player_positions: u64,
    pub all_positions: u64,
    pub single_player: bool,
}

impl Connect4Board {
    pub fn new(single_player: bool, all_positions: u64, player_positions: u64) -> Self {
        Self {
            single_player,
            all_positions,
            player_positions,
        }
    }
}

#[derive(Default)]
pub struct Connect4Command {

}

impl Connect4Command {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Connect4Command::default())
    }

    fn connected_four(player_positions: u64) -> bool {
        let mut m = player_positions & (player_positions >> 8);
        if (m & (m >> 16)) > 0 {
            true
        } else {
            m = player_positions & (player_positions >> 7);
            if (m & (m >> 14)) > 0 {
                true
            } else {
                m = player_positions & (player_positions >> 9);
                if (m & (m >> 18)) > 0 {
                    return true;
                } else {
                    m = player_positions & (player_positions >> 1);
                    if (m & (m >> 2)) > 0 {
                        true
                    } else {
                        false
                    }
                }
            }
        }
    }

    fn render_board(board: &Connect4Board) -> String {
        let mut output = String::new();
        for y in (0..6).rev() {
            for x in 0..7 {
                let offset = x * 8 + y;
                if 1 << offset & board.player_positions > 0 {
                    output += "🔵 ";
                } else if 1 << offset & board.all_positions > 0 {
                    output += "🔴 ";
                } else {
                    output += "⚫ ";
                }
            }
            output += "\n";
        }
        output
    }

    async fn number_chosen(pld: &mut CommandPayload, result: DialogResult) -> Result<bool, CallableError> {
        if let DialogResult::Number(choice) = result {
        let message_id = pld.msg.id;
        let cache_key = format!("Connect4Board:{}", message_id);
        let mut board = Connect4Command::board_from_cache(&pld.cache, &cache_key);
        board = Self::apply_move(&board, choice);
        let new_board = Self::render_board(&board);
        if board.single_player {
            //MinMax::MakeMove(board);
        }
        if let Ok(cached_board) = serde_json::to_string(&board) {
            pld.cache.set(cache_key, cached_board);
        }
        if let Some(winner) = Self::winner(&board) {
            if winner == 0 {
                //You Win
            } else {
                //You lose!
            }
        }
        pld.msg
            .channel_id
            .edit_message(pld.ctx.http(), message_id, |e| e.embed(|f| f.description(new_board))).await?;
    }
        Ok(false)
    }

    fn board_from_cache(cache: &CacheHandler, cache_key: &str) -> Connect4Board {
        let mut board = Connect4Board::new(false, 0u64, 0u64);
        if let Some(cache_entry) = cache.get(cache_key.to_string()) {
            if let Ok(cached_board) = serde_json::from_str(&cache_entry) {
                board = cached_board;
            }
        }
        board
    }
}

#[serenity::async_trait]
impl UnicornCommand for Connect4Command {
    fn command_name(&self) -> &str {
        "connectfour"
    }

    fn category(&self) -> &str {
        "minigames"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["c4", "connect4", "cfour"]
    }

    fn description(&self) -> &str {
        "Starts a connect four game with whomever you mention. \
        If you don't mention anyone, it starts a game with the bot. \
        You move by reacting with 1-7 to specify which column you want."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("upgrade", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut dlg = Dialogue::numbered_options("connectfour", 7);
        if !dlg.is_open(pld, pld.msg.author.id) {
            dlg.title = Some("Connect Four".to_string());
            dlg.footer = Some(format!("{}'s turn", pld.msg.author.name));
            dlg.description = Some(Box::new(|_pld| Connect4Command::render_board(&Connect4Board::new(false, 0, 0))));
            let dialog_result = dlg.show_dialog(pld).await?;
            Connect4Command::number_chosen(pld, dialog_result).await?;
            Ok(())
        } else {
            let mut response = unicorn_embed::UnicornEmbed::error("You already have a connectfour game running");
            pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
            Ok(())
        }
    }
}

impl GameBoard<Connect4Board, usize> for Connect4Command {
    fn gameover(board: &Connect4Board) -> bool {
        Self::possible_moves(board).len() == 0 || Self::winner(board).is_some()
    }

    fn score_board(board: &Connect4Board) -> f64 {
        match Self::winner(board) {
            Some(player) => {
                let moves = 22 - board.player_positions.count_ones();
                match player {
                    0 => moves as f64,
                    _ => -(moves as f64),
                }
            }
            None => 0.0,
        }
    }

    fn possible_moves(board: &Connect4Board) -> Vec<usize> {
        let full_board = board.all_positions;
        let top_row = (full_board << 8) & 0xF000000;
        let mut moves = Vec::new();
        for i in 1..7 {
            if i & top_row == 0 {
                moves.push(i as usize);
            }
        }
        moves
    }

    fn apply_move(board: &Connect4Board, player_move: usize) -> Connect4Board {
        let single_player = board.single_player;
        let next_playerboard = board.all_positions ^ board.player_positions;
        let new_fullboard = board.all_positions | (board.all_positions + (1 << (player_move * 8)));
        Connect4Board::new(single_player, new_fullboard, next_playerboard)
    }

    fn winner(board: &Connect4Board) -> Option<usize> {
        if Self::connected_four(board.player_positions) {
            Some(0)
        } else if Self::connected_four(board.player_positions ^ board.all_positions) {
            Some(1)
        } else {
            None
        }
    }

    fn copy(board: &Connect4Board) -> Connect4Board {
        Connect4Board::new(board.single_player, board.all_positions, board.player_positions)
    }
}

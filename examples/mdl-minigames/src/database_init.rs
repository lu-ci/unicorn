use log::{error, info};
use serde::{de::DeserializeOwned, Serialize};

use unicorn_database::DatabaseHandler;

use crate::named::Named;
use crate::professions::yaml::{YamlItem, YamlRecipe};

pub struct DatabaseInit;

impl DatabaseInit {
    pub fn check_initialise(db: &DatabaseHandler) {
        DatabaseInit::read_from_url::<YamlItem>(
            db,
            "items",
            "item",
            "https://gitlab.com/lu-ci/sigma/apex-sigma-res/raw/master/items/item_core_manifest.yml",
        );
        DatabaseInit::read_from_url::<YamlRecipe>(
            db,
            "recipes",
            "recipe",
            "https://gitlab.com/lu-ci/sigma/apex-sigma-res/raw/master/items/recipe_core_manifest.yml",
        );
    }

    fn read_from_url<T>(db: &DatabaseHandler, collection_name: &str, item_type: &str, url: &str)
    where
        T: Named + Serialize + DeserializeOwned,
    {
        if let Some(cli) = db.get_client() {
            let db = cli.database(&db.cfg.database.name);
            let item_collection = db.collection(collection_name);
            if let Ok(cursor) = item_collection.find(None, None) {
                if cursor.count() == 0 {
                    let _ = item_collection.drop(None);
                    info!("Updating profession {} files.", item_type);
                    let file_url = url;
                    let response = reqwest::blocking::get(file_url);
                    if let Ok(resp) = response {
                        let body = resp.text();
                        if let Ok(yaml) = body {
                            let items = serde_yaml::from_str::<Vec<T>>(yaml.as_str());
                            match items {
                                Ok(all_items) => {
                                    for item in all_items {
                                        if let Ok(bson_item) = bson::to_bson(&item) {
                                            if let Some(document) = bson_item.as_document() {
                                                match item_collection.insert_one(document.to_owned(), None) {
                                                    Ok(_) => info!("Updated profession {}: {}.", item_type, &item.get_name()),
                                                    Err(why) => error!("{}", why),
                                                }
                                            }
                                        }
                                    }
                                }
                                Err(why) => error!("{}", why),
                            }
                        }
                    }
                }
            }
        }
    }
}

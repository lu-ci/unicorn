use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct SendCommand;

impl SendCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for SendCommand {
    fn command_name(&self) -> &str {
        "send"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn example(&self) -> &str {
        "u:0123456789 We are watching..."
    }

    fn description(&self) -> &str {
        "Sends a message to a user, channel or server. \
         The first argument needs to be the destination parameter. \
         The destination parameter consists of the destination type and ID. \
         The types are U for User and C for Channel. \
         The type and ID are separated by a colon, or two dots put more simply."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("controller", false),
            CommandArgument::new("content", false).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.satisfied() {
            let controller = pld.args.get("controller");
            let content = pld.args.get("content");
            let control_pieces: Vec<&str> = controller.split(":").collect();
            if control_pieces.len() == 2 {
                let mode = control_pieces[0];
                let id_str = control_pieces[1];
                if let Ok(id) = id_str.parse::<u64>() {
                    match mode.to_lowercase().as_ref() {
                        "u" => {
                            if let Some(user) = pld.ctx.cache.user(id).await {
                                if let Ok(_) = user
                                    .dm(pld.ctx.http(), |m| {
                                        m.content(content);
                                        m
                                    })
                                    .await
                                {
                                    UnicornEmbed::ok(&format!("Message sent to {}#{:0>4}.", &user.name, &user.discriminator))
                                } else {
                                    UnicornEmbed::error(&format!("Failed to message {}#{:0>4}.", &user.name, &user.discriminator,))
                                }
                            } else {
                                UnicornEmbed::not_found("User not found.")
                            }
                        }
                        "c" => {
                            let channel = pld.ctx.cache.channel(id).await;
                            if let Some(channel) = channel {
                                let channel_name = &channel.id().name(&pld.ctx.cache).await.unwrap_or("<UNKNOWN>".to_owned());
                                if let Ok(_) = channel
                                    .id()
                                    .send_message(pld.ctx.http(), |m| {
                                        m.content(content);
                                        m
                                    })
                                    .await
                                {
                                    UnicornEmbed::ok(&format!("Message sent to #{}.", channel_name))
                                } else {
                                    UnicornEmbed::error(&format!("Failed to message #{}.", channel_name))
                                }
                            } else {
                                UnicornEmbed::not_found("Channel not found.")
                            }
                        }
                        _ => UnicornEmbed::error("The target type is invalid."),
                    }
                } else {
                    UnicornEmbed::error("The target ID is invalid.")
                }
            } else {
                UnicornEmbed::error("The controller argument is invalid.")
            }
        } else {
            UnicornEmbed::error("Not enough arguments, check the command example.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

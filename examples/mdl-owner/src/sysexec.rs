use std::process::Command;
use std::string::FromUtf8Error;

use log::debug;
use serenity::builder::CreateMessage;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct SysExecCommand;

impl SysExecCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn textify(content: Vec<u8>) -> Result<String, FromUtf8Error> {
        String::from_utf8(content)
    }
}

#[serenity::async_trait]
impl UnicornCommand for SysExecCommand {
    fn command_name(&self) -> &str {
        "sysexec"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["sh"]
    }

    fn example(&self) -> &str {
        "echo \"Hello, world!\""
    }

    fn description(&self) -> &str {
        "Executes a shell command. Extreme warning! \
         This executes commands in the operating system's shell. \
         Command Prompt on Windows and Bash on Linux. \
         It will execute things on the same level of authority as the program is ran by. \
         Meaning, don't do something stupid and wipe your damn root."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("command", false),
            CommandArgument::new("arguments", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if !pld.args.satisfied() {
            let mut cmd = Command::new(pld.args.get("command"));
            if pld.args.has_argument("arguments") {
                cmd.args(pld.args.get("arguments").split(" "));
            }
            let mut pr = CreateMessage::default();
            match cmd.output() {
                Ok(res) => {
                    if res.status.success() {
                        pr.embed(|e| {
                            e.color(0x77_b2_55);
                            e.title("✅ Shell command executed successfully.");
                            if !res.stdout.is_empty() {
                                if let Ok(content) = Self::textify(res.stdout) {
                                    e.description(format!("```\n{}\n```", content));
                                }
                            }
                            e
                        });
                        pr
                    } else {
                        pr.embed(|e| {
                            e.color(0xff_cc_4d);
                            e.title("⚠ Shell command executed but returned errors.");
                            if !res.stderr.is_empty() {
                                if let Ok(content) = Self::textify(res.stderr) {
                                    e.description(format!("```\n{}\n```", content));
                                }
                            }
                            e
                        });
                        pr
                    }
                }
                Err(why) => {
                    debug!("Shell command execution failed: {:?}", why);
                    UnicornEmbed::error("The shell command execution failed.")
                }
            }
        } else {
            UnicornEmbed::error("Nothing given to execute.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

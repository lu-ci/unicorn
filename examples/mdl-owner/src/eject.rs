use log::debug;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct EjectCommand;

impl EjectCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for EjectCommand {
    fn command_name(&self) -> &str {
        "eject"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn example(&self) -> &str {
        "012345678901234567"
    }

    fn description(&self) -> &str {
        "Makes the bot leave a Discord server."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("guild_id", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if !pld.args.is_empty() {
            if let Ok(gid) = pld.args.get("guild_id").parse::<u64>() {
                let guild = pld.ctx.cache.guild(gid).await;
                if let Some(guild) = guild {
                    match guild.leave(pld.ctx.http()).await {
                        Ok(_) => UnicornEmbed::ok(format!("Ok, left {}.", &guild.name)),
                        Err(why) => {
                            debug!("Failed leaving {} [{}]: {:?}", &guild.name, &guild.id.0, why);
                            UnicornEmbed::error(format!("Failed to leave {}.", &guild.name))
                        }
                    }
                } else {
                    UnicornEmbed::not_found("No guild with that ID was found.")
                }
            } else {
                UnicornEmbed::error("Invalid guild ID given.")
            }
        } else {
            UnicornEmbed::error("No guild ID given.")
        };
        pld.msg.channel_id.send_message(pld.ctx.http(), |_| &mut response).await?;
        Ok(())
    }
}

#!/usr/bin/env bash

release=$([[ ${1} =~ ^(--release|-r)$ ]] && echo 1 || echo 0)

echo "Synchronizing builder started!"
# shellcheck disable=SC2086
if [[ $release -eq 1 ]]; then
  echo "Building release target, workspace inclusive..."
  build_result=$(cargo build --release --workspace)
else
  echo "Building debug target, workspace inclusive..."
  build_result=$(cargo build --workspace)
fi

if [[ $build_result -ne 0 ]]; then
  echo "Build failure detected, terminating."
  # shellcheck disable=SC2086
  exit $build_result
fi

pwd
echo "Making sure modules exists..."
mkdir -p modules

case "$(uname -s)" in
  MINGW* | MSYS* | CYGWIN*) mdl_file="mdl_*.dll" && do_strip=0 ;;
  Linux*) mdl_file="libmdl_*.so" && do_strip=1 ;;
  *) echo "Unknown OS" && exit ;;
esac

echo "Cleaning modules folder..."
# shellcheck disable=SC2086
rm modules/${mdl_file}
echo "Copying module solutions..."
# shellcheck disable=SC2086
if [[ $release -eq 0 ]]; then
  do_strip=0
  cp -v -f target/debug/${mdl_file} modules/
else
  cp -v -f target/release/${mdl_file} modules/
fi

if [[ $do_strip -eq 1 ]]; then
  echo "Stripping module solutions..." &&
    for filename in modules/libmdl_*.so; do
      read -ra size_before <<< "$(du -h "$filename")"
      strip "$filename"
      read -ra size_after <<< "$(du -h "$filename")"
      echo "Stripped $filename: $size_before > $size_after"
    done
fi

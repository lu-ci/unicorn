use std::process::exit;

use fern::Dispatch;
use log::{error, info};

use unicorn_config::preferences::PreferencesConfig;

enum UnicornLoggerError {
    LoggerError(log::SetLoggerError),
    IOError(std::io::Error),
}

impl From<log::SetLoggerError> for UnicornLoggerError {
    fn from(err: log::SetLoggerError) -> Self {
        UnicornLoggerError::LoggerError(err)
    }
}

impl From<std::io::Error> for UnicornLoggerError {
    fn from(err: std::io::Error) -> Self {
        UnicornLoggerError::IOError(err)
    }
}

pub struct UnicornLogger;

impl UnicornLogger {
    pub fn init(cfg: &PreferencesConfig) {
        Self::check_dir();
        match Self::dispatch(cfg) {
            Ok(_) => {}
            Err(_) => {
                error!("Logger creation failed!");
                exit(22);
            }
        }
    }

    fn check_dir() {
        if !std::path::Path::new("logs").exists() {
            let _ = std::fs::create_dir("logs");
        }
    }

    fn dispatch(cfg: &PreferencesConfig) -> Result<(), UnicornLoggerError> {
        let level = if cfg.debug {
            log::LevelFilter::Debug
        } else {
            log::LevelFilter::Info
        };
        Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!(
                    "{}[{}][{}] {}",
                    chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                    record.target(),
                    record.level(),
                    message
                ))
            })
            .level(level)
            .chain(std::io::stdout())
            .chain(fern::log_file(format!(
                "logs/unc_{}.log",
                chrono::Local::now().format("%Y-%m-%d")
            ))?)
            .apply()?;
        info!("Unicorn logger created!");
        Ok(())
    }
}
